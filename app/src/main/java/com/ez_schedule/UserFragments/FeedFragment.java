package com.ez_schedule.UserFragments;


import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ez_schedule.Activities.AddPostActivity;
import com.ez_schedule.Activities.SignInActivity;
import com.ez_schedule.BarbarActivities.AddServiceActivity;
import com.ez_schedule.ModelClasses.PostModel;
import com.ez_schedule.ModelClasses.TimeslotModel;
import com.ez_schedule.R;
import com.ez_schedule.UserAdapters.FeedAdapter;
import com.ez_schedule.UserAdapters.TimeSlotAdapter;
import com.ez_schedule.Util.ConnectivityReceiver;
import com.ez_schedule.Util.Parameters;
import com.ez_schedule.Util.SavePref;
import com.ez_schedule.Util.util;
import com.ez_schedule.parser.AllAPIS;
import com.ez_schedule.parser.GetAsync;
import com.ez_schedule.parser.GetAsyncGet;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

/**
 * A simple {@link Fragment} subclass.
 */
public class FeedFragment extends Fragment {


    Context context;
    @BindView(R.id.my_recycler_view)
    RecyclerView myRecyclerView;
    @BindView(R.id.add_post)
    ImageView addPost;
    private SavePref savePref;
    Unbinder unbinder;

    ArrayList<PostModel> list;

    public FeedFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_feed, container, false);

        unbinder = ButterKnife.bind(this, view);
        context = getActivity();
        savePref = new SavePref(context);


        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (ConnectivityReceiver.isConnected()) {
            GET_POST_LIST();
        } else {
            util.IOSDialog(context, util.internet_Connection_Error);
        }
    }

    private void GET_POST_LIST() {
        ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.DATE, "1593231222");
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsyncGet mAsync = new GetAsyncGet(context, AllAPIS.GET_POST, formBody) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                list = new ArrayList<>();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {
                            JSONArray body = jsonmainObject.getJSONArray("body");
                            for (int i = 0; i < body.length(); i++) {
                                JSONObject object = body.getJSONObject(i);
                                PostModel postModel = new PostModel();
                                postModel.setCreated(object.getString("createdAt"));
                                postModel.setDescription(object.getString("description"));
                                postModel.setImage(object.getString("image"));
                                if (object.optJSONObject("user") != null) {
                                    postModel.setName(object.getJSONObject("user").getString("username"));
                                    postModel.setProfile_image(object.getJSONObject("user").getString("profile_image"));
                                }
                                list.add(postModel);
                            }

                            FeedAdapter adapter = new FeedAdapter(context, list);
                            myRecyclerView.setLayoutManager(new LinearLayoutManager(context));
                            myRecyclerView.setAdapter(adapter);

                        } else {
                            if (jsonmainObject.getString("msg").equals(util.Invalid_Authorization)) {
                                 savePref.setAuthorization_key("");
                                Intent intent = new Intent(context, SignInActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                            } else {
                                util.showToast(context, jsonmainObject.getString("msg"));
                            }
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.add_post)
    public void onClick() {
        startActivity(new Intent(context, AddPostActivity.class));
        getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
    }
}
