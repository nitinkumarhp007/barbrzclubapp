package com.ez_schedule.UserFragments;


import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ez_schedule.Activities.CharityActivity;
import com.ez_schedule.Activities.SignInActivity;
import com.ez_schedule.BarbarAdapters.RequetsAdapter;
import com.ez_schedule.BarbarFragments.HomeBarbarFragment;
import com.ez_schedule.MainActivity;
import com.ez_schedule.ModelClasses.BarbarServiceListModel;
import com.ez_schedule.ModelClasses.RequestModel;
import com.ez_schedule.ModelClasses.UserOrdersModel;
import com.ez_schedule.R;
import com.ez_schedule.UserAdapters.OrderAdapter;
import com.ez_schedule.UserAdapters.UserSideRequetsAdapter;
import com.ez_schedule.Util.ConnectivityReceiver;
import com.ez_schedule.Util.Parameters;
import com.ez_schedule.Util.SavePref;
import com.ez_schedule.Util.util;
import com.ez_schedule.parser.AllAPIS;
import com.ez_schedule.parser.GetAsync;
import com.ez_schedule.parser.GetAsyncGet;
import com.ligl.android.widget.iosdialog.IOSDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import sqip.Card;
import sqip.CardDetails;
import sqip.CardEntry;
import sqip.CardEntryActivityCommand;
import sqip.CardNonceBackgroundHandler;

import static com.ez_schedule.parser.AllAPIS.ORDER_PAYMENT;
import static sqip.CardEntry.DEFAULT_CARD_ENTRY_REQUEST_CODE;

/**
 * A simple {@link Fragment} subclass.
 */
public class OrdersFragment extends Fragment {


    Context context;
    @BindView(R.id.upcoming)
    Button upcoming;
    @BindView(R.id.past)
    Button past;
    @BindView(R.id.upcoming_view)
    View upcomingView;
    @BindView(R.id.past_view)
    View pastView;
    @BindView(R.id.my_recycler_view)
    RecyclerView myRecyclerView;
    @BindView(R.id.error_message)
    TextView errorMessage;
    @BindView(R.id.Requests)
    Button Requests;
    @BindView(R.id.Requests_view)
    View RequestsView;
    private SavePref savePref;
    Unbinder unbinder;

    ArrayList<UserOrdersModel> list_main;
    ArrayList<RequestModel> list_request;

    UserSideRequetsAdapter adapter;

    public int position_now = 0;

    public OrdersFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_orders, container, false);

        unbinder = ButterKnife.bind(this, view);
        context = getActivity();
        savePref = new SavePref(context);


        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        RequestsView.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
        upcomingView.setBackgroundColor(getResources().getColor(R.color.white));
        pastView.setBackgroundColor(getResources().getColor(R.color.white));
        if (ConnectivityReceiver.isConnected()) {
            REQURST_SENT_TO_BARBER_LIST();
        } else {
            util.IOSDialog(context, util.internet_Connection_Error);
        }

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.Requests, R.id.past, R.id.upcoming})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.Requests:
                RequestsView.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
                upcomingView.setBackgroundColor(getResources().getColor(R.color.white));
                pastView.setBackgroundColor(getResources().getColor(R.color.white));
                Requests.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                upcoming.setTextColor(getResources().getColor(R.color.black));
                past.setTextColor(getResources().getColor(R.color.black));
                if (ConnectivityReceiver.isConnected()) {
                    REQURST_SENT_TO_BARBER_LIST();
                } else {
                    util.IOSDialog(context, util.internet_Connection_Error);
                }
                break;
            case R.id.upcoming:
                upcomingView.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
                RequestsView.setBackgroundColor(getResources().getColor(R.color.white));
                pastView.setBackgroundColor(getResources().getColor(R.color.white));
                Requests.setTextColor(getResources().getColor(R.color.black));
                upcoming.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                past.setTextColor(getResources().getColor(R.color.black));
                if (ConnectivityReceiver.isConnected()) {
                    USER_ORDERS_LIST("1");
                } else {
                    util.IOSDialog(context, util.internet_Connection_Error);
                }
                break;
            case R.id.past:
                pastView.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
                RequestsView.setBackgroundColor(getResources().getColor(R.color.white));
                upcomingView.setBackgroundColor(getResources().getColor(R.color.white));
                Requests.setTextColor(getResources().getColor(R.color.black));
                upcoming.setTextColor(getResources().getColor(R.color.black));
                past.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                if (ConnectivityReceiver.isConnected()) {
                    USER_ORDERS_LIST("2");
                } else {
                    util.IOSDialog(context, util.internet_Connection_Error);
                }
                break;
        }
    }

    private void REQURST_SENT_TO_BARBER_LIST() {
        ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.AUTH_KEY, "");
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsyncGet mAsync = new GetAsyncGet(context, AllAPIS.REQURST_SENT_TO_BARBER, formBody) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                list_request = new ArrayList<>();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {
                            JSONArray body = jsonmainObject.getJSONArray("body");
                            for (int i = 0; i < body.length(); i++) {
                                JSONObject object = body.getJSONObject(i);
                                RequestModel requestModel = new RequestModel();
                                requestModel.setCost(object.getJSONObject("request").getString("total_amount"));
                                requestModel.setId(object.getJSONObject("request").getString("id"));
                                requestModel.setDate(object.getJSONObject("request").getString("date"));
                                requestModel.setStatus(object.getJSONObject("request").getString("status"));
                                requestModel.setInfo(object.getJSONObject("request").getString("info"));
                                requestModel.setImage(object.getJSONObject("user").getString("profile_image"));
                                requestModel.setUsername(object.getJSONObject("user").getString("username"));
                                requestModel.setSlot_Detail(object.getJSONObject("slot_Detail").getString("start_time") + "-" + object.getJSONObject("slot_Detail").getString("end_time"));
                                ArrayList<BarbarServiceListModel> list = new ArrayList<>();
                                for (int j = 0; j < object.getJSONArray("services_detail").length(); j++) {
                                    JSONObject obj = object.getJSONArray("services_detail").getJSONObject(j);
                                    BarbarServiceListModel barbarServiceListModel = new BarbarServiceListModel();
                                    barbarServiceListModel.setId(obj.getString("id"));
                                    barbarServiceListModel.setName(obj.getString("name"));
                                    barbarServiceListModel.setPrice(obj.getString("price"));
                                    barbarServiceListModel.setDuration(obj.optString("duration"));
                                    list.add(barbarServiceListModel);
                                }
                                requestModel.setList(list);
                                list_request.add(requestModel);
                            }


                            if (list_request.size() > 0) {
                                adapter = new UserSideRequetsAdapter(context, list_request, OrdersFragment.this);
                                myRecyclerView.setLayoutManager(new LinearLayoutManager(context));
                                myRecyclerView.setAdapter(adapter);

                                myRecyclerView.setVisibility(View.VISIBLE);
                                errorMessage.setVisibility(View.GONE);

                            } else {
                                errorMessage.setText("No Requets Found");
                                errorMessage.setVisibility(View.VISIBLE);
                                myRecyclerView.setVisibility(View.GONE);
                            }

                        } else {
                            if (jsonmainObject.getString("msg").equals(util.Invalid_Authorization)) {
                                 savePref.setAuthorization_key("");
                                Intent intent = new Intent(context, SignInActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                            } else {
                                util.showToast(context, jsonmainObject.getString("msg"));
                            }
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    public void ProceedPayment(int position) {
        position_now = position;
        if (ConnectivityReceiver.isConnected()) {
            CardEntry.startCardEntryActivity(getActivity(), true,
                    DEFAULT_CARD_ENTRY_REQUEST_CODE);
        } else {
            util.IOSDialog(context, util.internet_Connection_Error);
        }

    }


    public void ORDER_PAYMENT(int position, String payment_nonce, String type) {
        Log.e("position__", String.valueOf(position));
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.REQUEST_ID, list_request.get(position).getId());
        formBuilder.addFormDataPart(Parameters.PAYMENT_NONCE, payment_nonce);
        formBuilder.addFormDataPart(Parameters.TYPE, type);//1=online , 2 = cash
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, ORDER_PAYMENT, formBody, savePref.getAuthorization_key()) {

            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonMainobject = new JSONObject(result);
                        if (jsonMainobject.getString("code").equalsIgnoreCase("200")) {

                            String message = "";
                            if (type.equals("1"))
                                message = "Payment Done! Order Placed Successfully!";
                            else if (type.equals("2"))
                                message = "Order Placed Successfully!";

                            new IOSDialog.Builder(context)
                                    .setCancelable(false)
                                    .setMessage(message).setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    upcomingView.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
                                    RequestsView.setBackgroundColor(getResources().getColor(R.color.white));
                                    pastView.setBackgroundColor(getResources().getColor(R.color.white));
                                    Requests.setTextColor(getResources().getColor(R.color.black));
                                    upcoming.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                                    past.setTextColor(getResources().getColor(R.color.black));
                                    if (ConnectivityReceiver.isConnected()) {
                                        USER_ORDERS_LIST("1");
                                    } else {
                                        util.IOSDialog(context, util.internet_Connection_Error);
                                    }

                                    /*startActivity(new Intent(getActivity(), CharityActivity.class));
                                    getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);*/

                                }
                            }).show();
                        } else {
                            util.showToast(context, jsonMainobject.getString("msg"));
                        }


                    } catch (Exception e) {
                    }
                }

            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();

    }

    private void USER_ORDERS_LIST(String is_upcoming) {
        ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.IS_UPCOMING, is_upcoming);
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.USER_ORDERS, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                list_main = new ArrayList<>();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {
                            JSONArray body = jsonmainObject.getJSONArray("body");
                            for (int i = 0; i < body.length(); i++) {
                                JSONObject object = body.getJSONObject(i);
                                UserOrdersModel requestModel = new UserOrdersModel();
                                requestModel.setStatus(object.getJSONObject("order").getString("status"));
                                requestModel.setDate(object.getJSONObject("order").getString("date"));
                                requestModel.setCost(object.getJSONObject("order").getString("total_amount"));
                                requestModel.setId(object.getJSONObject("order").getString("id"));
                                requestModel.setImage(object.getJSONObject("barber").getString("profile_image"));
                                requestModel.setUsername(object.getJSONObject("barber").getString("username"));

                                requestModel.setSlot_Detail(object.getJSONObject("slot_Detail").getString("start_time") + "-" + object.getJSONObject("slot_Detail").getString("end_time"));
                                ArrayList<BarbarServiceListModel> list = new ArrayList<>();
                                for (int j = 0; j < object.getJSONArray("services_detail").length(); j++) {
                                    JSONObject obj = object.getJSONArray("services_detail").getJSONObject(j);
                                    BarbarServiceListModel barbarServiceListModel = new BarbarServiceListModel();
                                    barbarServiceListModel.setId(obj.getString("id"));
                                    barbarServiceListModel.setName(obj.getString("name"));
                                    barbarServiceListModel.setPrice(obj.getString("price"));
                                    barbarServiceListModel.setDuration(obj.optString("duration"));
                                    list.add(barbarServiceListModel);
                                }
                                requestModel.setList(list);
                                list_main.add(requestModel);
                            }


                            if (list_main.size() > 0) {
                                OrderAdapter adapter = new OrderAdapter(context, list_main, is_upcoming);
                                myRecyclerView.setLayoutManager(new LinearLayoutManager(context));
                                myRecyclerView.setAdapter(adapter);

                                myRecyclerView.setVisibility(View.VISIBLE);
                                errorMessage.setVisibility(View.GONE);

                            } else {
                                errorMessage.setText(R.string.no_orders_yet);
                                errorMessage.setVisibility(View.VISIBLE);
                                myRecyclerView.setVisibility(View.GONE);
                            }

                        } else {
                            if (jsonmainObject.getString("msg").equals(util.Invalid_Authorization)) {
                                 savePref.setAuthorization_key("");
                                Intent intent = new Intent(context, SignInActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                            } else {
                                util.showToast(context, jsonmainObject.getString("msg"));
                            }
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    //square SDK Work

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        CardEntry.handleActivityResult(data, result -> {
            if (result.isSuccess()) {
                CardDetails cardResult = result.getSuccessValue();
                Card card = cardResult.getCard();
                String nonce = cardResult.getNonce();

                /*Toast.makeText(context,
                        "Payment Successfully Done",
                        Toast.LENGTH_SHORT)
                        .show();*/

                Log.e("position__", nonce);

                ORDER_PAYMENT(position_now, nonce, "1");


            } else if (result.isCanceled()) {
                Toast.makeText(context,
                        R.string.cancelled,
                        Toast.LENGTH_SHORT)
                        .show();
            }
        });
    }

}


