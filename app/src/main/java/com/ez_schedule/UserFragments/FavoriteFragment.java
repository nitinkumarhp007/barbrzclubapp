package com.ez_schedule.UserFragments;


import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ez_schedule.Activities.SignInActivity;
import com.ez_schedule.ModelClasses.BarbarModel;
import com.ez_schedule.R;
import com.ez_schedule.UserAdapters.FavoriteAdapter;
import com.ez_schedule.Util.ConnectivityReceiver;
import com.ez_schedule.Util.Parameters;
import com.ez_schedule.Util.SavePref;
import com.ez_schedule.Util.util;
import com.ez_schedule.parser.AllAPIS;
import com.ez_schedule.parser.GetAsync;
import com.ez_schedule.parser.GetAsyncGet;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

/**
 * A simple {@link Fragment} subclass.
 */
public class FavoriteFragment extends Fragment {


    Context context;
    @BindView(R.id.my_recycler_view)
    RecyclerView myRecyclerView;
    @BindView(R.id.error_message)
    TextView errorMessage;
    private SavePref savePref;
    Unbinder unbinder;
    FavoriteAdapter adapter;
    ArrayList<BarbarModel> list;

    public FavoriteFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_favorite, container, false);

        unbinder = ButterKnife.bind(this, view);
        context = getActivity();
        savePref = new SavePref(context);


        if (ConnectivityReceiver.isConnected()) {
            FAV_LIST();
        } else {
            util.IOSDialog(context, util.internet_Connection_Error);
        }


        return view;
    }

    private void FAV_LIST() {
        ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.AUTH_KEY, "");
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsyncGet mAsync = new GetAsyncGet(context, AllAPIS.FAV_LIST, formBody) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                list = new ArrayList<>();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {
                            JSONArray body = jsonmainObject.getJSONArray("body");
                            for (int i = 0; i < body.length(); i++) {
                                if (body.getJSONObject(i).getJSONObject("barber") != null) {
                                    JSONObject object = body.getJSONObject(i).getJSONObject("barber");
                                    BarbarModel barbarModel = new BarbarModel();
                                    barbarModel.setAvg_rating(object.getString("avg_rating"));
                                    barbarModel.setId(object.getString("id"));
                                    barbarModel.setProfile_image(object.getString("profile_image"));
                                    barbarModel.setUsername(object.getString("username"));
                                    list.add(barbarModel);
                                }
                            }


                            if (list.size() > 0) {
                                adapter = new FavoriteAdapter(context, list, FavoriteFragment.this);
                                myRecyclerView.setLayoutManager(new GridLayoutManager(context, 2));
                                myRecyclerView.setAdapter(adapter);

                                myRecyclerView.setVisibility(View.VISIBLE);
                                errorMessage.setVisibility(View.GONE);

                            } else {
                                errorMessage.setVisibility(View.VISIBLE);
                                myRecyclerView.setVisibility(View.GONE);
                            }

                        } else {
                            if (jsonmainObject.getString("msg").equals(util.Invalid_Authorization)) {
                                 savePref.setAuthorization_key("");
                                Intent intent = new Intent(context, SignInActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                            } else {
                                util.showToast(context, jsonmainObject.getString("msg"));
                            }
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    public void FAV_UNFAV_API(int position) {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.BARBER_ID, list.get(position).getId());
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.FAV_UNFAV, formBody, savePref.getAuthorization_key()) {

            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonObject = new JSONObject(result);
                        list.remove(position);


                        if (list.size() > 0) {
                            adapter.notifyDataSetChanged();

                            myRecyclerView.setVisibility(View.VISIBLE);
                            errorMessage.setVisibility(View.GONE);

                        } else {
                            errorMessage.setVisibility(View.VISIBLE);
                            myRecyclerView.setVisibility(View.GONE);
                        }

                    } catch (Exception e) {
                    }
                }

            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
