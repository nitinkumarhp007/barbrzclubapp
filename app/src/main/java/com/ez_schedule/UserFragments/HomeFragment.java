package com.ez_schedule.UserFragments;


import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.os.Looper;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ez_schedule.Activities.BarbarsListingActivity;
import com.ez_schedule.Activities.CharityActivity;
import com.ez_schedule.Activities.SignInActivity;
import com.ez_schedule.MainActivity;
import com.ez_schedule.ModelClasses.BarbarModel;
import com.ez_schedule.ModelClasses.BarbarServiceListModel;
import com.ez_schedule.ModelClasses.CategoryModel;
import com.ez_schedule.UserAdapters.HomeBarbarsAdapter;
import com.ez_schedule.UserAdapters.HomeCategoryAdapter;
import com.ez_schedule.R;
import com.ez_schedule.Util.ConnectivityReceiver;
import com.ez_schedule.Util.GPSTracker;
import com.ez_schedule.Util.Parameters;
import com.ez_schedule.Util.SavePref;
import com.ez_schedule.Util.util;
import com.ez_schedule.parser.AllAPIS;
import com.ez_schedule.parser.GetAsync;
import com.ez_schedule.parser.GetAsyncGet;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.google.android.play.core.review.ReviewInfo;
import com.google.android.play.core.review.ReviewManager;
import com.google.android.play.core.review.ReviewManagerFactory;
import com.google.android.play.core.tasks.Task;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

import static androidx.core.content.ContextCompat.checkSelfPermission;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {
    Context context;
    @BindView(R.id.search_bar)
    EditText searchBar;
    @BindView(R.id.location)
    TextView location_t;
    @BindView(R.id.my_recycler_view_top)
    RecyclerView myRecyclerViewTop;
    @BindView(R.id.my_recycler_view)
    RecyclerView myRecyclerView;
    @BindView(R.id.error_message)
    TextView errorMessage;
    private SavePref savePref;
    Unbinder unbinder;
    HomeBarbarsAdapter adapter;
    private final int PLACE_PICKER_REQUEST = 787;
    String latitude = "", longitude = "";
    GPSTracker gpsTracker = null;
    FusedLocationProviderClient fusedLocationClient = null;
    Location location = null;
    private ArrayList<CategoryModel> list;
    private ArrayList<BarbarModel> barbar_list;
    private ArrayList<BarbarModel> barbar_list_search;

    boolean hit = false;

    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        unbinder = ButterKnife.bind(this, view);
        context = getActivity();
        savePref = new SavePref(context);


        gpsTracker = new GPSTracker(context);

        /*Intent intent = new Intent(context, CharityActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);*/

        // ReviewInfo reviewInfo=null;

       /* ReviewManager manager = ReviewManagerFactory.create(context);
        Task<ReviewInfo> request = manager.requestReviewFlow();
        request.addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                // We can get the ReviewInfo object
                ReviewInfo   reviewInfo = task.getResult();
                Task<Void> flow = manager.launchReviewFlow(getActivity(), reviewInfo);
                flow.addOnCompleteListener(task1 -> {
                    // The flow has finished. The API does not indicate whether the user
                    // reviewed or not, or even whether the review dialog was shown. Thus, no
                    // matter the result, we continue our app flow.
                });
            } else {
                // There was some problem, continue regardless of the result.
            }
        });
*/


        location_t.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Locationget();
            }
        });

        searchBar.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {

                    if (ConnectivityReceiver.isConnected()) {
                        GETCATEGORYLIST(latitude, longitude, v.getText().toString().trim());
                    } else
                        util.IOSDialog(context, util.internet_Connection_Error);




                    /*if (!v.getText().toString().trim().isEmpty())
                        USER_LISTING(v.getText().toString().trim());*/
                    return true;
                }
                return false;
            }
        });


        return view;
    }


    private void UPDATE_PROFILE_API() {
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
       /* formBuilder.addFormDataPart(Parameters.USERNAME, savePref.getName());
        formBuilder.addFormDataPart(Parameters.EMAIL, savePref.getEmail());
        formBuilder.addFormDataPart(Parameters.PHONE, savePref.getPhone());*/
        formBuilder.addFormDataPart(Parameters.LAT, latitude);
        formBuilder.addFormDataPart(Parameters.LNG, longitude);
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.UPDATE_LAT_LONG, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonMainobject = new JSONObject(result);
                        if (jsonMainobject.getString("code").equalsIgnoreCase("200")) {
                            //util.showToast(context, "Profile Updated Sucessfully!!!");
                        } else {
                            util.IOSDialog(context, jsonMainobject.getString("msg"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    private String getCompleteAddressString(double LATITUDE, double LONGITUDE) {
        String strAdd = "";
        Geocoder geocoder = new Geocoder(context, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);
            if (addresses != null) {
                Address returnedAddress = addresses.get(0);
                StringBuilder strReturnedAddress = new StringBuilder("");

                for (int i = 0; i <= returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
                }
                strAdd = strReturnedAddress.toString();
                // Log.e("My Current loction address", strReturnedAddress.toString());
            } else {
                // Log.e("My Current loction address", "No Address returned!");
            }
        } catch (Exception e) {
            e.printStackTrace();
            // Log.w("My Current loction address", "Canont get Address!");
        }
        return strAdd;
    }

    private void Locationget() {
        // Initialize Places.
        Places.initialize(context, "AIzaSyAA-k3KpN8PbS5u4_9qLFGIFZ_fIm52iM4");
        // Create a new Places client instance.
        PlacesClient placesClient = Places.createClient(context);
        // Set the fields to specify which types of place data to return.
        List<com.google.android.libraries.places.api.model.Place.Field> fields = Arrays.asList(com.google.android.libraries.places.api.model.Place.Field.ID,
                com.google.android.libraries.places.api.model.Place.Field.NAME, com.google.android.libraries.places.api.model.Place.Field.LAT_LNG, com.google.android.libraries.places.api.model.Place.Field.ADDRESS, com.google.android.libraries.places.api.model.Place.Field.ID, com.google.android.libraries.places.api.model.Place.Field.PHONE_NUMBER, com.google.android.libraries.places.api.model.Place.Field.RATING, com.google.android.libraries.places.api.model.Place.Field.WEBSITE_URI);
        // Start the autocomplete intent.
        Intent intent = new Autocomplete.IntentBuilder(AutocompleteActivityMode.OVERLAY, fields).build(context);

        startActivityForResult(intent, PLACE_PICKER_REQUEST);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //checkPermissionOnActivityResult(requestCode, resultCode, data);
        if (resultCode == getActivity().RESULT_OK) {
            switch (requestCode) {
                case PLACE_PICKER_REQUEST:
                    Place place = Autocomplete.getPlaceFromIntent(data);
                    String placeName = String.valueOf(place.getName());
                    location_t.setText(placeName);
                    latitude = String.valueOf(place.getLatLng().latitude);
                    longitude = String.valueOf(place.getLatLng().longitude);

                    if (ConnectivityReceiver.isConnected()) {
                        GETCATEGORYLIST(latitude, longitude, "");
                    } else {
                        util.IOSDialog(context, util.internet_Connection_Error);
                    }

            }
        }

    }

    private void GETCATEGORYLIST(String latitude, String longitude, String search) {
        ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.LAT, latitude);
        formBuilder.addFormDataPart(Parameters.LNG, longitude);
        formBuilder.addFormDataPart(Parameters.SEARCH, search);
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.HOME, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                try {
                    if (mDialog != null)
                        mDialog.dismiss();
                } catch (Exception e) {

                }
                list = new ArrayList<>();
                barbar_list = new ArrayList<>();
                barbar_list_search = new ArrayList<>();
                UPDATE_PROFILE_API();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {
                            JSONObject body = jsonmainObject.getJSONObject("body");
                            JSONArray Facility = body.getJSONArray("categorydata");
                            for (int i = 0; i < Facility.length(); i++) {
                                JSONObject object = Facility.getJSONObject(i);
                                CategoryModel categoryModel = new CategoryModel();
                                categoryModel.setId(object.getString("id"));
                                categoryModel.setName(object.getString("name"));
                                categoryModel.setImage(object.optString("image"));
                                list.add(categoryModel);
                            }

                            HomeCategoryAdapter homeAdapter = new HomeCategoryAdapter(context, list);
                            myRecyclerViewTop.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
                            myRecyclerViewTop.setAdapter(homeAdapter);


                            JSONArray all_barbers = body.getJSONArray("all_barbers");
                            for (int i = 0; i < all_barbers.length(); i++) {
                                JSONObject object = all_barbers.getJSONObject(i);

                                BarbarModel barbarModel = new BarbarModel();
                                barbarModel.setAddress(object.getString("address"));
                                barbarModel.setAvg_rating(object.getString("avg_rating"));
                                barbarModel.setDescription(object.getString("description"));
                                barbarModel.setEmail(object.getString("email"));
                                barbarModel.setId(object.getString("id"));
                                barbarModel.setIs_fav(object.getString("is_fav"));
                                barbarModel.setPhone(object.getString("phone"));
                                barbarModel.setProfile_image(object.getString("profile_image"));
                                barbarModel.setUsername(object.getString("username"));
                                if (search.isEmpty())
                                    barbar_list.add(barbarModel);
                                else
                                    barbar_list_search.add(barbarModel);
                            }

                            if (search.isEmpty()) {
                                adapter = new HomeBarbarsAdapter(context, barbar_list, HomeFragment.this);
                                myRecyclerView.setLayoutManager(new GridLayoutManager(context, 2));
                                myRecyclerView.setAdapter(adapter);
                            } else {
                                if (barbar_list_search.size() == 0) {
                                    util.IOSDialog(context, "No Professional Found");
                                } else {
                                    searchBar.setText("");
                                    Intent intent = new Intent(context, BarbarsListingActivity.class);
                                    intent.putExtra("is_search", true);
                                    intent.putExtra("data", barbar_list_search);
                                    context.startActivity(intent);
                                    MainActivity.context.overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                                }

                            }


                        } else {
                            if (jsonmainObject.getString("msg").equals(util.Invalid_Authorization)) {
                                 savePref.setAuthorization_key("");
                                Intent intent = new Intent(context, SignInActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                            } else {
                                util.showToast(context, jsonmainObject.getString("msg"));
                            }
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    public void FAV_UNFAV_API(int position) {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.BARBER_ID, barbar_list.get(position).getId());
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.FAV_UNFAV, formBody, savePref.getAuthorization_key()) {

            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonObject = new JSONObject(result);
                        if (barbar_list.get(position).getIs_fav().equals("1"))
                            barbar_list.get(position).setIs_fav("0");
                        else
                            barbar_list.get(position).setIs_fav("1");
                        adapter.notifyDataSetChanged();
                    } catch (Exception e) {
                    }
                }

            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    @Override
    public void onResume() {
        super.onResume();

        if (gpsTracker.canGetLocation()) {
            location_t.setText(getCompleteAddressString(gpsTracker.getLatitude(), gpsTracker.getLongitude()));

            latitude = String.valueOf(gpsTracker.getLatitude());
            longitude = String.valueOf(gpsTracker.getLongitude());

            hit = true;


            if (ConnectivityReceiver.isConnected()) {
                GETCATEGORYLIST(latitude, longitude, "");
            } else {
                util.IOSDialog(context, util.internet_Connection_Error);
            }

        }
        EnableGPSAutoMatically();
    }

    private void EnableGPSAutoMatically() {
        GoogleApiClient googleApiClient = null;
        if (googleApiClient == null) {
            googleApiClient = new GoogleApiClient.Builder(getActivity())
                    .addApi(LocationServices.API).addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this).build();
            googleApiClient.connect();
            LocationRequest locationRequest = LocationRequest.create();
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            locationRequest.setInterval(1 * 1000);
            locationRequest.setFastestInterval(1 * 1000);
            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                    .addLocationRequest(locationRequest);

            // **************************
            builder.setAlwaysShow(true); // this is the key ingredient
            // **************************

            PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi
                    .checkLocationSettings(googleApiClient, builder.build());
            result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
                @Override
                public void onResult(LocationSettingsResult result) {
                    final Status status = result.getStatus();
                    final LocationSettingsStates state = result
                            .getLocationSettingsStates();
                    switch (status.getStatusCode()) {
                        case LocationSettingsStatusCodes.SUCCESS:
                            // util.showToast(context, "GPS Success");
                            // All location settings are satisfied. The client can
                            // initialize location
                            // requests here.

                            if (fusedLocationClient == null)
                                fusedLocationClient = LocationServices.getFusedLocationProviderClient(requireContext());
                            if (checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                                // TODO: Consider calling
                                //    Activity#requestPermissions
                                // here to request the missing permissions, and then overriding
                                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                //                                          int[] grantResults)
                                // to handle the case where the user grants the permission. See the documentation
                                // for Activity#requestPermissions for more details.
                                return;
                            }
                            fusedLocationClient.requestLocationUpdates(locationRequest, mLocationCallback, Looper.myLooper());


                            break;
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            // util.showToast(context, "GPS is not on");
                            // Location settings are not satisfied. But could be
                            // fixed by showing the user
                            // a dialog.
                            try {
                                // Show the dialog by calling
                                // startResolutionForResult(),
                                // and check the result in onActivityResult().
                                status.startResolutionForResult(getActivity(), 1000);

                            } catch (IntentSender.SendIntentException e) {
                                // Ignore the error.
                            }


                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                            // util.showToast(context, "Setting change not allowed");
                            // Location settings are not satisfied. However, we have
                            // no way to fix the
                            // settings so we won't show the dialog.
                            break;
                    }
                }
            });
        }
    }

    LocationCallback mLocationCallback = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult locationResult) {
            List<Location> locationList = locationResult.getLocations();
            if (locationList.size() > 0) {
                //The last location in the list is the newest
                location = locationList.get(locationList.size() - 1);
                // util.showToast(getActivity(), "lat long " + location.getLatitude());


                /*gpsTracker = new GPSTracker(context);
                //util.showToast(context, "latt " + String.valueOf(gpsTracker.getLatitude()));
                if (ConnectivityReceiver.isConnected()) {
                    AGENCIE("", true);
                } else {
                    util.IOSDialog(context, util.internet_Connection_Error);
                }*/

                if (latitude.isEmpty() || latitude == null) {
                    latitude = String.valueOf(location.getLatitude());
                    longitude = String.valueOf(location.getLatitude());

                    if (location_t != null)
                        location_t.setText(getCompleteAddressString(location.getLatitude(), location.getLongitude()));
                }


                if (ConnectivityReceiver.isConnected()) {
                    if (!hit)
                        GETCATEGORYLIST(latitude, longitude, "");
                } else {
                    util.IOSDialog(context, util.internet_Connection_Error);
                }


                fusedLocationClient.removeLocationUpdates(mLocationCallback);
            }
        }
    };


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}
