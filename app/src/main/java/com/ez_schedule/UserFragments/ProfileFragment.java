package com.ez_schedule.UserFragments;


import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.ez_schedule.Activities.BarbarsListingActivity;
import com.ez_schedule.Activities.ChangePasswordActivity;
import com.ez_schedule.Activities.ReviewListActivity;
import com.ez_schedule.Activities.RewardPointsActivity;
import com.ez_schedule.Activities.SignInActivity;
import com.ez_schedule.Activities.SignupTimeSlotActivity;
import com.ez_schedule.Activities.TermConditionActivity;
import com.ez_schedule.Activities.TimeSlotRequestListActivity;
import com.ez_schedule.Activities.UpdateProfileActivity;
import com.ez_schedule.MainActivity;
import com.ez_schedule.ModelClasses.ReviewModel;
import com.ez_schedule.R;
import com.ez_schedule.UserAdapters.RatingAdapter;
import com.ez_schedule.Util.ConnectivityReceiver;
import com.ez_schedule.Util.GPSTracker;
import com.ez_schedule.Util.LocaleHelper;
import com.ez_schedule.Util.Parameters;
import com.ez_schedule.Util.SavePref;
import com.ez_schedule.Util.util;
import com.ez_schedule.parser.AllAPIS;
import com.ez_schedule.parser.GetAsync;
import com.bumptech.glide.Glide;
import com.ligl.android.widget.iosdialog.IOSDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.FormBody;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import sqip.Card;
import sqip.CardDetails;
import sqip.CardEntry;
import sqip.CardEntryActivityCommand;
import sqip.CardNonceBackgroundHandler;

import static sqip.CardEntry.DEFAULT_CARD_ENTRY_REQUEST_CODE;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends Fragment {
    Context context;
    @BindView(R.id.profile_pic)
    CircleImageView profilePic;
    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.email)
    TextView email;
    @BindView(R.id.phone)
    TextView phone;
    @BindView(R.id.edit_profile_text)
    TextView editProfileText;
    @BindView(R.id.edit_profile111)
    RelativeLayout editProfile111;
    @BindView(R.id.change_password)
    TextView changePassword;
    @BindView(R.id.change_password___)
    RelativeLayout changePassword__;
    @BindView(R.id.privacy_policy)
    TextView privacyPolicy;
    @BindView(R.id.privacy_policy_layout)
    RelativeLayout privacyPolicyLayout;
    @BindView(R.id.terms)
    TextView terms;
    @BindView(R.id.terms_layout)
    RelativeLayout termsLayout;
    @BindView(R.id.delete_profile)
    TextView delete_profile;
    @BindView(R.id.delete_profile___)
    RelativeLayout delete_profile___;
    @BindView(R.id.logout)
    TextView logout;
    @BindView(R.id.logout_layout)
    RelativeLayout logoutLayout;
    @BindView(R.id.reward_points)
    TextView rewardPoints;
    @BindView(R.id.reward_point___)
    RelativeLayout rewardPoint;
    @BindView(R.id.language)
    TextView language;
    @BindView(R.id.language___)
    RelativeLayout language___;
    @BindView(R.id.time_slot)
    TextView timeSlot;
    @BindView(R.id.time_slot___)
    RelativeLayout timeSlo___t;
    @BindView(R.id.rating_bar)
    RatingBar ratingBar;
    @BindView(R.id.rating_bar_layout)
    RelativeLayout ratingBarLayout;
    private SavePref savePref;
    Unbinder unbinder;

    String latitude = "", longitude = "";
    GPSTracker gpsTracker = null;
    Resources resources;
    Context context1;

    public ProfileFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_profile, container, false);

        unbinder = ButterKnife.bind(this, view);
        context = getActivity();
        savePref = new SavePref(context);

        gpsTracker = new GPSTracker(context);

        if (savePref.getLang().equals("en"))
            language.setText(context.getResources().getString(R.string.language) + " (English)");
        else
            language.setText(context.getResources().getString(R.string.language) + " (Spanish)");


        return view;
    }


    @Override
    public void onResume() {
        super.onResume();
        setdata();
    }

    private void setdata() {
        name.setText(savePref.getName());
        email.setText(savePref.getEmail());
        phone.setText("+" + savePref.getPhone());

        Glide.with(context).load(savePref.getImage()).error(R.drawable.placeholder).into(profilePic);

        if (ConnectivityReceiver.isConnected())
            PROVIDER_RATINGS();
        else
            util.IOSDialog(context, util.internet_Connection_Error);
    }

    private void PROVIDER_RATINGS() {
        ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.TO_ID, savePref.getID());
        //formBuilder.addFormDataPart(Parameters.TYPE, "2");
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.GET_USER_RATINGS, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                try {
                    if (mDialog != null)
                        mDialog.dismiss();
                } catch (Exception e) {

                }
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {
                            JSONObject body = jsonmainObject.getJSONObject("body");

                            if (!body.getJSONObject("average_rating").getString("avg_rating").isEmpty())
                                ratingBar.setRating(Float.parseFloat(body.getJSONObject("average_rating").getString("avg_rating")));


                        } else {
                            if (jsonmainObject.getString("error_message").equals(util.Invalid_Authorization)) {
                                SavePref savePref = new SavePref(context);
                                savePref.setAuthorization_key("");
                                Intent intent = new Intent(context, SignInActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                            } else {
                                util.showToast(context, jsonmainObject.getString("msg"));
                            }
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.delete_profile, R.id.delete_profile___, R.id.language, R.id.language___, R.id.rating_bar_layout, R.id.edit_profile_text, R.id.edit_profile111, R.id.time_slot, R.id.time_slot___, R.id.reward_points, R.id.reward_point___, R.id.change_password, R.id.change_password___, R.id.privacy_policy, R.id.privacy_policy_layout, R.id.terms, R.id.terms_layout, R.id.logout, R.id.logout_layout})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.rating_bar_layout:
                Intent intent = new Intent(getActivity(), ReviewListActivity.class);
                intent.putExtra("user_id", savePref.getID());
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
            case R.id.edit_profile_text:
                startActivity(new Intent(getActivity(), UpdateProfileActivity.class));
                getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
            case R.id.edit_profile111:
                startActivity(new Intent(getActivity(), UpdateProfileActivity.class));
                getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
            case R.id.time_slot:
                startActivity(new Intent(getActivity(), TimeSlotRequestListActivity.class));
                getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
            case R.id.time_slot___:
                startActivity(new Intent(getActivity(), TimeSlotRequestListActivity.class));
                getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
            case R.id.change_password:
                startActivity(new Intent(getActivity(), ChangePasswordActivity.class));
                getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
            case R.id.change_password___:
                startActivity(new Intent(getActivity(), ChangePasswordActivity.class));
                getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
            case R.id.reward_points:
                startActivity(new Intent(getActivity(), RewardPointsActivity.class));
                getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
            case R.id.reward_point___:
                startActivity(new Intent(getActivity(), RewardPointsActivity.class));
                getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
            case R.id.privacy_policy:
                Intent intent11111 = new Intent(context, TermConditionActivity.class);
                intent11111.putExtra("type", "privacy");
                startActivity(intent11111);
                break;
            case R.id.privacy_policy_layout:
                Intent intent111112 = new Intent(context, TermConditionActivity.class);
                intent111112.putExtra("type", "privacy");
                startActivity(intent111112);
                break;
            case R.id.terms:
                Intent intent11 = new Intent(context, TermConditionActivity.class);
                intent11.putExtra("type", "term");
                startActivity(intent11);
                break;
            case R.id.terms_layout:
                Intent intent111 = new Intent(context, TermConditionActivity.class);
                intent111.putExtra("type", "term");
                startActivity(intent111);
                break;
            case R.id.logout:
                LogoutAlert();
                break;
            case R.id.logout_layout:
                LogoutAlert();
                break;
            case R.id.delete_profile:
                DELETE_PROFILE_Alert();
                break;
            case R.id.delete_profile___:
                DELETE_PROFILE_Alert();
                break;
            case R.id.language___:
                LanguageDialog();
                break;
            case R.id.language:
                LanguageDialog();
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        CardEntry.handleActivityResult(data, result -> {
            if (result.isSuccess()) {
                CardDetails cardResult = result.getSuccessValue();
                Card card = cardResult.getCard();
                String nonce = cardResult.getNonce();


                Log.e("nonce_data", nonce);
                Toast.makeText(context,
                        "Successfully",
                        Toast.LENGTH_SHORT)
                        .show();
            } else if (result.isCanceled()) {
                Toast.makeText(context,
                        "Canceled",
                        Toast.LENGTH_SHORT)
                        .show();
            }
        });
    }

   /* public class CardEntryBackgroundHandler implements CardNonceBackgroundHandler {
        @Override
        public CardEntryActivityCommand handleEnteredCardInBackground(CardDetails cardDetails) {

            try {
                // TODO Call your backend service
                MyBackendServiceResponse response = // myBackendService(cardDetails.getNonce());...

                if (response.isSuccessful()) {
                    return new CardEntryActivityCommand.Finish();
                } else {
                    return new CardEntryActivityCommand.ShowError(response.errorMessage)
                }
            } catch(IOException exception) {
                return new CardEntryActivityCommand.ShowError(
                        resources.getString(R.string.network_failure));
            }
        }
    }*/


    private void LogoutAlert() {
        new IOSDialog.Builder(context)
                .setTitle(context.getResources().getString(R.string.app_name))
                .setMessage(R.string.are_you_sure_logout)
                .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        LOGOUT_API();
                    }
                })
                .setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).show();
    }

    //{"success":false,"message":"Invalid authorization","code":400,"body":[]}
    private void LOGOUT_API() {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        FormBody.Builder formBuilder = new FormBody.Builder();
        formBuilder.add(Parameters.AUTH_KEY, "");
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.LOGOUT, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonObject = new JSONObject(result);
                        if (jsonObject.getString("code").equalsIgnoreCase("200")) {
                            savePref.setAuthorization_key("");
                            util.showToast(context, getString(R.string.user_logout_sucessfully));
                            Intent intent = new Intent(context, SignInActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                        } else {
                            if (jsonObject.getString("msg").equals(util.Invalid_Authorization)) {
                                savePref.setAuthorization_key("");
                                Intent intent = new Intent(context, SignInActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                            } else {
                                util.showToast(context, jsonObject.getString("msg"));
                            }
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    private void DELETE_PROFILE_Alert() {
        new IOSDialog.Builder(context)
                .setTitle(context.getResources().getString(R.string.app_name))
                .setMessage(R.string.sure_to_delete_profile)
                .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        DELETE_PROFILE_API();
                    }
                })
                .setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).show();
    }

    private void DELETE_PROFILE_API() {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        FormBody.Builder formBuilder = new FormBody.Builder();
        formBuilder.add(Parameters.AUTH_KEY, "");
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.DELETE_PROFILE, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonObject = new JSONObject(result);
                        if (jsonObject.getString("code").equalsIgnoreCase("200")) {
                            savePref.setAuthorization_key("");
                            util.showToast(context, getString(R.string.account_removed_successfully));
                            Intent intent = new Intent(context, SignInActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                        } else {
                            if (jsonObject.getString("msg").equals(util.Invalid_Authorization)) {
                                savePref.setAuthorization_key("");
                                Intent intent = new Intent(context, SignInActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                            } else {
                                util.showToast(context, jsonObject.getString("msg"));
                            }
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    private void LanguageDialog() {
        ArrayList<String> list = new ArrayList<>();
        list.add("English");
        list.add("Spanish");
        final CharSequence[] Animals = list.toArray(new String[list.size()]);
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        dialogBuilder.setTitle(context.getResources().getString(R.string.select_language));
        dialogBuilder.setItems(Animals, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                if (item == 0) {
                    savePref.setLang("en");
                    language.setText(context.getResources().getString(R.string.language) + " (English)");
                    /*language.setCompoundDrawablesWithIntrinsicBounds(getContext().getResources().getDrawable(R.drawable.flag),
                            null, getContext().getResources().getDrawable(R.drawable.arrow_d), null);*/
                    dialog.dismiss();
                    LANGUAGE_CHANGE_API("1");
                } else {
                    savePref.setLang("es");
                    language.setText(context.getResources().getString(R.string.language) + " (Spanish)");
                   /* language.setCompoundDrawablesWithIntrinsicBounds(getContext().getResources().getDrawable(R.drawable.romania_flag),
                            null, getContext().getResources().getDrawable(R.drawable.arrow_d), null);*/
                    dialog.dismiss();
                    LANGUAGE_CHANGE_API("2");
                }


            }
        });
        //Create alert dialog object via builder
        AlertDialog alertDialogObject = dialogBuilder.create();
        //Show the dialog
        alertDialogObject.show();
    }

    private void LANGUAGE_CHANGE_API(String type) {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        FormBody.Builder formBuilder = new FormBody.Builder();
        formBuilder.add(Parameters.TYPE, type);//1=eng , 2=spanish
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.LANGUAGE_CHANGE, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonObject = new JSONObject(result);
                        if (jsonObject.getString("code").equalsIgnoreCase("200")) {
                            context1 = LocaleHelper.setLocale(context, savePref.getLang());
                            resources = context1.getResources();
                            Intent intent = new Intent(context, MainActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                        } else {
                            if (jsonObject.getString("msg").equals(util.Invalid_Authorization)) {
                                savePref.setAuthorization_key("");
                                Intent intent = new Intent(context, SignInActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                            } else {
                                util.showToast(context, jsonObject.getString("msg"));
                            }
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

}
