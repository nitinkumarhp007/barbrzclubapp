package com.ez_schedule.services;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.util.Log;

import androidx.core.app.NotificationCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.ez_schedule.Activities.OrderDetailActivity;
import com.ez_schedule.Activities.TimeSlotRequestListActivity;
import com.ez_schedule.Activities.UpdateProfileActivity;
import com.ez_schedule.BarbarActivities.BarbarMainActivity;
import com.ez_schedule.MainActivity;
import com.ez_schedule.R;
import com.ez_schedule.Util.SavePref;
import com.ez_schedule.Util.util;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URL;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";
    SavePref savePref;
    private static int i;
    private static int value = 0;

    String message = "", provider_id = "", image = "", order_id = "", offer_status = "", offer_amount = "", notification_code = "", product_id = "", username = "", sender_id = "", chat_message, timeStamp;

    String CHANNEL_ID = "";// The id of the channel.
    String CHANNEL_ONE_NAME = "Channel One";
    NotificationChannel notificationChannel;
    public static NotificationManager notificationManager;
    public static Notification.InboxStyle inboxStyle = new Notification.InboxStyle();
    Notification notification;


    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        JSONObject obj = null;
        savePref = new SavePref(getApplicationContext());
        Log.e(TAG, "Notification Message Body: " + remoteMessage.getData());


        notification_code = remoteMessage.getData().get("type");

        getManager();
        CHANNEL_ID = getApplicationContext().getPackageName();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            notificationChannel = new NotificationChannel(CHANNEL_ID, CHANNEL_ONE_NAME, notificationManager.IMPORTANCE_DEFAULT);
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.setShowBadge(true);
            notificationChannel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);

        }

      /*  Book barber =>1111
        accpet request =>1112
        reject request =>1113
        Payment done and  order placed =>1114
        Mark as complete =>1115
        Swap request add=>1116*/

        if (notification_code.equals("1111")) {//Request to saloon
            try {
                JSONObject object = new JSONObject(remoteMessage.getData().get("notification"));
                message = object.getString("title");
                sendNotification(getApplicationContext(), message);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else if (notification_code.equals("1114") || notification_code.equals("1122") || notification_code.equals("1123")) {//payment done ,swipe accept, swipe decline
            try {
                JSONObject object = new JSONObject(remoteMessage.getData().get("notification"));
                JSONObject object1 = new JSONObject(remoteMessage.getData().get("body"));
                message = object.getString("title");

                if (notification_code.equals("1114"))
                    order_id = object1.getJSONObject("data").getString("id");
                else
                    order_id = object1.getJSONObject("data").getString("from_order_id");

                sendNotification(getApplicationContext(), message);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            try {
                JSONObject object = new JSONObject(remoteMessage.getData().get("notification"));
                message = object.getString("title");
                sendNotification(getApplicationContext(), message);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }


    }

    @Override
    public void onNewToken(String token) {
        super.onNewToken(token);
        SavePref.setDeviceToken(getApplicationContext(), "token", token);
    }

    private void sendNotification(Context context, String message) {
        Intent intent = null;
        PendingIntent pendingIntent;

        if (notification_code.equals("1111")) {
            intent = new Intent(context, BarbarMainActivity.class);

        } else if (notification_code.equals("1112") || notification_code.equals("1113")) {
            intent = new Intent(context, MainActivity.class);
            intent.putExtra("to_request", true);
        } else if (notification_code.equals("1114") || notification_code.equals("1122") | notification_code.equals("1123")) {//Payment done and  order placed , swipe accept ,swipe decline
            intent = new Intent(context, OrderDetailActivity.class);
            if (notification_code.equals("1122") || notification_code.equals("1123"))
                intent.putExtra("barber_side", false);
            else
                intent.putExtra("barber_side", true);
            intent.putExtra("order_id", order_id);
        } else if (notification_code.equals("1116")) {//swipe request send
            intent = new Intent(context, TimeSlotRequestListActivity.class);
        } else {
            intent = new Intent(context, MainActivity.class);
        }
        intent.putExtra("notification_code", notification_code);
        intent.putExtra("is_from_push", true);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        pendingIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        Bitmap image_bitmap = null;
        try {
            URL url = new URL(image);
            image_bitmap = BitmapFactory.decodeStream(url.openConnection().getInputStream());
        } catch (IOException e) {
            System.out.println(e);
        }

        Bitmap icon1 = BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

       /* Notification.Builder notificationBuilder = new Notification.Builder(context)
                .setSmallIcon(R.mipmap.ic_launcher).setLargeIcon(image_bitmap)
                .setContentTitle(message)
                .setOngoing(false)
                .setContentText("")
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);*/

        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this, CHANNEL_ID)
                        .setSmallIcon(R.mipmap.ic_launcher).setLargeIcon(icon1)
                        .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                        .setContentTitle(context.getResources().getString(R.string.app_name))
                        .setContentText(message)
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            notificationBuilder.setChannelId(CHANNEL_ID);
            notificationManager.createNotificationChannel(notificationChannel);
        }
        notification = notificationBuilder.build();

        notificationManager.notify(i++, notification);


    }

    private NotificationManager getManager() {
        if (notificationManager == null) {
            notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        }
        return notificationManager;
    }


    // offer_status
    private void publishResultsMessage(String message, String created, String voice_length, String username, String message_id, String message_type) {

        Intent intent = new Intent(util.NOTIFICATION_MESSAGE);
        intent.putExtra("message", message);
        intent.putExtra("created", created);
        intent.putExtra("username", username);
        intent.putExtra("message_id", message_id);
        intent.putExtra("voice_length", voice_length);
        intent.putExtra("message_type", message_type);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

}
