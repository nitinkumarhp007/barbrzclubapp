package com.ez_schedule.BarbarAdapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.ez_schedule.BarbarActivities.AddServiceActivity;
import com.ez_schedule.BarbarFragments.BarbarServicesFragment;
import com.ez_schedule.ModelClasses.BarbarServiceListModel;
import com.ez_schedule.ModelClasses.RequestModel;
import com.ez_schedule.R;
import com.ez_schedule.Util.util;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class BarbarServicesAdapter extends RecyclerView.Adapter<BarbarServicesAdapter.RecyclerViewHolder> {
    Context context;
    LayoutInflater Inflater;
    private View view;
    ArrayList<BarbarServiceListModel> list;
    ArrayList<BarbarServiceListModel> tempList;
    BarbarServicesFragment barbarServicesFragment;

    public BarbarServicesAdapter(Context context, ArrayList<BarbarServiceListModel> list, BarbarServicesFragment barbarServicesFragment) {
        this.context = context;
        this.list = list;
        this.tempList = list;
        this.barbarServicesFragment = barbarServicesFragment;
        Inflater = LayoutInflater.from(context);

    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = Inflater.inflate(R.layout.barbar_services_row, parent, false);
        RecyclerViewHolder viewHolder = new RecyclerViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, final int position) {
        holder.name.setText(list.get(position).getName());
        holder.price.setText("$" + list.get(position).getPrice());

        String duration_text = list.get(position).getDuration();

        if (duration_text.equals("1"))
            holder.duration.setText("15 Mins");
        else if (duration_text.equals("2"))
            holder.duration.setText("30 Mins");
        else if (duration_text.equals("3"))
            holder.duration.setText("45 Mins");
        else if (duration_text.equals("4"))
            holder.duration.setText("1 Hour");
        else if (duration_text.equals("5"))
            holder.duration.setText("1 Hour 15 Mins");
        else if (duration_text.equals("6"))
            holder.duration.setText("1 Hour 30 Mins");
        else if (duration_text.equals("7"))
            holder.duration.setText("1 Hour 45 Mins");
        else if (duration_text.equals("8"))
            holder.duration.setText("2 Hours");

        Glide.with(context)
                .load(list.get(position).getImage())
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        holder.progressBar.setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        holder.progressBar.setVisibility(View.GONE);
                        return false;
                    }
                })
                .into(holder.image);


        holder.deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                barbarServicesFragment.DeleteAlert(position);
            }
        });
        holder.editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                util.hideKeyboard(barbarServicesFragment.getActivity());
                Intent intent = new Intent(context, AddServiceActivity.class);
                intent.putExtra("from_edit", true);
                intent.putExtra("data", list.get(position));
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.image)
        ImageView image;
        @BindView(R.id.progress_bar)
        ProgressBar progressBar;
        @BindView(R.id.price)
        TextView price;
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.duration)
        TextView duration;
        @BindView(R.id.edit_button)
        ImageView editButton;
        @BindView(R.id.delete_button)
        ImageView deleteButton;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }

    public void filter(String charText) {
        charText = charText.toLowerCase();
        ArrayList<BarbarServiceListModel> nList = new ArrayList<BarbarServiceListModel>();
        if (charText.length() == 0) {
            nList.addAll(tempList);
        } else {
            for (BarbarServiceListModel wp : tempList) {
                if (wp.getName().toLowerCase().contains(charText.toLowerCase()))//contains for less accurate result nd matches for accurate result
                {
                    nList.add(wp);
                }
            }
        }
        list = nList;
        notifyDataSetChanged();
    }
}
