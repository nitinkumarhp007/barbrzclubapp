package com.ez_schedule.BarbarAdapters;

import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ez_schedule.BarbarFragments.HomeBarbarFragment;
import com.ez_schedule.ModelClasses.RequestModel;
import com.ez_schedule.R;
import com.ez_schedule.Util.ConnectivityReceiver;
import com.ez_schedule.Util.util;
import com.bumptech.glide.Glide;
import com.ligl.android.widget.iosdialog.IOSDialog;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;


public class RequetsAdapter extends RecyclerView.Adapter<RequetsAdapter.RecyclerViewHolder> {
    Context context;
    LayoutInflater Inflater;
    private View view;
    ArrayList<RequestModel> list;
    ArrayList<RequestModel> tempList;
    HomeBarbarFragment homeBarbarFragment;

    public RequetsAdapter(Context context, ArrayList<RequestModel> list, HomeBarbarFragment homeBarbarFragment) {
        this.context = context;
        this.list = list;
        this.tempList = list;
        this.homeBarbarFragment = homeBarbarFragment;
        Inflater = LayoutInflater.from(context);

    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = Inflater.inflate(R.layout.requests_row, parent, false);
        RecyclerViewHolder viewHolder = new RecyclerViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, final int position) {

        if (list.get(position).getStatus().equals("0")) {
            holder.accept.setVisibility(View.VISIBLE);
            holder.decline.setVisibility(View.VISIBLE);
            holder.status.setVisibility(View.GONE);
        } else {
            holder.accept.setVisibility(View.GONE);
            holder.status.setVisibility(View.VISIBLE);

            if (list.get(position).getStatus().equals("1")) {
                holder.status.setText(R.string.accepted);
                holder.decline.setVisibility(View.VISIBLE);
                holder.decline.setText(R.string.cancel);
            } else if (list.get(position).getStatus().equals("2")) {
                holder.status.setText(R.string.declined);
                holder.decline.setVisibility(View.GONE);
            } else if (list.get(position).getStatus().equals("5")) {
                holder.status.setText(R.string.cancelled);
                holder.decline.setVisibility(View.GONE);
            }
        }

        holder.name.setText(list.get(position).getUsername());
        holder.price.setText(context.getString(R.string.total) + list.get(position).getCost());
        if (list.get(position).getInfo().isEmpty())
            holder.special.setVisibility(View.GONE);
        else {
            holder.special.setVisibility(View.VISIBLE);
            holder.special.setText(R.string.additional_info + list.get(position).getInfo());
        }

        // holder.time_slot.setText();
        holder.date.setText("Fecha: " + util.convertTimeStampDate(Long.parseLong(list.get(position).getDate())) + " | " + list.get(position).getSlot_Detail());
        Glide.with(context).load(list.get(position).getImage()).error(R.drawable.logo_new).into(holder.image);

        holder.myRecyclerView.setLayoutManager(new LinearLayoutManager(context));
        holder.myRecyclerView.setAdapter(new BarbarrequestservicesAdapter(context, list.get(position).getList()));


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*Intent intent = new Intent(context, GroupDetailActivity.class);
                intent.putExtra("group_id", list.get(position).getId());
                context.startActivity(intent);
                MainActivity.context.overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);*/
            }
        });

        holder.accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                homeBarbarFragment.BARBER_REQURST_STATUS_API(list.get(position).getId(), "1");
            }
        });
        holder.decline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (list.get(position).getStatus().equals("1")) {
                    //util.IOSDialog(context, "Cancel Request");

                    new IOSDialog.Builder(context)
                            .setTitle(context.getResources().getString(R.string.app_name))
                            .setMessage("Are you sure to cancel?")
                            .setPositiveButton(context.getString(R.string.yes), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    if (ConnectivityReceiver.isConnected()) {
                                        homeBarbarFragment.CANCEL_ORDER_API(list.get(position).getId(), position);
                                    } else {
                                        util.IOSDialog(context, util.internet_Connection_Error);
                                    }
                                }
                            })
                            .setNegativeButton(context.getString(R.string.no), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            }).show();
                } else {
                    homeBarbarFragment.BARBER_REQURST_STATUS_API(list.get(position).getId(), "2");
                }

            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.image)
        CircleImageView image;
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.my_recycler_view)
        RecyclerView myRecyclerView;
        @BindView(R.id.price)
        TextView price;
        @BindView(R.id.accept)
        TextView accept;
        @BindView(R.id.status)
        TextView status;
        @BindView(R.id.decline)
        TextView decline;
        @BindView(R.id.time_slot)
        TextView time_slot;
        @BindView(R.id.time)
        TextView time;
        @BindView(R.id.date)
        TextView date;
        @BindView(R.id.special)
        TextView special;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }

    public void filter(String charText) {
        charText = charText.toLowerCase();
        ArrayList<RequestModel> nList = new ArrayList<RequestModel>();
        if (charText.length() == 0) {
            nList.addAll(tempList);
        } else {
            for (RequestModel wp : tempList) {
                if (wp.getUsername().toLowerCase().contains(charText.toLowerCase()))//contains for less accurate result nd matches for accurate result
                {
                    nList.add(wp);
                }
            }
        }
        list = nList;
        notifyDataSetChanged();
    }

}
