package com.ez_schedule.BarbarAdapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.ez_schedule.Activities.OrderDetailActivity;
import com.ez_schedule.ModelClasses.UserOrdersModel;
import com.ez_schedule.R;
import com.ez_schedule.Util.util;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;


public class BarbarOrdersAdapter extends RecyclerView.Adapter<BarbarOrdersAdapter.RecyclerViewHolder> {
    Context context;
    LayoutInflater Inflater;
    private View view;
    ArrayList<UserOrdersModel> list;

    public BarbarOrdersAdapter(Context context, ArrayList<UserOrdersModel> list, String is_upcoming) {
        this.context = context;
        this.list = list;
        Inflater = LayoutInflater.from(context);

    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = Inflater.inflate(R.layout.barbar_orders_row, parent, false);
        RecyclerViewHolder viewHolder = new RecyclerViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, final int position) {
        Glide.with(context).load(list.get(position).getImage()).into(holder.image);

        holder.name.setText(list.get(position).getUsername());
        holder.price.setText("$" + list.get(position).getCost());
        holder.time.setText(context.getString(R.string.time_slot_) + list.get(position).getSlot_Detail());
        holder.date.setText(context.getString(R.string.date) + util.convertTimeStampDate(Long.parseLong(list.get(position).getDate())));

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!list.get(position).getStatus().equals("1")) {
                    Intent intent = new Intent(context, OrderDetailActivity.class);
                    intent.putExtra("data", list.get(position));
                    intent.putExtra("barber_side", true);
                    context.startActivity(intent);
                }

            }
        });
        if (list.get(position).getStatus().equals("5")) {
            holder.status.setText(context.getString(R.string.cancelled));
        } else if (list.get(position).getStatus().equals("4")) {
            holder.status.setText(R.string.completed);
        } else if (list.get(position).getStatus().equals("1")) {
            holder.status.setText(R.string.accepted);
        } else {
            holder.status.setText(R.string.booked);
        }


    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.image)
        CircleImageView image;
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.date)
        TextView date;
        @BindView(R.id.time)
        TextView time;
        @BindView(R.id.price)
        TextView price;
        @BindView(R.id.status)
        TextView status;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}
