package com.ez_schedule.BarbarAdapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.ez_schedule.Activities.TimeSlotRequestListActivity;
import com.ez_schedule.ModelClasses.SwapRequestModel;
import com.ez_schedule.R;
import com.ez_schedule.Util.util;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;


public class BarbarSlotSwapAdapter extends RecyclerView.Adapter<BarbarSlotSwapAdapter.RecyclerViewHolder> {
    TimeSlotRequestListActivity context;
    LayoutInflater Inflater;
    private View view;
    ArrayList<SwapRequestModel> list;

    public BarbarSlotSwapAdapter(TimeSlotRequestListActivity context, ArrayList<SwapRequestModel> list) {
        this.context = context;
        this.list = list;
        Inflater = LayoutInflater.from(context);

    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = Inflater.inflate(R.layout.barbar_slot_swap_row, parent, false);
        RecyclerViewHolder viewHolder = new RecyclerViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, final int position) {

        holder.name.setText(list.get(position).getTo_request());
        holder.nameTo.setText(list.get(position).getFrom_request());

        holder.date_from.setText(util.convertTimeStampDate(Long.parseLong(list.get(position).getDate())));
        holder.date_to.setText(util.convertTimeStampDate(Long.parseLong(list.get(position).getDate_to())));


        Glide.with(context).load(list.get(position).getImage()).into(holder.image);

        holder.nameBarbar.setText(list.get(position).getName());
        holder.price.setText("$" + list.get(position).getPrice());
        holder.time.setText(context.getString(R.string.time_slot_) + list.get(position).getTo_request());
        holder.date.setText(context.getString(R.string.date) + util.convertTimeStampDate(Long.parseLong(list.get(position).getDate())));

        holder.accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                context.SWAP_REQUEST_STATUS_API(list.get(position).getId(), "1");
            }
        });
        holder.decline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.SWAP_REQUEST_STATUS_API(list.get(position).getId(), "2");
            }
        });

        if (list.get(position).getStatus().equals("0")) {

            holder.accept.setVisibility(View.VISIBLE);
            holder.decline.setVisibility(View.VISIBLE);
            holder.status.setVisibility(View.GONE);

        } else if (list.get(position).getStatus().equals("1")) {
            holder.accept.setVisibility(View.GONE);
            holder.decline.setVisibility(View.GONE);
            holder.status.setVisibility(View.VISIBLE);
            holder.status.setText(R.string.accepted);
        } else if (list.get(position).getStatus().equals("2")) {
            holder.accept.setVisibility(View.GONE);
            holder.decline.setVisibility(View.GONE);
            holder.status.setVisibility(View.VISIBLE);
            holder.status.setText(R.string.declined);
        }


    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.name_to)
        TextView nameTo;
        @BindView(R.id.accept)
        TextView accept;
        @BindView(R.id.status)
        TextView status;
        @BindView(R.id.decline)
        TextView decline;

        @BindView(R.id.image)
        CircleImageView image;
        @BindView(R.id.name_barbar)
        TextView nameBarbar;
        @BindView(R.id.date)
        TextView date;
        @BindView(R.id.time)
        TextView time;
        @BindView(R.id.price)
        TextView price;
        @BindView(R.id.date_from)
        TextView date_from;
        @BindView(R.id.date_to)
        TextView date_to;


        public RecyclerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}
