package com.ez_schedule.BarbarAdapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.ez_schedule.ModelClasses.BarbarServiceListModel;
import com.ez_schedule.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class BarbarrequestservicesAdapter extends RecyclerView.Adapter<BarbarrequestservicesAdapter.RecyclerViewHolder> {
    Context context;
    LayoutInflater Inflater;
    private View view;
    ArrayList<BarbarServiceListModel> list;

    public BarbarrequestservicesAdapter(Context context, ArrayList<BarbarServiceListModel> list) {
        this.list = list;
        this.context = context;
        Inflater = LayoutInflater.from(context);

    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = Inflater.inflate(R.layout.row_service, parent, false);
        RecyclerViewHolder viewHolder = new RecyclerViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, final int position) {

        String duration_text = list.get(position).getDuration();

        if (duration_text.equals("1"))
            holder.title.setText(list.get(position).getName() + " : $" + list.get(position).getPrice()+ " (15 Mins)");
        else if (duration_text.equals("2"))
            holder.title.setText(list.get(position).getName()+ " : $" + list.get(position).getPrice() + " (30 Mins)");
        else if (duration_text.equals("3"))
            holder.title.setText(list.get(position).getName()+ " : $" + list.get(position).getPrice() + " (45 Mins)");
        else if (duration_text.equals("4"))
            holder.title.setText(list.get(position).getName()+ " : $" + list.get(position).getPrice() + " (1 Hour)");
        else if (duration_text.equals("5"))
            holder.title.setText(list.get(position).getName()+ " : $" + list.get(position).getPrice() + " (1 Hour 15 Mins)");
        else if (duration_text.equals("6"))
            holder.title.setText(list.get(position).getName() + " : $" + list.get(position).getPrice()+ " (1 Hour 30 Mins)");
        else if (duration_text.equals("7"))
            holder.title.setText(list.get(position).getName()+ " : $" + list.get(position).getPrice() + " (1 Hour 45 Mins)");
        else if (duration_text.equals("8"))
            holder.title.setText(list.get(position).getName()+ " : $" + list.get(position).getPrice() + " (2 Hours)");

    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.title)
        TextView title;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}
