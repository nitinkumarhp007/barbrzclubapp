package com.ez_schedule.BarbarAdapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.ez_schedule.ModelClasses.BarbarServiceListModel;
import com.ez_schedule.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class DetailservicesAdapter extends RecyclerView.Adapter<DetailservicesAdapter.RecyclerViewHolder> {
    Context context;
    LayoutInflater Inflater;
    private View view;
    ArrayList<BarbarServiceListModel> list;

    public DetailservicesAdapter(Context context, ArrayList<BarbarServiceListModel> list) {
        this.list = list;
        this.context = context;
        Inflater = LayoutInflater.from(context);

    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = Inflater.inflate(R.layout.row_detail_service, parent, false);
        RecyclerViewHolder viewHolder = new RecyclerViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, final int position) {
        holder.title.setText(list.get(position).getName() + " : $" + list.get(position).getPrice());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.title)
        TextView title;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}
