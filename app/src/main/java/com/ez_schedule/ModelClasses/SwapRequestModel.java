package com.ez_schedule.ModelClasses;

public class SwapRequestModel {
    String id="";
    String from_request="";
    String to_request="";
    String status="";
    String name="";
    String date="";
    String image="";
    String price="";
    String date_to="";


    public String getDate_to() {
        return date_to;
    }

    public void setDate_to(String date_to) {
        this.date_to = date_to;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getFrom_request() {
        return from_request;
    }

    public void setFrom_request(String from_request) {
        this.from_request = from_request;
    }

    public String getTo_request() {
        return to_request;
    }

    public void setTo_request(String to_request) {
        this.to_request = to_request;
    }
}
