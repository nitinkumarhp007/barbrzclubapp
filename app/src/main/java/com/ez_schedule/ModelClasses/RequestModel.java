package com.ez_schedule.ModelClasses;

import java.util.ArrayList;

public class RequestModel {
    String id="";
    String username="";
    String image="";
    String cost="";
    String slot_Detail="";
    String status="";
    String date="";
    String info="";
    ArrayList<BarbarServiceListModel>list;


    public String getId() {
        return id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSlot_Detail() {
        return slot_Detail;
    }

    public void setSlot_Detail(String slot_Detail) {
        this.slot_Detail = slot_Detail;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getImage() {
        return image;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    public ArrayList<BarbarServiceListModel> getList() {
        return list;
    }

    public void setList(ArrayList<BarbarServiceListModel> list) {
        this.list = list;
    }
}
