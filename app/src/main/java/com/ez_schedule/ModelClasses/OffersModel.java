package com.ez_schedule.ModelClasses;

public class OffersModel {

    String id="";
    String businesses_name="";
    String offer="";
    String image="";
    String zipcode="";

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBusinesses_name() {
        return businesses_name;
    }

    public void setBusinesses_name(String businesses_name) {
        this.businesses_name = businesses_name;
    }

    public String getOffer() {
        return offer;
    }

    public void setOffer(String offer) {
        this.offer = offer;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }
}
