package com.ez_schedule.ModelClasses;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class UserOrdersModel implements Parcelable {

    String id="";
    String username="";
    String image="";
    String cost="";
    String slot_Detail="";
    String status="";
    String date="";
    ArrayList<BarbarServiceListModel> list;

    public UserOrdersModel()
    {}

    protected UserOrdersModel(Parcel in) {
        id = in.readString();
        username = in.readString();
        image = in.readString();
        cost = in.readString();
        slot_Detail = in.readString();
        status = in.readString();
        date = in.readString();
        list = in.createTypedArrayList(BarbarServiceListModel.CREATOR);
    }

    public static final Creator<UserOrdersModel> CREATOR = new Creator<UserOrdersModel>() {
        @Override
        public UserOrdersModel createFromParcel(Parcel in) {
            return new UserOrdersModel(in);
        }

        @Override
        public UserOrdersModel[] newArray(int size) {
            return new UserOrdersModel[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ArrayList<BarbarServiceListModel> getList() {
        return list;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setList(ArrayList<BarbarServiceListModel> list) {
        this.list = list;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    public String getSlot_Detail() {
        return slot_Detail;
    }

    public void setSlot_Detail(String slot_Detail) {
        this.slot_Detail = slot_Detail;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(username);
        dest.writeString(image);
        dest.writeString(cost);
        dest.writeString(slot_Detail);
        dest.writeString(status);
        dest.writeString(date);
        dest.writeTypedList(list);
    }
}
