package com.ez_schedule.ModelClasses;

import android.os.Parcel;
import android.os.Parcelable;

public class BarbarServiceListModel implements Parcelable {

    String id = "";
    String category_id = "";
    String category_name = "";
    String name = "";
    String name_spanish = "";
    String price = "";
    String description = "";
    String image = "";
    String duration = "";
    boolean is_check = false;

    public BarbarServiceListModel()
    {}

    protected BarbarServiceListModel(Parcel in) {
        id = in.readString();
        category_id = in.readString();
        category_name = in.readString();
        name = in.readString();
        name_spanish = in.readString();
        price = in.readString();
        description = in.readString();
        image = in.readString();
        duration = in.readString();
        is_check = in.readByte() != 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(category_id);
        dest.writeString(category_name);
        dest.writeString(name);
        dest.writeString(name_spanish);
        dest.writeString(price);
        dest.writeString(description);
        dest.writeString(image);
        dest.writeString(duration);
        dest.writeByte((byte) (is_check ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<BarbarServiceListModel> CREATOR = new Creator<BarbarServiceListModel>() {
        @Override
        public BarbarServiceListModel createFromParcel(Parcel in) {
            return new BarbarServiceListModel(in);
        }

        @Override
        public BarbarServiceListModel[] newArray(int size) {
            return new BarbarServiceListModel[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public String getCategory_name() {
        return category_name;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName_spanish() {
        return name_spanish;
    }

    public void setName_spanish(String name_spanish) {
        this.name_spanish = name_spanish;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public boolean isIs_check() {
        return is_check;
    }

    public void setIs_check(boolean is_check) {
        this.is_check = is_check;
    }
}
