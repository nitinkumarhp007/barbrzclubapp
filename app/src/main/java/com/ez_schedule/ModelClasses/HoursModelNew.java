package com.ez_schedule.ModelClasses;

import java.util.ArrayList;

public class HoursModelNew {
    String title="";
    String start_time="";
    String end_time="";
    String is_close="";

    ArrayList<TimeslotModel>list;

    public String getIs_close() {
        return is_close;
    }

    public ArrayList<TimeslotModel> getList() {
        return list;
    }

    public void setList(ArrayList<TimeslotModel> list) {
        this.list = list;
    }

    public void setIs_close(String is_close) {
        this.is_close = is_close;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public String getEnd_time() {
        return end_time;
    }

    public void setEnd_time(String end_time) {
        this.end_time = end_time;
    }
}
