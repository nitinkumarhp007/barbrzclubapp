package com.ez_schedule.ModelClasses;

import android.os.Parcel;
import android.os.Parcelable;

import java.sql.Time;

public class TimeslotModel implements Parcelable {

    String id="";
    String slot="";
    String Availability="";
    String order_id="";
    String user_id="";
    String is_close="";
    String swap_request_sent="";
    String date="";
    boolean is_checked=false;

    public TimeslotModel()
    {}

    protected TimeslotModel(Parcel in) {
        id = in.readString();
        slot = in.readString();
        Availability = in.readString();
        order_id = in.readString();
        user_id = in.readString();
        is_close = in.readString();
        swap_request_sent = in.readString();
        date = in.readString();
        is_checked = in.readByte() != 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(slot);
        dest.writeString(Availability);
        dest.writeString(order_id);
        dest.writeString(user_id);
        dest.writeString(is_close);
        dest.writeString(swap_request_sent);
        dest.writeString(date);
        dest.writeByte((byte) (is_checked ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<TimeslotModel> CREATOR = new Creator<TimeslotModel>() {
        @Override
        public TimeslotModel createFromParcel(Parcel in) {
            return new TimeslotModel(in);
        }

        @Override
        public TimeslotModel[] newArray(int size) {
            return new TimeslotModel[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSlot() {
        return slot;
    }

    public String getOrder_id() {
        return order_id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getSwap_request_sent() {
        return swap_request_sent;
    }

    public void setSwap_request_sent(String swap_request_sent) {
        this.swap_request_sent = swap_request_sent;
    }

    public String getIs_close() {
        return is_close;
    }

    public void setIs_close(String is_close) {
        this.is_close = is_close;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public void setSlot(String slot) {
        this.slot = slot;
    }

    public boolean isIs_checked() {
        return is_checked;
    }

    public String getAvailability() {
        return Availability;
    }

    public void setAvailability(String availability) {
        Availability = availability;
    }

    public void setIs_checked(boolean is_checked) {
        this.is_checked = is_checked;
    }
}
