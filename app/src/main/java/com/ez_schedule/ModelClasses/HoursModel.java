package com.ez_schedule.ModelClasses;

public class HoursModel {
    public HoursModel(String title, String start_time, String end_time,String is_close) {
        this.title = title;
        this.start_time = start_time;
        this.end_time = end_time;
        this.is_close = is_close;
    }

    String title="";
    String start_time="";
    String end_time="";
    String is_close="";

    public String getIs_close() {
        return is_close;
    }

    public void setIs_close(String is_close) {
        this.is_close = is_close;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public String getEnd_time() {
        return end_time;
    }

    public void setEnd_time(String end_time) {
        this.end_time = end_time;
    }
}
