package com.ez_schedule.ModelClasses;

import android.os.Parcel;
import android.os.Parcelable;

public class BarbarModel implements Parcelable {
    String id="";
    String username="";
    String avg_rating="";
    String profile_image="";
    String phone="";
    String email="";
    String address="";
    String description="";
    String is_fav="";
    String price="";

    public BarbarModel()
    {}

    protected BarbarModel(Parcel in) {
        id = in.readString();
        username = in.readString();
        avg_rating = in.readString();
        profile_image = in.readString();
        phone = in.readString();
        email = in.readString();
        address = in.readString();
        description = in.readString();
        is_fav = in.readString();
        price = in.readString();
    }

    public static final Creator<BarbarModel> CREATOR = new Creator<BarbarModel>() {
        @Override
        public BarbarModel createFromParcel(Parcel in) {
            return new BarbarModel(in);
        }

        @Override
        public BarbarModel[] newArray(int size) {
            return new BarbarModel[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getAvg_rating() {
        return avg_rating;
    }

    public void setAvg_rating(String avg_rating) {
        this.avg_rating = avg_rating;
    }

    public String getProfile_image() {
        return profile_image;
    }

    public void setProfile_image(String profile_image) {
        this.profile_image = profile_image;
    }

    public String getPhone() {
        return phone;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIs_fav() {
        return is_fav;
    }

    public void setIs_fav(String is_fav) {
        this.is_fav = is_fav;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(username);
        dest.writeString(avg_rating);
        dest.writeString(profile_image);
        dest.writeString(phone);
        dest.writeString(email);
        dest.writeString(address);
        dest.writeString(description);
        dest.writeString(is_fav);
        dest.writeString(price);
    }
}
