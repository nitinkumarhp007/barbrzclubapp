package com.ez_schedule.ModelClasses;

import android.os.Parcel;
import android.os.Parcelable;

public class ReviewModel implements Parcelable {
    String id="";
    String image="";
    String name="";
    String rating="";
    String text="";
    String type="";
    String name_product="";
    String created="";

    public ReviewModel()
    {}


    protected ReviewModel(Parcel in) {
        id = in.readString();
        image = in.readString();
        name = in.readString();
        rating = in.readString();
        text = in.readString();
        type = in.readString();
        name_product = in.readString();
        created = in.readString();
    }

    public static final Creator<ReviewModel> CREATOR = new Creator<ReviewModel>() {
        @Override
        public ReviewModel createFromParcel(Parcel in) {
            return new ReviewModel(in);
        }

        @Override
        public ReviewModel[] newArray(int size) {
            return new ReviewModel[size];
        }
    };

    public String getId() {
        return id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName_product() {
        return name_product;
    }

    public void setName_product(String name_product) {
        this.name_product = name_product;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(image);
        dest.writeString(name);
        dest.writeString(rating);
        dest.writeString(text);
        dest.writeString(type);
        dest.writeString(name_product);
        dest.writeString(created);
    }
}
