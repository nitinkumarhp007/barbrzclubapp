package com.ez_schedule.BarbarActivities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.ez_schedule.BarbarFragments.BarbarOrdersFragment;
import com.ez_schedule.BarbarFragments.BarbarServicesFragment;
import com.ez_schedule.BarbarFragments.CalenderFragment;
import com.ez_schedule.BarbarFragments.HomeBarbarFragment;
import com.ez_schedule.BarbarFragments.ProfileBarbarFragment;
import com.ez_schedule.MainActivity;
import com.ez_schedule.R;
import com.ez_schedule.UserFragments.FavoriteFragment;
import com.ez_schedule.UserFragments.FeedFragment;
import com.ez_schedule.UserFragments.HomeFragment;
import com.ez_schedule.UserFragments.OrdersFragment;
import com.ez_schedule.UserFragments.ProfileFragment;
import com.ez_schedule.Util.ConnectivityReceiver;
import com.ez_schedule.Util.Parameters;
import com.ez_schedule.Util.SavePref;
import com.ez_schedule.Util.util;
import com.ez_schedule.parser.AllAPIS;
import com.ez_schedule.parser.GetAsync;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.ligl.android.widget.iosdialog.IOSDialog;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import sqip.Card;
import sqip.CardDetails;
import sqip.CardEntry;

import static com.ez_schedule.parser.AllAPIS.ORDER_PAYMENT;
import static sqip.CardEntry.DEFAULT_CARD_ENTRY_REQUEST_CODE;

public class BarbarMainActivity extends AppCompatActivity {

    @BindView(R.id.title)
    TextView title;
    private SavePref savePref;
    public static Toolbar toolbar;
    public static BarbarMainActivity context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_barbar_main);
        ButterKnife.bind(this);

        context = BarbarMainActivity.this;
        savePref = new SavePref(context);
        toolbar = (Toolbar) findViewById(R.id.toolbar);


        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        navigation.setSelectedItemId(R.id.navigation_event);

        Log.e("token____", SavePref.getDeviceToken(this, "token"));




    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            toolbar.setVisibility(View.VISIBLE);
            switch (item.getItemId()) {
                case R.id.navigation_event:
                    title.setText(getString(R.string.home));
                    loadFragment(new HomeBarbarFragment());
                    return true;
                case R.id.navigation_notification:
                    title.setText(getString(R.string.services));
                    loadFragment(new BarbarServicesFragment());
                    return true;
                case R.id.navigation_chat:
                    title.setText(getString(R.string.orders));
                    loadFragment(new BarbarOrdersFragment());
                    return true;
                case R.id.navigation_calender:
                    toolbar.setVisibility(View.GONE);
                    title.setText(getString(R.string.calender));
                    loadFragment(new CalenderFragment());
                    return true;
                case R.id.navigation_profile:
                    title.setText(getString(R.string.profile));
                    loadFragment(new ProfileBarbarFragment());
                    return true;
            }
            return false;
        }
    };

    private void loadFragment(Fragment fragment) {
        // load fragment
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_container, fragment);
        transaction.commit();
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        for (Fragment fragment : getSupportFragmentManager().getFragments()) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }
    }


}