package com.ez_schedule.BarbarActivities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.ez_schedule.Activities.SignInActivity;
import com.ez_schedule.BarbarAdapters.BarbarOrdersAdapter;
import com.ez_schedule.BarbarAdapters.TrackOrdersAdapter;
import com.ez_schedule.ModelClasses.BarbarServiceListModel;
import com.ez_schedule.ModelClasses.UserOrdersModel;
import com.ez_schedule.R;
import com.ez_schedule.UserAdapters.OrderAdapter;
import com.ez_schedule.Util.ConnectivityReceiver;
import com.ez_schedule.Util.Parameters;
import com.ez_schedule.Util.SavePref;
import com.ez_schedule.Util.util;
import com.ez_schedule.parser.AllAPIS;
import com.ez_schedule.parser.GetAsync;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class TrackSaleActivity extends AppCompatActivity {
    TrackSaleActivity context;
    private SavePref savePref;

    @BindView(R.id.net_income)
    TextView net_income;
    @BindView(R.id.orders_completed)
    TextView orders_completed;
    @BindView(R.id.order_ongoing)
    TextView order_ongoing;
    @BindView(R.id.filter)
    TextView filter;
    @BindView(R.id.error_text)
    TextView error_text;
    @BindView(R.id.tabLayout)
    LinearLayout tabLayout;
    @BindView(R.id.my_recycler_view)
    RecyclerView myRecyclerView;

    ArrayList<UserOrdersModel> list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_track_sale);

        ButterKnife.bind(this);

        context = TrackSaleActivity.this;
        savePref = new SavePref(context);


        filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//Creating the instance of PopupMenu
                PopupMenu popup = new PopupMenu(TrackSaleActivity.this, filter);
                //Inflating the Popup using xml file
                popup.getMenuInflater().inflate(R.menu.option, popup.getMenu());

                //registering popup with OnMenuItemClickListener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {
                        filter.setText(item.getTitle());

                        if (item.getTitle().equals("Today")) {
                            if (ConnectivityReceiver.isConnected())
                                TRACK_SALE_LIST("1");
                            else
                                util.IOSDialog(context, util.internet_Connection_Error);
                        } else if (item.getTitle().equals("Weekly")) {
                            if (ConnectivityReceiver.isConnected())
                                TRACK_SALE_LIST("2");
                            else
                                util.IOSDialog(context, util.internet_Connection_Error);
                        } else if (item.getTitle().equals("Monthly")) {
                            if (ConnectivityReceiver.isConnected())
                                TRACK_SALE_LIST("3");
                            else
                                util.IOSDialog(context, util.internet_Connection_Error);
                        }
                        return true;
                    }
                });
                popup.show();
            }
        });

        setToolbar();


    }

    @Override
    protected void onResume() {
        super.onResume();
        filter.setText("Monthly");
        if (ConnectivityReceiver.isConnected())
            TRACK_SALE_LIST("3");
        else
            util.IOSDialog(context, util.internet_Connection_Error);
    }

    private void TRACK_SALE_LIST(String type) {
        ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.TYPE, type);//1=Today,2 =week, 3 =month
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.TRACK_SALE, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                list = new ArrayList<>();
                if (list.size() > 0)
                    list.clear();

                Log.e("data___", type);
                Log.e("data___", result);

                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {
                            JSONObject jsonObject = jsonmainObject.getJSONObject("body");

                            orders_completed.setText(jsonObject.optString("complete_orders"));
                            order_ongoing.setText(jsonObject.optString("upcoming"));
                            net_income.setText("$" + jsonObject.optString("sale_amount"));

                            tabLayout.setVisibility(View.VISIBLE);


                            JSONArray body = jsonObject.getJSONArray("orders");
                            for (int i = 0; i < body.length(); i++) {
                                JSONObject object = body.getJSONObject(i);
                                UserOrdersModel requestModel = new UserOrdersModel();
                                requestModel.setStatus(object.getJSONObject("order").getString("status"));
                                requestModel.setDate(object.getJSONObject("order").getString("date"));
                                requestModel.setCost(object.getJSONObject("order").getString("total_amount"));
                                requestModel.setId(object.getJSONObject("order").getString("id"));
                                requestModel.setImage(object.getJSONObject("user").getString("profile_image"));
                                requestModel.setUsername(object.getJSONObject("user").getString("username"));

                                requestModel.setSlot_Detail(object.getJSONObject("slot_Detail").getString("start_time") + "-" + object.getJSONObject("slot_Detail").getString("end_time"));
                                ArrayList<BarbarServiceListModel> list_inner = new ArrayList<>();
                                for (int j = 0; j < object.getJSONArray("services_detail").length(); j++) {
                                    JSONObject obj = object.getJSONArray("services_detail").getJSONObject(j);
                                    BarbarServiceListModel barbarServiceListModel = new BarbarServiceListModel();
                                    barbarServiceListModel.setId(obj.getString("id"));
                                    barbarServiceListModel.setName(obj.getString("name"));
                                    barbarServiceListModel.setPrice(obj.getString("price"));
                                    barbarServiceListModel.setDuration(obj.optString("duration"));
                                    list_inner.add(barbarServiceListModel);
                                }
                                requestModel.setList(list_inner);
                                list.add(requestModel);
                            }

                            // No cancelled & accepted


                            if (list.size() > 0) {
                                TrackOrdersAdapter adapter = new TrackOrdersAdapter(context, list);
                                myRecyclerView.setLayoutManager(new LinearLayoutManager(context));
                                myRecyclerView.setAdapter(adapter);

                                myRecyclerView.setVisibility(View.VISIBLE);
                                error_text.setVisibility(View.GONE);

                            } else {
                                error_text.setText(getString(R.string.no_order_found));
                                error_text.setVisibility(View.VISIBLE);
                                myRecyclerView.setVisibility(View.GONE);
                            }

                        } else {
                            if (jsonmainObject.getString("msg").equals(util.Invalid_Authorization)) {
                                 savePref.setAuthorization_key("");
                                Intent intent = new Intent(context, SignInActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                            } else {
                                util.showToast(context, jsonmainObject.getString("msg"));
                            }
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView title = (TextView) toolbar.findViewById(R.id.title);
        title.setText(getString(R.string.track_sale));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back_w);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }
}