package com.ez_schedule.BarbarActivities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ScrollView;

import com.ez_schedule.Activities.SignInActivity;
import com.ez_schedule.ModelClasses.CategoryModel;
import com.ez_schedule.ModelClasses.HoursModel;
import com.ez_schedule.ModelClasses.HoursModelNew;
import com.ez_schedule.ModelClasses.TimeslotModel;
import com.ez_schedule.R;
import com.ez_schedule.UserAdapters.ChangeHoursAdapter;
import com.ez_schedule.Util.ConnectivityReceiver;
import com.ez_schedule.Util.Parameters;
import com.ez_schedule.Util.SavePref;
import com.ez_schedule.Util.util;
import com.ez_schedule.parser.AllAPIS;
import com.ez_schedule.parser.GetAsync;
import com.ez_schedule.parser.GetAsyncGet;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class ChangeTimeSlotActivity extends AppCompatActivity {
    ChangeTimeSlotActivity context;
    private SavePref savePref;

    @BindView(R.id.save)
    Button save;
    @BindView(R.id.my_recycler_view)
    RecyclerView myRecyclerView;
    @BindView(R.id.back_button)
    ImageView back_button;
    @BindView(R.id.scrollView)
    ScrollView scrollView;

    ArrayList<HoursModelNew> list;

    ArrayList<String> time_list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_time_slot);

        ButterKnife.bind(this);

        context = ChangeTimeSlotActivity.this;
        savePref = new SavePref(context);


        back_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ArrayList arrayList = new ArrayList();
                ArrayList arrayList_block = new ArrayList();

                for (int i = 0; i < list.size(); i++) {
                    JSONObject object = new JSONObject();
                    try {
                        object.put("day", i + 1);
                        object.put("open_time", util.time_to_timestamp(list.get(i).getStart_time(),true));
                        object.put("close_time", util.time_to_timestamp(list.get(i).getEnd_time(),false));
                        object.put("is_close", list.get(i).getIs_close());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    arrayList.add(object);


                    for (int j = 0; j < list.get(i).getList().size(); j++) {
                        JSONObject obj = new JSONObject();
                        boolean b = false;
                        if (list.get(i).getList().get(j).isIs_checked()) {
                            b = true;
                            Log.e("here__", "top");
                            try {
                                obj.put("slot_id", list.get(i).getList().get(j).getId());
                                obj.put("is_close", "1");

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else {
                            if (list.get(i).getList().get(j).getIs_close().equals("1")) {
                                b = true;
                                Log.e("here__", "bot "+list.get(i).getList().get(j).getIs_close());
                                try {
                                    obj.put("slot_id", list.get(i).getList().get(j).getId());
                                    obj.put("is_close", "0");
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }
                        }
                        if (b)
                            arrayList_block.add(obj);
                    }
                }


                //[{"slot_id" : 73,"is_close":0},{"slot_id" : 74,"is_close":1},{"slot_id" : 75,"is_close":0}]

                Log.e("idsssss", String.valueOf(arrayList) + " ----" + String.valueOf(arrayList_block));

               /* if (ConnectivityReceiver.isConnected())
                    UPDATE_PROFILE_API(String.valueOf(arrayList), String.valueOf(arrayList_block));
                else
                    util.IOSDialog(context, util.internet_Connection_Error);*/

            }
        });


        time_list = new ArrayList<>();
        time_list.add("1:00 AM");
        time_list.add("2:00 AM");
        time_list.add("3:00 AM");
        time_list.add("4:00 AM");
        time_list.add("5:00 AM");
        time_list.add("6:00 AM");
        time_list.add("7:00 AM");
        time_list.add("8:00 AM");
        time_list.add("9:00 AM");
        time_list.add("10:00 AM");
        time_list.add("11:00 AM");
        time_list.add("12:00 AM");
        time_list.add("1:00 PM");
        time_list.add("2:00 PM");
        time_list.add("3:00 PM");
        time_list.add("4:00 PM");
        time_list.add("5:00 PM");
        time_list.add("6:00 PM");
        time_list.add("7:00 PM");
        time_list.add("8:00 PM");
        time_list.add("9:00 PM");
        time_list.add("10:00 PM");
        time_list.add("11:00 PM");
        time_list.add("12:00 PM");


        if (ConnectivityReceiver.isConnected())
            ALL_TIME_SLOTS_LIST();
        else
            util.IOSDialog(context, util.internet_Connection_Error);
    }

    private void ALL_TIME_SLOTS_LIST() {
        ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.AUTH_KEY, "");
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.ALL_TIME_SLOTS, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                list = new ArrayList<>();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {
                            JSONArray Facility = jsonmainObject.getJSONArray("body");
                            for (int i = 0; i < Facility.length(); i++) {
                                JSONObject object = Facility.getJSONObject(i);
                                HoursModelNew hoursModel = new HoursModelNew();
                                hoursModel.setTitle(object.getString("day"));
                                hoursModel.setStart_time(object.getString("start_time"));
                                hoursModel.setEnd_time(object.getString("end_time"));
                                hoursModel.setIs_close(object.getString("is_close"));
                                ArrayList<TimeslotModel> timeslot_list = new ArrayList<>();
                                for (int j = 0; j < object.getJSONArray("all_slots").length(); j++) {
                                    JSONObject obj = object.getJSONArray("all_slots").getJSONObject(i);
                                    TimeslotModel timeslotModel = new TimeslotModel();
                                    timeslotModel.setId(obj.getString("id"));
                                    timeslotModel.setIs_close(obj.optString("is_close"));
                                    timeslotModel.setSlot(obj.getString("start_time") + "-" + obj.getString("end_time"));
                                    timeslot_list.add(timeslotModel);
                                }
                                hoursModel.setList(timeslot_list);
                                list.add(hoursModel);
                            }

                            myRecyclerView.setLayoutManager(new LinearLayoutManager(context));
                            myRecyclerView.setAdapter(new ChangeHoursAdapter(context, list, time_list));

                            scrollView.setVisibility(View.VISIBLE);
                            mDialog.dismiss();

                        } else {
                            if (jsonmainObject.getString("msg").equals(util.Invalid_Authorization)) {
                                 savePref.setAuthorization_key("");
                                Intent intent = new Intent(context, SignInActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                            } else {
                                util.showToast(context, jsonmainObject.getString("msg"));
                            }
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    private void UPDATE_PROFILE_API(String schedule, String json) {
        util.hideKeyboard(context);
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.IDS, json);//:[{"slot_id" : 73,"is_close":0},{"slot_id" : 74,"is_close":1},{"slot_id" : 75,"is_close":0}]
        formBuilder.addFormDataPart(Parameters.SCHEDULE, schedule);
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.EDIT_PROFILE, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonMainobject = new JSONObject(result);
                        if (jsonMainobject.getString("code").equalsIgnoreCase("200")) {
                            util.hideKeyboard(context);
                            finish();
                            util.showToast(context, "Time Slots Updated Sucessfully!!!");

                        } else {
                            util.IOSDialog(context, jsonMainobject.getString("msg"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

}