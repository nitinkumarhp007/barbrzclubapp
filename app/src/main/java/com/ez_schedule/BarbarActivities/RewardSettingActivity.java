package com.ez_schedule.BarbarActivities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.ez_schedule.R;
import com.ez_schedule.Util.ConnectivityReceiver;
import com.ez_schedule.Util.Parameters;
import com.ez_schedule.Util.SavePref;
import com.ez_schedule.Util.util;
import com.ez_schedule.parser.AllAPIS;
import com.ez_schedule.parser.GetAsync;
import com.ligl.android.widget.iosdialog.IOSDialog;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class RewardSettingActivity extends AppCompatActivity {
    RewardSettingActivity context;
    private SavePref savePref;
    @BindView(R.id.order_count)
    EditText orderCount;
    @BindView(R.id.reward_percentage)
    EditText rewardPercentage;
    @BindView(R.id.post_button)
    Button postButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reward_setting);
        ButterKnife.bind(this);

        context = RewardSettingActivity.this;
        savePref = new SavePref(context);

        orderCount.setText(savePref.getStringLatest(Parameters.REWARD_ORDER_COUNT));
        rewardPercentage.setText(savePref.getStringLatest(Parameters.REWARD_PERCENTANGE));

        setToolbar();
    }

    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView title = (TextView) toolbar.findViewById(R.id.title);
        title.setText(R.string.reward_setting);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back_w);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    public void REWARD_SETTING_API() {
        util.hideKeyboard(context);
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.REWARD_PERCENTANGE, rewardPercentage.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.REWARD_ORDER_COUNT, orderCount.getText().toString().trim());
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.REWARD_SETTING, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonObject = new JSONObject(result);
                        if (jsonObject.getString("code").equalsIgnoreCase("200")) {
                            savePref.setStringLatest(Parameters.REWARD_ORDER_COUNT, orderCount.getText().toString().trim());
                            savePref.setStringLatest(Parameters.REWARD_PERCENTANGE, rewardPercentage.getText().toString().trim());

                            new IOSDialog.Builder(context)
                                    .setCancelable(false)
                                    .setMessage(getString(R.string.setting_updated)).setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    finish();
                                }
                            }).show();
                        } else {
                            util.IOSDialog(context, jsonObject.getString("msg"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    @OnClick(R.id.post_button)
    public void onClick() {
        if (ConnectivityReceiver.isConnected()) {
            if (orderCount.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, getString(R.string.enter_order_count));
                postButton.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (rewardPercentage.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, getString(R.string.enter_reward_percentage));
                postButton.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else {
                REWARD_SETTING_API();
            }
        } else {
            util.IOSDialog(context, util.internet_Connection_Error);
        }
    }
}
