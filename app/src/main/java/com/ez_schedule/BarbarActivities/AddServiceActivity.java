package com.ez_schedule.BarbarActivities;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.ez_schedule.Activities.AddCategoryActivity;
import com.ez_schedule.Activities.SignInActivity;
import com.ez_schedule.ModelClasses.BarbarServiceListModel;
import com.ez_schedule.ModelClasses.CategoryModel;
import com.ez_schedule.R;
import com.ez_schedule.Util.ConnectivityReceiver;
import com.ez_schedule.Util.Parameters;
import com.ez_schedule.Util.SavePref;
import com.ez_schedule.Util.util;
import com.ez_schedule.parser.AllAPIS;
import com.ez_schedule.parser.GetAsync;
import com.ez_schedule.parser.GetAsyncGet;
import com.bumptech.glide.Glide;
import com.ligl.android.widget.iosdialog.IOSDialog;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class AddServiceActivity extends AppCompatActivity {
    AddServiceActivity context;
    @BindView(R.id.duration)
    TextView duration;
    private SavePref savePref;
    @BindView(R.id.image)
    ImageView image;
    @BindView(R.id.title___)
    EditText title;
    @BindView(R.id.title_spanish)
    EditText title_spanish;
    @BindView(R.id.description)
    EditText description;
    @BindView(R.id.category)
    TextView category;
    @BindView(R.id.price)
    EditText price;
    @BindView(R.id.post_button)
    Button postButton;

    private ArrayList<CategoryModel> list;
    private String selectedimage = "", category_id = "", duration_text = "";
    Uri fileUri;
    boolean from_edit = false;

    BarbarServiceListModel barbarListModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_service);
        ButterKnife.bind(this);


        context = AddServiceActivity.this;
        savePref = new SavePref(context);

        from_edit = getIntent().getBooleanExtra("from_edit", false);

        if (from_edit) {
            barbarListModel = getIntent().getExtras().getParcelable("data");
            title.setText(barbarListModel.getName());
            title_spanish.setText(barbarListModel.getName_spanish());
            description.setText(barbarListModel.getDescription());
            price.setText(barbarListModel.getPrice());
            Glide.with(context).load(barbarListModel.getImage()).into(image);

            category_id = barbarListModel.getCategory_id();

            duration_text = barbarListModel.getDuration();

            if (duration_text.equals("1"))
                duration.setText("15 Mins");
            else if (duration_text.equals("2"))
                duration.setText("30 Mins");
            else if (duration_text.equals("3"))
                duration.setText("45 Mins");
            else if (duration_text.equals("4"))
                duration.setText("1 Hour");
            else if (duration_text.equals("5"))
                duration.setText("1 Hour 15 Mins");
            else if (duration_text.equals("6"))
                duration.setText("1 Hour 30 Mins");
            else if (duration_text.equals("7"))
                duration.setText("1 Hour 45 Mins");
            else if (duration_text.equals("8"))
                duration.setText("2 Hours");


            postButton.setText(getString(R.string.update));
        }


        setToolbar();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (ConnectivityReceiver.isConnected()) {
            GETCATEGORYLIST(false);
        } else {
            util.IOSDialog(context, util.internet_Connection_Error);
        }
    }

    private void GETCATEGORYLIST(boolean flag) {
        ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.AUTH_KEY, "");
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsyncGet mAsync = new GetAsyncGet(context, AllAPIS.GETCATEGORYLIST, formBody) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                list = new ArrayList<>();
                if (list.size() > 0)
                    list.clear();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {
                            JSONArray Facility = jsonmainObject.getJSONArray("body");
                            for (int i = 0; i < Facility.length(); i++) {
                                JSONObject object = Facility.getJSONObject(i);
                                CategoryModel categoryModel = new CategoryModel();
                                categoryModel.setId(object.getString("id"));

                                if (from_edit) {
                                    if (!barbarListModel.getCategory_id().isEmpty()) {
                                        if (object.getString("id").equals(barbarListModel.getCategory_id()))
                                            category.setText(object.getString("name"));
                                    }
                                }
                                categoryModel.setName(object.getString("name"));
                                categoryModel.setImage(object.optString("image"));
                                list.add(categoryModel);
                            }

                            CategoryModel categoryModel = new CategoryModel();
                            categoryModel.setName(getString(R.string.add_new_category));
                            list.add(categoryModel);


                            if (flag)
                                Show_Category();

                        } else {
                            if (jsonmainObject.getString("msg").equals(util.Invalid_Authorization)) {
                                savePref.setAuthorization_key("");
                                Intent intent = new Intent(context, SignInActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                            } else {
                                util.showToast(context, jsonmainObject.getString("msg"));
                            }
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }


    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView title = (TextView) toolbar.findViewById(R.id.title);
        if (from_edit) {
            title.setText(R.string.update_service);
        } else {
            title.setText(R.string.add_service);
        }

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back_w);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    @OnClick({R.id.image, R.id.category, R.id.duration, R.id.post_button})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.image:
                CropImage.activity(fileUri)
                        .setAspectRatio(2, 2)
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .start(this);
                break;
            case R.id.category:
                if (list != null) {
                    Show_Category();
                } else {
                    if (list == null)
                        list = new ArrayList<>();

                    GETCATEGORYLIST(true);
                }

                break;
            case R.id.duration:
                DurationDialog();
                break;
            case R.id.post_button:
                AddServiceTask();
                break;
        }
    }

    private void DurationDialog() {
        // setup the alert builder
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Service Duration");

        /*ArrayList to Array Conversion */
        String array[] = new String[8];
        array[0] = "15 Mins";
        array[1] = "30 Mins";
        array[2] = "45 Mins";
        array[3] = "1 Hour";
        array[4] = "1 Hour 15 Mins";
        array[5] = "1 Hour 30 Mins";
        array[6] = "1 Hour 45 Mins";
        array[7] = "2 Hours";


        builder.setItems(array, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int position) {
                duration.setText(array[position]);
                duration_text = String.valueOf(position + 1);
                dialog.dismiss();
            }
        });

// create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void Show_Category() {
        // setup the alert builder
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(R.string.choose_an_category);

        /*ArrayList to Array Conversion */
        String array[] = new String[list.size()];
        for (int j = 0; j < list.size(); j++) {
            array[j] = list.get(j).getName();
        }
        builder.setItems(array, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                if ((list.size() - 1) == which) {
                    startActivity(new Intent(context, AddCategoryActivity.class));
                    overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                } else {
                    category.setText(list.get(which).getName());
                    category_id = list.get(which).getId();
                }


            }
        });

// create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void AddServiceTask() {
        if (ConnectivityReceiver.isConnected()) {
           /* if (title.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, getString(R.string.enter_service_name));
                title.requestFocus();
                postButton.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (title_spanish.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, getString(R.string.enter_service_name));
                title_spanish.requestFocus();
                postButton.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else */

            if (title.getText().toString().trim().isEmpty() && title_spanish.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, getString(R.string.enter_service_name));
                postButton.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (description.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, getString(R.string.enter_service_description));
                postButton.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } /*else if (category.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, getString(R.string.select_service_category));
                postButton.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            }*/ else if (duration.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, getString(R.string.select_service_duration));
                postButton.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (price.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, getString(R.string.enter_service_price));
                postButton.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else {
                if (from_edit) {
                    ADD_SERVICES_API(AllAPIS.UPDATE_SERVICES);
                } else {
                    if (selectedimage.isEmpty()) {
                        util.IOSDialog(context, getString(R.string.select_service_image));
                        postButton.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
                    } else {
                        ADD_SERVICES_API(AllAPIS.ADD_SERVICES);
                    }
                }

            }
        } else {
            util.IOSDialog(context, util.internet_Connection_Error);
        }
    }

    private void ADD_SERVICES_API(String url) {
        String title_eng = "", title_spa = "";

        if (!title.getText().toString().trim().isEmpty())
            title_eng = title.getText().toString().trim();
        else
            title_eng = title_spanish.getText().toString().trim();

        if (!title_spanish.getText().toString().trim().isEmpty())
            title_spa = title_spanish.getText().toString().trim();
        else
            title_spa = title.getText().toString().trim();

        util.hideKeyboard(context);
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        if (!selectedimage.isEmpty()) {
            final MediaType MEDIA_TYPE = selectedimage.endsWith("png") ?
                    MediaType.parse("image/png") : MediaType.parse("image/jpeg");

            File file = new File(selectedimage);
            formBuilder.addFormDataPart(Parameters.IMAGE, file.getName(), RequestBody.create(MEDIA_TYPE, file));
        }
        if (from_edit)
            formBuilder.addFormDataPart(Parameters.SERVICE_ID, barbarListModel.getId());
        formBuilder.addFormDataPart(Parameters.NAME, title_eng.trim());
        formBuilder.addFormDataPart(Parameters.NAME_SPANISH, title_spa.trim());
        formBuilder.addFormDataPart(Parameters.CATEGORY_ID, category_id);
        formBuilder.addFormDataPart(Parameters.PRICE, price.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.DURATION, duration_text);
        formBuilder.addFormDataPart(Parameters.DESCRIPTION, description.getText().toString().trim());
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, url, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonObject = new JSONObject(result);
                        if (jsonObject.getString("code").equalsIgnoreCase("200")) {
                            String title = "";
                            if (from_edit)
                                title = getString(R.string.service_updated_successfully);
                            else
                                title = getString(R.string.service_added_successfully);

                            new IOSDialog.Builder(context)
                                    .setCancelable(false)
                                    .setMessage(title).setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    finish();
                                }
                            }).show();
                        } else {
                            util.IOSDialog(context, jsonObject.getString("msg"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
                CropImage.ActivityResult result = CropImage.getActivityResult(data);
                if (resultCode == RESULT_OK) {
                    Uri resultUri = result.getUri();

                    selectedimage = getAbsolutePath(this, resultUri);

                    Glide.with(this).load(selectedimage).into(image);


                } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                    Exception error = result.getError();
                }
            }
        }
    }

    public String getAbsolutePath(Context activity, Uri uri) {

        if ("content".equalsIgnoreCase(uri.getScheme())) {
            String[] projection = {"_data"};
            Cursor cursor = null;
            try {
                cursor = activity.getContentResolver().query(uri, projection, null, null, null);
                int column_index = cursor.getColumnIndexOrThrow("_data");
                if (cursor.moveToFirst()) {
                    return cursor.getString(column_index);
                }
            } catch (Exception e) {
                // Eat it
                e.printStackTrace();
            }
        } else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

}
