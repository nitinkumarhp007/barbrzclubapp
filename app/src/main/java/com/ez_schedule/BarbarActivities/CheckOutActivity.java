package com.ez_schedule.BarbarActivities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ez_schedule.Activities.CharityActivity;
import com.ez_schedule.Activities.SignInActivity;
import com.ez_schedule.MainActivity;
import com.ez_schedule.ModelClasses.BarbarServiceListModel;
import com.ez_schedule.ModelClasses.TimeslotModel;
import com.ez_schedule.R;
import com.ez_schedule.UserAdapters.ServicesAdapter;
import com.ez_schedule.UserAdapters.TimeSlotAdapter;
import com.ez_schedule.Util.ConnectivityReceiver;
import com.ez_schedule.Util.Parameters;
import com.ez_schedule.Util.SavePref;
import com.ez_schedule.Util.util;
import com.ez_schedule.parser.AllAPIS;
import com.ez_schedule.parser.GetAsync;
import com.github.sundeepk.compactcalendarview.CompactCalendarView;
import com.ligl.android.widget.iosdialog.IOSDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class CheckOutActivity extends AppCompatActivity {
    CheckOutActivity context;
    @BindView(R.id.price)
    public TextView price;
    @BindView(R.id.description)
    EditText description;
    @BindView(R.id.scrollable)
    ScrollView scrollView;
    @BindView(R.id.reward_achive_text)
    public TextView rewardAchiveText;
    @BindView(R.id.seekbar_1)
    SeekBar seekbar1;
    @BindView(R.id.completed_order)
    TextView completedOrder;
    @BindView(R.id.reward_order_count)
    TextView rewardOrderCount;
    @BindView(R.id.reward_layout)
    LinearLayout rewardLayout;
    @BindView(R.id.next_layout)
    RelativeLayout nextLayout;
    @BindView(R.id.discount_applied_text)
    public TextView discountAppliedText;
    private SavePref savePref;
    @BindView(R.id.my_recycler_view)
    RecyclerView myRecyclerView;
    @BindView(R.id.compactcalendar_view)
    CompactCalendarView compactcalendarView;
    @BindView(R.id.error_message)
    TextView error_message;
    @BindView(R.id.my_recycler_view_time_slot)
    RecyclerView myRecyclerViewTimeSlot;
    @BindView(R.id.next)
    Button next;
    ArrayList<TimeslotModel> timeslot_list;
    ArrayList<BarbarServiceListModel> list;
    String barber_id = "";
    public String slot_id = "";
    public String is_rewarded = "";
    public String total_amount = "";
    String strDate = "";
    public String reward_percentage = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_out);
        ButterKnife.bind(this);
        setToolbar();

        context = CheckOutActivity.this;
        savePref = new SavePref(context);

        barber_id = getIntent().getStringExtra("barber_id");
        list = getIntent().getParcelableArrayListExtra("service_list");

        ServicesAdapter adapter = new ServicesAdapter(context, list);
        myRecyclerView.setLayoutManager(new LinearLayoutManager(context));
        myRecyclerView.setAdapter(adapter);


        compactcalendarView.setListener(new CompactCalendarView.CompactCalendarViewListener() {
            @Override
            public void onDayClick(Date date) {
                DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                strDate = dateFormat.format(date);
                TIME_SLOT_LIST();
            }

            @Override
            public void onMonthScroll(Date firstDayOfNewMonth) {
                DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                strDate = dateFormat.format(firstDayOfNewMonth);
                TIME_SLOT_LIST();
            }
        });



        Locale locale = new Locale(savePref.getLang());
        TimeZone tz = TimeZone.getDefault();
        compactcalendarView.setLocale(tz,locale);


        if (ConnectivityReceiver.isConnected()) {
            strDate = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault()).format(new Date());
            TIME_SLOT_LIST();
        } else {
            util.IOSDialog(context, util.internet_Connection_Error);
        }

    }

    private void TIME_SLOT_LIST() {
        String dateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault()).format(new Date());
        Log.e("StringstrDate_", strDate);
        Long tsLong = System.currentTimeMillis() / 1000;
        Log.e("StringstrDate_", tsLong.toString());
        ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.BARBER_ID, barber_id);
        formBuilder.addFormDataPart(Parameters.DATE, util.date_to_timestamp(strDate));
        if (strDate.equals(dateFormat)) {
            Log.e("showwwwww", "true");
            formBuilder.addFormDataPart(Parameters.CURRENT_TIMESTAMP, tsLong.toString());//only for current day
        } else {
            Log.e("showwwwww", "false");
        }

        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.TIME_SLOT, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                timeslot_list = new ArrayList<>();
                if (timeslot_list.size() > 0)
                    timeslot_list.clear();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {
                            JSONObject body = jsonmainObject.getJSONObject("body");

                            reward_percentage = body.getString("reward_percentage");
                            String reward_order_count = body.getString("reward_order_count");
                            String completed_order = body.getString("completed_order");

                            if (reward_order_count.isEmpty() || reward_order_count.equals("0")) {
                                rewardLayout.setVisibility(View.GONE);
                            } else {
                                rewardLayout.setVisibility(View.VISIBLE);
                                seekbar1.setMax(Integer.parseInt(reward_order_count));
                                seekbar1.setProgress(Integer.parseInt(completed_order));

                                completedOrder.setText(completed_order);
                                rewardOrderCount.setText(reward_order_count);

                                discountAppliedText.setText(reward_percentage + getString(R.string.discount_applied));

                                rewardAchiveText.setText(getString(R.string.odrer_completed_reward) + completed_order + " out " + reward_order_count + ")");

                                if (reward_order_count.equals(completed_order)) {
                                    is_rewarded = "2";
                                } else {
                                    is_rewarded = "";
                                }


                            }


                            JSONArray time_slots = body.getJSONArray("time_slots");
                            for (int i = 0; i < time_slots.length(); i++) {
                                JSONObject object = time_slots.getJSONObject(i);
                                TimeslotModel timeslotModel = new TimeslotModel();
                                timeslotModel.setId(object.getString("slot_id"));
                                timeslotModel.setSlot(object.getString("slot"));
                                timeslotModel.setAvailability("1");
                                timeslot_list.add(timeslotModel);
                            }

                            if (time_slots.length() > 0)
                                Log.e("day___", time_slots.getJSONObject(0).getString("day"));

                            if (timeslot_list.size() > 0) {
                                myRecyclerViewTimeSlot.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
                                myRecyclerViewTimeSlot.setAdapter(new TimeSlotAdapter(context, timeslot_list));

                                myRecyclerViewTimeSlot.setVisibility(View.VISIBLE);
                                error_message.setVisibility(View.GONE);
                            } else {

                                error_message.setVisibility(View.VISIBLE);
                                myRecyclerViewTimeSlot.setVisibility(View.INVISIBLE);
                            }


                            scrollView.setVisibility(View.VISIBLE);

                        } else {
                            if (jsonmainObject.getString("msg").equals(util.Invalid_Authorization)) {
                                 savePref.setAuthorization_key("");
                                Intent intent = new Intent(context, SignInActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                            } else {
                                util.showToast(context, jsonmainObject.getString("msg"));
                            }
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }


    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView title = (TextView) toolbar.findViewById(R.id.title);
        title.setText(R.string.book_appointment);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back_w);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    @OnClick(R.id.next)
    public void onClick() {
        String services_text = "";
        String number_of_slots = "";
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).isIs_check()) {

                if (number_of_slots.equals(""))
                    number_of_slots = list.get(i).getDuration();
                else
                    number_of_slots = String.valueOf(Integer.parseInt(number_of_slots) + Integer.parseInt(list.get(i).getDuration()));

                if (services_text.equals(""))
                    services_text = list.get(i).getId();
                else
                    services_text = services_text + "," + list.get(i).getId();
            }
        }

        Log.e("dataaaaa", "number_of_slots: " + number_of_slots);
        Log.e("dataaaaa", "services_text: " + services_text);

        if (ConnectivityReceiver.isConnected()) {
            if (services_text.isEmpty()) {
                util.IOSDialog(context, getString(R.string.select_services));
            } else if (slot_id.isEmpty()) {
                util.IOSDialog(context, getString(R.string.select_time_slot));
            } else {
                String finalServices_text = services_text;
                String finalNumber_of_slots = number_of_slots;
                new IOSDialog.Builder(context)
                        .setTitle(context.getResources().getString(R.string.app_name))
                        .setMessage(getString(R.string.are_you_sure))
                        .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                BOOK_BARBER_API(finalServices_text, finalNumber_of_slots);
                            }
                        })
                        .setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        }).show();
            }
        } else {
            util.IOSDialog(context, util.internet_Connection_Error);
        }


    }

    public void BOOK_BARBER_API(String services_text, String number_of_slots) {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.BARBER_ID, barber_id);
        formBuilder.addFormDataPart(Parameters.DATE, util.date_to_timestamp(strDate));
        formBuilder.addFormDataPart(Parameters.SLOT_ID, slot_id);
        formBuilder.addFormDataPart(Parameters.IS_REWARDED, is_rewarded);
        formBuilder.addFormDataPart(Parameters.TOTAL_AMOUNT, total_amount);
        formBuilder.addFormDataPart(Parameters.SERVICES, services_text);
        formBuilder.addFormDataPart(Parameters.NO_SLOTS, number_of_slots);
        formBuilder.addFormDataPart(Parameters.ADDITIONAL_INFO, description.getText().toString().trim());
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.BOOK_BARBER, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonMainobject = new JSONObject(result);
                        if (jsonMainobject.getString("code").equalsIgnoreCase("200")) {
                            new IOSDialog.Builder(context)
                                    .setCancelable(false)
                                    .setMessage(getString(R.string.booking_request_sent)).setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    /*Intent intent = new Intent(context, MainActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(intent);
                                    overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);*/


                                    Intent intent = new Intent(context, CharityActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(intent);
                                    overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);


                                }
                            }).show();
                        } else {
                            util.IOSDialog(context, jsonMainobject.getString("msg"));
                        }


                    } catch (Exception e) {
                    }
                }

            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

}
