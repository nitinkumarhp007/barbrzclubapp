package com.ez_schedule.Activities;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.load.engine.Resource;
import com.ez_schedule.BarbarActivities.BarbarMainActivity;
import com.ez_schedule.MainActivity;
import com.ez_schedule.R;
import com.ez_schedule.Util.ConnectivityReceiver;
import com.ez_schedule.Util.LocaleHelper;
import com.ez_schedule.Util.Parameters;
import com.ez_schedule.Util.SavePref;
import com.ez_schedule.Util.util;
import com.ez_schedule.parser.AllAPIS;
import com.ez_schedule.parser.GetAsync;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Parameter;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class SignInActivity extends AppCompatActivity {
    SignInActivity context;
    @BindView(R.id.email_address)
    EditText emailAddress;
    @BindView(R.id.password)
    EditText password;
    @BindView(R.id.sign_in)
    Button signIn;
    @BindView(R.id.forgot_password)
    Button forgotPassword;
    @BindView(R.id.sign_up)
    Button signUp;
    @BindView(R.id.change_language)
    Button change_language;
    private SavePref savePref;

    Context context1;
    Resources resources;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        ButterKnife.bind(this);

        context = SignInActivity.this;
        savePref = new SavePref(context);

        //  util.IOSDialog(context, "App version V3");

    }

    @OnClick({R.id.sign_in, R.id.forgot_password, R.id.change_language, R.id.sign_up})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.sign_in:
                SigninProcess();
                break;
            case R.id.forgot_password:
                startActivity(new Intent(this, ForgotPasswordActivity.class));
                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
            case R.id.sign_up:
                startActivity(new Intent(this, SignupAsChooseActivity.class));
                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
            case R.id.change_language:
                LanguageDialog();
                break;
        }
    }

    private void SigninProcess() {
        if (ConnectivityReceiver.isConnected()) {
            if (emailAddress.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, getString(R.string.enter_email));
                signIn.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (!util.isValidEmail(emailAddress.getText().toString().trim())) {
                util.IOSDialog(context, getString(R.string.enter_vaild_email));
                signIn.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (password.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, getString(R.string.enter_password));
                signIn.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else {
                LOGIN_API();
            }
        } else {
            util.IOSDialog(context, util.internet_Connection_Error);
        }

    }

    private void LOGIN_API() {
        util.hideKeyboard(context);
        Log.e("device_token__", SavePref.getDeviceToken(SignInActivity.this, "token"));
        String s = SavePref.getDeviceToken(SignInActivity.this, "token");
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.EMAIL, emailAddress.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.PASSWORD, password.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.DEVICE_TYPE, "1");
        formBuilder.addFormDataPart(Parameters.DEVICE_TOKEN, SavePref.getDeviceToken(this, "token"));
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.USERLOGIN, formBody, "") {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonMainobject = new JSONObject(result);
                        if (jsonMainobject.getString("code").equalsIgnoreCase("200")) {
                            util.hideKeyboard(context);
                            JSONObject body = jsonMainobject.getJSONObject("body");

                            //  show_dialog(result);

                            savePref.setAuthorization_key(body.optString("auth_key"));
                            savePref.setID(body.optString("id"));
                            savePref.setEmail(body.optString("email"));
                            savePref.setName(body.optString("username"));
                            savePref.setPhone(body.optString("phone"));
                            savePref.setImage(body.optString("profile_image"));
                            savePref.setStringLatest("is_from_social", "0");
                            savePref.setStringLatest(Parameters.REWARD_ORDER_COUNT, body.optString(Parameters.REWARD_ORDER_COUNT));
                            savePref.setStringLatest(Parameters.REWARD_PERCENTANGE, body.optString(Parameters.REWARD_PERCENTANGE));

                            if (body.optString("user_type").equals("2")) {
                                savePref.setAddress(body.optString("address"));
                                savePref.setStringLatest(Parameters.START_TIME_CALL, body.optString(Parameters.START_TIME_CALL));
                                savePref.setStringLatest(Parameters.END_TIME_CALL, body.optString(Parameters.END_TIME_CALL));
                                savePref.setStringLatest("lat", body.optString("lat"));
                                savePref.setStringLatest("lng", body.optString("lng"));
                                savePref.setStringLatest("description", body.optString("description"));
                                savePref.setStringLatest(Parameters.SQUARE_ID, body.optString("square_id"));
                                savePref.setStringLatest("subscription_status", body.optString("subscription_status"));
                            }

                            savePref.setStringLatest("user_type", body.optString("user_type"));
                            if (body.optString("user_type").equals("1")) {
                                Intent intent = new Intent(context, MainActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                                util.showToast(context, getString(R.string.login_successfully));
                            } else {
                                Intent intent = new Intent(context, BarbarMainActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                                util.showToast(context, getString(R.string.login_successfully));
                            }


                        } else {
                            util.IOSDialog(context, jsonMainobject.getString("msg"));
                        }
                    } catch (JSONException ex) {
                        util.IOSDialog(context, ex.toString());
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        util.IOSDialog(context, ex.toString());
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    private void show_dialog(String text) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog);

        TextView submit = (TextView) dialog.findViewById(R.id.submit);
        TextView text1 = (TextView) dialog.findViewById(R.id.text);
        text1.setText(text);

        text1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ClipboardManager cm = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
                cm.setText(text1.getText());
                Toast.makeText(context, R.string.copied_to_clipboard, Toast.LENGTH_SHORT).show();
            }
        });


        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }


    private void LanguageDialog() {
        ArrayList<String> list = new ArrayList<>();
        list.add("English");
        list.add("Spanish");
        final CharSequence[] Animals = list.toArray(new String[list.size()]);
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        dialogBuilder.setTitle(context.getResources().getString(R.string.select_language));
        dialogBuilder.setItems(Animals, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                if (item == 0) {
                    savePref.setLang("en");
                    context1 = LocaleHelper.setLocale(context, savePref.getLang());
                    //language.setText(context.getResources().getString(R.string.language) + " (English)");
                    /*language.setCompoundDrawablesWithIntrinsicBounds(getContext().getResources().getDrawable(R.drawable.flag),
                            null, getContext().getResources().getDrawable(R.drawable.arrow_d), null);*/
                    dialog.dismiss();
                    //LANGUAGE_CHANGE_API("1");
                } else {
                    savePref.setLang("es");
                    context1 = LocaleHelper.setLocale(context, savePref.getLang());
                    //language.setText(context.getResources().getString(R.string.language) + " (Spanish)");
                   /* language.setCompoundDrawablesWithIntrinsicBounds(getContext().getResources().getDrawable(R.drawable.romania_flag),
                            null, getContext().getResources().getDrawable(R.drawable.arrow_d), null);*/
                    dialog.dismiss();
                    //LANGUAGE_CHANGE_API("2");
                }

                resources = context1.getResources();

                startActivity(getIntent());
                finish();
                overridePendingTransition(0, 0);


            }
        });
        //Create alert dialog object via builder
        AlertDialog alertDialogObject = dialogBuilder.create();
        //Show the dialog
        alertDialogObject.show();
    }

}
