package com.ez_schedule.Activities;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ScrollView;

import com.ez_schedule.ModelClasses.BarbarServiceListModel;
import com.ez_schedule.ModelClasses.HoursModel;
import com.ez_schedule.R;
import com.ez_schedule.UserAdapters.DetailServicesAdapter;
import com.ez_schedule.UserAdapters.HoursAdapter;
import com.ez_schedule.Util.ConnectivityReceiver;
import com.ez_schedule.Util.Parameters;
import com.ez_schedule.Util.SavePref;
import com.ez_schedule.Util.util;
import com.ez_schedule.parser.AllAPIS;
import com.ez_schedule.parser.GetAsync;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.google.gson.JsonArray;
import com.ligl.android.widget.iosdialog.IOSDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class SignupTimeSlotActivity extends AppCompatActivity {
    SignupTimeSlotActivity context;
    private SavePref savePref;
    @BindView(R.id.change)
    Button change;
    @BindView(R.id.my_recycler_view)
    RecyclerView myRecyclerView;
    @BindView(R.id.scrollView)
    ScrollView scrollView;
    @BindView(R.id.back_button)
    ImageView back_button;
    private ArrayList<HoursModel> hourslist;
    ArrayList<String> time_list;

    boolean from_profile = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup_time_slot);

        ButterKnife.bind(this);
        context = SignupTimeSlotActivity.this;
        savePref = new SavePref(context);

        from_profile = getIntent().getBooleanExtra("from_profile", false);

        //util.IOSDialog(context, "Code Updated 3.0");


        time_list = new ArrayList<>();
        time_list.add("1:00 AM");
        time_list.add("2:00 AM");
        time_list.add("3:00 AM");
        time_list.add("4:00 AM");
        time_list.add("5:00 AM");
        time_list.add("6:00 AM");
        time_list.add("7:00 AM");
        time_list.add("8:00 AM");
        time_list.add("9:00 AM");
        time_list.add("10:00 AM");
        time_list.add("11:00 AM");
        time_list.add("12:00 AM");
        time_list.add("1:00 PM");
        time_list.add("2:00 PM");
        time_list.add("3:00 PM");
        time_list.add("4:00 PM");
        time_list.add("5:00 PM");
        time_list.add("6:00 PM");
        time_list.add("7:00 PM");
        time_list.add("8:00 PM");
        time_list.add("9:00 PM");
        time_list.add("10:00 PM");
        time_list.add("11:00 PM");
        time_list.add("12:00 PM");

        if (from_profile) {
            change.setText(getString(R.string.change));

            if (ConnectivityReceiver.isConnected())
                BARBAR_PROFILE();
            else
                util.IOSDialog(context, util.internet_Connection_Error);

        } else {
            change.setText(getString(R.string.sign_up));

            HoursModel hoursModel1 = new HoursModel(getResources().getString(R.string.monday), "10:00 AM", "8:00 PM", "0");
            HoursModel hoursModel2 = new HoursModel(getResources().getString(R.string.tuesday), "10:00 AM", "8:00 PM", "0");
            HoursModel hoursModel3 = new HoursModel(getResources().getString(R.string.wednesday), "10:00 AM", "8:00 PM", "0");
            HoursModel hoursModel4 = new HoursModel(getResources().getString(R.string.thursday), "10:00 AM", "8:00 PM", "0");
            HoursModel hoursModel5 = new HoursModel(getResources().getString(R.string.friday), "10:00 AM", "8:00 PM", "0");
            HoursModel hoursModel6 = new HoursModel(getResources().getString(R.string.saturday), "10:00 AM", "8:00 PM", "0");
            HoursModel hoursModel7 = new HoursModel(getResources().getString(R.string.sunday), "10:00 AM", "8:00 PM", "0");

            hourslist = new ArrayList<>();
            hourslist.add(hoursModel1);
            hourslist.add(hoursModel2);
            hourslist.add(hoursModel3);
            hourslist.add(hoursModel4);
            hourslist.add(hoursModel5);
            hourslist.add(hoursModel6);
            hourslist.add(hoursModel7);

            myRecyclerView.setLayoutManager(new LinearLayoutManager(this));
            myRecyclerView.setAdapter(new HoursAdapter(this, hourslist, time_list));

            scrollView.setVisibility(View.VISIBLE);
        }


        back_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        change.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              /*  hourslist_new = new ArrayList<>();
                hourslist_new = hourslist;
                for (int i = 0; i < hourslist.size(); i++) {
                    hourslist_new.get(i).setStart_time(util.time_to_timestamp(hourslist.get(i).getStart_time()));
                    hourslist_new.get(i).setEnd_time(util.time_to_timestamp(hourslist.get(i).getEnd_time()));
                }
            }
        });*/

                ArrayList arrayList = new ArrayList();
                for (int i = 0; i < hourslist.size(); i++) {
                    JSONObject object = new JSONObject();
                    try {
                        object.put("day", i + 1);
                        object.put("open_time", util.time_to_timestamp(hourslist.get(i).getStart_time(), true));
                        object.put("close_time", util.time_to_timestamp(hourslist.get(i).getEnd_time(), false));
                        object.put("is_close", hourslist.get(i).getIs_close());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    arrayList.add(object);
                }

                Log.e("data____", String.valueOf(arrayList));


              //  util.IOSDialog(context, String.valueOf(arrayList));





                if (ConnectivityReceiver.isConnected()) {
                    if (!from_profile)
                        USER_SIGNUP_API(String.valueOf(arrayList));
                    else
                        UPDATE_PROFILE_API(String.valueOf(arrayList));
                } else
                    util.IOSDialog(context, util.internet_Connection_Error);


            }
        });

    }

    private void BARBAR_PROFILE() {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.BARBER_ID, savePref.getID());
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.BARBAR_PROFILE, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                hourslist = new ArrayList<>();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {

                            JSONObject body = jsonmainObject.getJSONObject("body");

                            JSONArray schedule = body.getJSONArray("schedule");

                            for (int i = 0; i < schedule.length(); i++) {
                                JSONObject object = schedule.getJSONObject(i);

                                String day_name = "";
                                if (object.getString("day").equals("1")) {
                                    day_name = getString(R.string.monday);
                                } else if (object.getString("day").equals("2")) {
                                    day_name = getString(R.string.tuesday);
                                } else if (object.getString("day").equals("3")) {
                                    day_name = getString(R.string.wednesday);
                                } else if (object.getString("day").equals("4")) {
                                    day_name = getString(R.string.thursday);
                                } else if (object.getString("day").equals("5")) {
                                    day_name = getString(R.string.friday);
                                } else if (object.getString("day").equals("6")) {
                                    day_name = getString(R.string.saturday);
                                } else if (object.getString("day").equals("7")) {
                                    day_name = getString(R.string.sunday);
                                }

                                HoursModel hoursModel = new HoursModel(day_name, util.time_to_timestamp_24_12(object.getString("open_time")),
                                        util.time_to_timestamp_24_12(object.getString("close_time")), object.getString("is_close"));


                                hourslist.add(hoursModel);
                            }

                            myRecyclerView.setLayoutManager(new LinearLayoutManager(context));
                            myRecyclerView.setAdapter(new HoursAdapter(context, hourslist, time_list));

                            scrollView.setVisibility(View.VISIBLE);

                        } else {
                            if (jsonmainObject.getString("msg").equals(util.Invalid_Authorization)) {
                                savePref.setAuthorization_key("");
                                Intent intent = new Intent(context, SignInActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                            } else {
                                util.showToast(context, jsonmainObject.getString("msg"));
                            }
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    private void UPDATE_PROFILE_API(String json) {


        util.hideKeyboard(context);
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.IDS, json);//:[{"slot_id" : 73,"is_close":0},{"slot_id" : 74,"is_close":1},{"slot_id" : 75,"is_close":0}]
        formBuilder.addFormDataPart(Parameters.USERNAME, savePref.getName());
        formBuilder.addFormDataPart(Parameters.EMAIL, savePref.getEmail());
        formBuilder.addFormDataPart(Parameters.PHONE, savePref.getPhone());
        formBuilder.addFormDataPart(Parameters.SCHEDULE, json);
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.EDIT_PROFILE, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonMainobject = new JSONObject(result);
                        if (jsonMainobject.getString("code").equalsIgnoreCase("200")) {
                            util.hideKeyboard(context);

                            new IOSDialog.Builder(context)
                                    .setCancelable(false)
                                    .setMessage(getString(R.string.scheduleupdatedsucessfully)).setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            }).show();

                        } else {
                            util.IOSDialog(context, jsonMainobject.getString("msg"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    private void USER_SIGNUP_API(String schedule_text) {
        util.hideKeyboard(context);
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        if (!getIntent().getStringExtra("selectedimage").isEmpty()) {
            final MediaType MEDIA_TYPE = getIntent().getStringExtra("selectedimage").endsWith("png") ?
                    MediaType.parse("image/png") : MediaType.parse("image/jpeg");

            File file = new File(getIntent().getStringExtra("selectedimage"));
            formBuilder.addFormDataPart(Parameters.PROFILE_IMAGE, file.getName(), RequestBody.create(MEDIA_TYPE, file));
        }

        formBuilder.addFormDataPart(Parameters.USERNAME, getIntent().getStringExtra("username"));
        formBuilder.addFormDataPart(Parameters.EMAIL, getIntent().getStringExtra("emailAddress"));
        formBuilder.addFormDataPart(Parameters.PASSWORD, getIntent().getStringExtra("password"));
        formBuilder.addFormDataPart(Parameters.PHONE, getIntent().getStringExtra("phone"));

        formBuilder.addFormDataPart(Parameters.USER_TYPE, getIntent().getStringExtra("type"));
        formBuilder.addFormDataPart(Parameters.ADDRESS, getIntent().getStringExtra("address"));
        formBuilder.addFormDataPart(Parameters.LAT, getIntent().getStringExtra("latitude"));
        formBuilder.addFormDataPart(Parameters.LNG, getIntent().getStringExtra("longitude"));
        formBuilder.addFormDataPart(Parameters.DESCRIPTION, getIntent().getStringExtra("description"));
        formBuilder.addFormDataPart(Parameters.SCHEDULE, schedule_text);
        formBuilder.addFormDataPart(Parameters.DEVICE_TYPE, "1");
        formBuilder.addFormDataPart(Parameters.DEVICE_TOKEN, SavePref.getDeviceToken(this, "token"));
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.USER_SIGNUP, formBody, "") {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonMainobject = new JSONObject(result);
                        if (jsonMainobject.getString("code").equalsIgnoreCase("200")) {
                            util.hideKeyboard(context);
                            Intent intent = new Intent(context, OTPActivity.class);
                            intent.putExtra("authorization_key", jsonMainobject.getJSONObject("body").getString("auth_key"));
                            intent.putExtra("type", getIntent().getStringExtra("type"));
                            startActivity(intent);
                            overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                            // util.showToast(context, "User Registered Sucessfully!!!");

                        } else {
                            change.startAnimation(AnimationUtils.loadAnimation(context, R.anim.shake));
                            util.IOSDialog(context, jsonMainobject.getString("msg"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }
}

