package com.ez_schedule.Activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ez_schedule.ModelClasses.BarbarModel;
import com.ez_schedule.UserAdapters.BarbarsListAdapter;
import com.ez_schedule.R;
import com.ez_schedule.UserAdapters.FavoriteAdapter;
import com.ez_schedule.UserFragments.FavoriteFragment;
import com.ez_schedule.Util.ConnectivityReceiver;
import com.ez_schedule.Util.GPSTracker;
import com.ez_schedule.Util.Parameters;
import com.ez_schedule.Util.SavePref;
import com.ez_schedule.Util.util;
import com.ez_schedule.parser.AllAPIS;
import com.ez_schedule.parser.GetAsync;
import com.ez_schedule.parser.GetAsyncGet;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

import static androidx.core.content.ContextCompat.checkSelfPermission;

public class BarbarsListingActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {
    BarbarsListingActivity context;
    private SavePref savePref;
    @BindView(R.id.search_bar)
    EditText searchBar;
    @BindView(R.id.my_recycler_view)
    RecyclerView myRecyclerView;
    @BindView(R.id.error_message)
    TextView errorMessage;

    ArrayList<BarbarModel> list;
    String category_id = "";

    String latitude = "", longitude = "";
    GPSTracker gpsTracker = null;
    FusedLocationProviderClient fusedLocationClient = null;
    Location location = null;

    BarbarsListAdapter adapter = null;

    boolean hit = false;
    boolean is_search = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_barbars_listing);
        ButterKnife.bind(this);

        context = BarbarsListingActivity.this;
        savePref = new SavePref(context);

        category_id = getIntent().getStringExtra("id");

        gpsTracker = new GPSTracker(context);

        is_search = getIntent().getBooleanExtra("is_search", false);

        if (is_search) {
            list = getIntent().getParcelableArrayListExtra("data");

            adapter = new BarbarsListAdapter(context, list);
            myRecyclerView.setLayoutManager(new LinearLayoutManager(context));
            myRecyclerView.setAdapter(adapter);

        } else {
            if (gpsTracker.canGetLocation()) {
                latitude = String.valueOf(gpsTracker.getLatitude());
                longitude = String.valueOf(gpsTracker.getLongitude());
                hit = true;

                if (ConnectivityReceiver.isConnected()) {
                    BARBER_LIST();
                } else {
                    util.IOSDialog(context, util.internet_Connection_Error);
                }


            }
        }


        searchBar.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (adapter != null)
                    adapter.filter(s.toString());

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        setToolbar();
    }

    private void BARBER_LIST() {
        ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.CATEGORY_ID, category_id);
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.BARBER_LIST, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                list = new ArrayList<>();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {
                            JSONArray body = jsonmainObject.getJSONArray("body");
                            for (int i = 0; i < body.length(); i++) {
                                JSONObject object = body.getJSONObject(i);
                                BarbarModel barbarModel = new BarbarModel();
                                barbarModel.setAvg_rating(object.getString("avg_rating"));
                                barbarModel.setId(object.getString("id"));
                                barbarModel.setIs_fav(object.getString("is_fav"));
                                barbarModel.setPrice(object.optString("price"));
                                barbarModel.setProfile_image(object.getString("profile_image"));
                                barbarModel.setUsername(object.getString("username"));
                                barbarModel.setAddress(object.getString("address"));
                                list.add(barbarModel);
                            }

                            if (list.size() > 0) {
                                adapter = new BarbarsListAdapter(context, list);
                                myRecyclerView.setLayoutManager(new LinearLayoutManager(context));
                                myRecyclerView.setAdapter(adapter);

                                myRecyclerView.setVisibility(View.VISIBLE);
                                errorMessage.setVisibility(View.GONE);

                            } else {
                                errorMessage.setVisibility(View.VISIBLE);
                                myRecyclerView.setVisibility(View.GONE);
                            }


                        } else {
                            if (jsonmainObject.getString("msg").equals(util.Invalid_Authorization)) {
                                 savePref.setAuthorization_key("");
                                Intent intent = new Intent(context, SignInActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                            } else {
                                util.showToast(context, jsonmainObject.getString("msg"));
                            }
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    public void FAV_UNFAV_API(int position) {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.BARBER_ID, list.get(position).getId());
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.FAV_UNFAV, formBody, savePref.getAuthorization_key()) {

            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonObject = new JSONObject(result);
                        if (!list.get(position).getIs_fav().equals("1"))
                            list.get(position).setIs_fav("1");
                        else
                            list.get(position).setIs_fav("0");
                        myRecyclerView.setAdapter(new BarbarsListAdapter(context, list));
                    } catch (Exception e) {
                    }
                }

            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView title = (TextView) toolbar.findViewById(R.id.title);
        title.setText(R.string.professonals);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back_w);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    @Override
    public void onResume() {
        super.onResume();
        EnableGPSAutoMatically();
    }

    private void EnableGPSAutoMatically() {
        GoogleApiClient googleApiClient = null;
        if (googleApiClient == null) {
            googleApiClient = new GoogleApiClient.Builder(context)
                    .addApi(LocationServices.API).addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this).build();
            googleApiClient.connect();
            LocationRequest locationRequest = LocationRequest.create();
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            locationRequest.setInterval(1 * 1000);
            locationRequest.setFastestInterval(1 * 1000);
            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                    .addLocationRequest(locationRequest);

            // **************************
            builder.setAlwaysShow(true); // this is the key ingredient
            // **************************

            PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi
                    .checkLocationSettings(googleApiClient, builder.build());
            result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
                @Override
                public void onResult(LocationSettingsResult result) {
                    final Status status = result.getStatus();
                    final LocationSettingsStates state = result
                            .getLocationSettingsStates();
                    switch (status.getStatusCode()) {
                        case LocationSettingsStatusCodes.SUCCESS:
                            // util.showToast(context, "GPS Success");
                            // All location settings are satisfied. The client can
                            // initialize location
                            // requests here.

                            if (fusedLocationClient == null)
                                fusedLocationClient = LocationServices.getFusedLocationProviderClient(context);
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                                    // TODO: Consider calling
                                    //    Activity#requestPermissions
                                    // here to request the missing permissions, and then overriding
                                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                    //                                          int[] grantResults)
                                    // to handle the case where the user grants the permission. See the documentation
                                    // for Activity#requestPermissions for more details.
                                    return;
                                }
                            }
                            fusedLocationClient.requestLocationUpdates(locationRequest, mLocationCallback, Looper.myLooper());


                            break;
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            // util.showToast(context, "GPS is not on");
                            // Location settings are not satisfied. But could be
                            // fixed by showing the user
                            // a dialog.
                            try {
                                // Show the dialog by calling
                                // startResolutionForResult(),
                                // and check the result in onActivityResult().
                                status.startResolutionForResult(context, 1000);

                            } catch (IntentSender.SendIntentException e) {
                                // Ignore the error.
                            }


                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                            // util.showToast(context, "Setting change not allowed");
                            // Location settings are not satisfied. However, we have
                            // no way to fix the
                            // settings so we won't show the dialog.
                            break;
                    }
                }
            });
        }
    }

    LocationCallback mLocationCallback = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult locationResult) {
            List<Location> locationList = locationResult.getLocations();
            if (locationList.size() > 0) {
                //The last location in the list is the newest
                location = locationList.get(locationList.size() - 1);
                // util.showToast(getActivity(), "lat long " + location.getLatitude());


                /*gpsTracker = new GPSTracker(context);
                //util.showToast(context, "latt " + String.valueOf(gpsTracker.getLatitude()));
                if (ConnectivityReceiver.isConnected()) {
                    AGENCIE("", true);
                } else {
                    util.IOSDialog(context, util.internet_Connection_Error);
                }*/

                latitude = String.valueOf(location.getLatitude());
                longitude = String.valueOf(location.getLatitude());

                if (is_search) {

                } else {
                    if (!hit) {
                        if (ConnectivityReceiver.isConnected()) {
                            BARBER_LIST();
                        } else {
                            util.IOSDialog(context, util.internet_Connection_Error);
                        }

                    }
                }


                fusedLocationClient.removeLocationUpdates(mLocationCallback);
            }
        }
    };


    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

}
