package com.ez_schedule.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.applandeo.materialcalendarview.EventDay;
import com.applandeo.materialcalendarview.listeners.OnDayClickListener;
import com.ez_schedule.ModelClasses.TimeslotModel;
import com.ez_schedule.ModelClasses.UserOrdersModel;
import com.ez_schedule.R;
import com.ez_schedule.UserAdapters.ChangeTimeSlotAdapter;
import com.ez_schedule.UserAdapters.TimeSlotAdapter;
import com.ez_schedule.Util.ConnectivityReceiver;
import com.ez_schedule.Util.Parameters;
import com.ez_schedule.Util.SavePref;
import com.ez_schedule.Util.util;
import com.ez_schedule.parser.AllAPIS;
import com.ez_schedule.parser.GetAsync;
import com.github.sundeepk.compactcalendarview.CompactCalendarView;
import com.ligl.android.widget.iosdialog.IOSDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.nio.Buffer;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class BlockTimeSlotActivity extends AppCompatActivity {

    BlockTimeSlotActivity context;
    private SavePref savePref;
    @BindView(R.id.compactcalendar_view)
    com.applandeo.materialcalendarview.CalendarView compactcalendarView;
    @BindView(R.id.my_recycler_view)
    RecyclerView myRecyclerView;
    @BindView(R.id.error_message)
    TextView error_message;
    @BindView(R.id.block_whole_day)
    public TextView block_whole_day;
    @BindView(R.id.text2)
    TextView text2;
    @BindView(R.id.save)
    Button save;
    long selcted_date = 0;

    ChangeTimeSlotAdapter timeSlotAdapter = null;

    ArrayList<TimeslotModel> timeslot_list;

    public String date_selected = "", block_whole_day_text = "0", block_whole_day_text_original = "0";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_block_time_slot);

        ButterKnife.bind(this);

        context = BlockTimeSlotActivity.this;
        savePref = new SavePref(context);

        compactcalendarView.setHeaderColor(R.color.colorAccent);
        compactcalendarView.setOnDayClickListener(new OnDayClickListener() {
            @Override
            public void onDayClick(EventDay eventDay) {

                block_whole_day.setBackground(context.getResources().getDrawable(R.drawable.drawable_border));
                block_whole_day.setTextColor(context.getResources().getColor(R.color.black));
                block_whole_day_text = "0";

                selcted_date = eventDay.getCalendar().getTimeInMillis();

                Calendar c = Calendar.getInstance();
                TimeZone tz = TimeZone.getDefault();
                c.add(c.MILLISECOND, tz.getOffset(c.getTimeInMillis()));

                /*List<Calendar> calendars = new ArrayList<>();
                calendars.add(c);
                compactcalendarView.setSelectedDates(calendars);*/

                SimpleDateFormat day = new SimpleDateFormat("dd-MM-yyyy");
                day.setTimeZone(tz);
                Date currenTimeZone = new Date(selcted_date);
                String day_text = day.format(currenTimeZone);

                date_selected = day_text;
                Log.e("calender_time_s: ", day_text);
                if (ConnectivityReceiver.isConnected()) {
                    TIME_SLOT_LIST(util.date_to_timestamp(day_text));
                } else {
                    util.IOSDialog(context, util.internet_Connection_Error);
                }


            }
        });

        if (ConnectivityReceiver.isConnected()) {
            date_selected = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault()).format(new Date());
            Log.e("calender_time_s", date_selected);
            TIME_SLOT_LIST(util.date_to_timestamp(date_selected));
        } else {
            util.IOSDialog(context, util.internet_Connection_Error);
        }


        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList arrayList_block = new ArrayList();

                for (int j = 0; j < timeslot_list.size(); j++) {
                    JSONObject obj = new JSONObject();
                    boolean b = false;
                    if (timeslot_list.get(j).isIs_checked()) {
                        b = true;
                        Log.e("here__", "top");
                        try {
                            obj.put("slot_id", timeslot_list.get(j).getId());
                            obj.put("is_close", "1");

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else {
                        if (timeslot_list.get(j).getIs_close().equals("1")) {
                            b = true;
                            Log.e("here__", "bot " + timeslot_list.get(j).getIs_close());
                            try {
                                obj.put("slot_id", timeslot_list.get(j).getId());
                                obj.put("is_close", "0");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    }
                    if (b)
                        arrayList_block.add(obj);
                }


                //[{"slot_id" : 73,"is_close":0},{"slot_id" : 74,"is_close":1},{"slot_id" : 75,"is_close":0}]

                Log.e("idsssss", util.date_to_timestamp(date_selected) + " ----" + block_whole_day_text + " ----" + String.valueOf(arrayList_block));

                if (ConnectivityReceiver.isConnected())
                    UPDATE_PROFILE_API(String.valueOf(arrayList_block));
                else
                    util.IOSDialog(context, util.internet_Connection_Error);
            }
        });


        block_whole_day.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                for (int j = 0; j < timeslot_list.size(); j++) {
                    if (timeslot_list.get(j).isIs_checked())
                        timeslot_list.get(j).setIs_checked(false);
                }
                timeSlotAdapter.notifyDataSetChanged();

                if (!block_whole_day_text.equals("0"))
                    block_whole_day_text = "0";
                else
                    block_whole_day_text = "1";

                if (block_whole_day_text.equals("1")) {
                    block_whole_day.setBackground(context.getResources().getDrawable(R.drawable.drawable_button_red));
                    block_whole_day.setTextColor(context.getResources().getColor(R.color.white));
                } else {
                    block_whole_day.setBackground(context.getResources().getDrawable(R.drawable.drawable_border));
                    block_whole_day.setTextColor(context.getResources().getColor(R.color.black));
                }
            }
        });

    }

    private void UPDATE_PROFILE_API(String json) {
        util.hideKeyboard(context);
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.IDS, json);//:[{"slot_id" : 73,"is_close":0},{"slot_id" : 74,"is_close":1},{"slot_id" : 75,"is_close":0}]
        formBuilder.addFormDataPart(Parameters.USERNAME, savePref.getName());
        formBuilder.addFormDataPart(Parameters.EMAIL, savePref.getEmail());
        formBuilder.addFormDataPart(Parameters.PHONE, savePref.getPhone());
        formBuilder.addFormDataPart(Parameters.BLOCK_DATE, util.date_to_timestamp(date_selected));

        if (block_whole_day_text_original.equals("1") && block_whole_day_text.equals("0"))
            formBuilder.addFormDataPart(Parameters.BLOCK_WHOLE_DAY, block_whole_day_text);
        else if (block_whole_day_text.equals("1"))
            formBuilder.addFormDataPart(Parameters.BLOCK_WHOLE_DAY, block_whole_day_text);
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.EDIT_PROFILE, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonMainobject = new JSONObject(result);
                        if (jsonMainobject.getString("code").equalsIgnoreCase("200")) {
                            util.hideKeyboard(context);

                            new IOSDialog.Builder(context)
                                    .setCancelable(false)
                                    .setMessage(getString(R.string.time_slots_updated_sucessfully)).setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            }).show();

                        } else {
                            util.IOSDialog(context, jsonMainobject.getString("msg"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    private void TIME_SLOT_LIST(String strDate) {
        ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.DATE, strDate);
        //formBuilder.addFormDataPart(Parameters.CURRENT_TIMESTAMP, tsLong.toString());only for current day
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.ALL_TIME_SLOTS, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                timeslot_list = new ArrayList<>();

                if (timeslot_list.size() > 0)
                    timeslot_list.clear();

                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {
                            JSONObject body = jsonmainObject.getJSONObject("body");
                            text2.setVisibility(View.INVISIBLE);

                            if (body.toString().equals("{}")) {
                                myRecyclerView.setVisibility(View.GONE);
                                error_message.setVisibility(View.VISIBLE);
                            } else if (body.getString("is_close").equals("1")) {
                                text2.setVisibility(View.VISIBLE);
                                text2.setText("Seleted Date: " + date_selected + " (" + body.getString("start_time") + "-" + body.getString("end_time") + ")");
                                myRecyclerView.setVisibility(View.GONE);
                                error_message.setVisibility(View.VISIBLE);
                            } else {
                                text2.setVisibility(View.VISIBLE);
                                text2.setText("Seleted Date: " + date_selected + " (" + body.getString("start_time") + "-" + body.getString("end_time") + ")");
                                myRecyclerView.setVisibility(View.VISIBLE);
                                error_message.setVisibility(View.GONE);

                                block_whole_day_text_original = body.optString("block_whole_day");
                                block_whole_day_text = body.optString("block_whole_day");

                                if (block_whole_day_text.equals("1")) {
                                    block_whole_day.setBackground(context.getResources().getDrawable(R.drawable.drawable_button_red));
                                    block_whole_day.setTextColor(context.getResources().getColor(R.color.white));
                                } else {
                                    block_whole_day.setBackground(context.getResources().getDrawable(R.drawable.drawable_border));
                                    block_whole_day.setTextColor(context.getResources().getColor(R.color.black));
                                }

                                JSONArray time_slots = body.getJSONArray("all_slots");
                                for (int i = 0; i < time_slots.length(); i++) {
                                    JSONObject object = time_slots.getJSONObject(i);
                                    TimeslotModel timeslotModel = new TimeslotModel();
                                    timeslotModel.setId(object.getString("slot_id"));
                                    timeslotModel.setSlot(object.getString("start_time") + "-" + object.getString("end_time"));
                                    timeslotModel.setAvailability("1");
                                    timeslotModel.setIs_close(object.getString("is_close"));
                                    if (object.getString("is_close").equals("1"))
                                        timeslotModel.setIs_checked(true);
                                    else
                                        timeslotModel.setIs_checked(false);
                                    timeslot_list.add(timeslotModel);
                                }

                                timeSlotAdapter = new ChangeTimeSlotAdapter(context, timeslot_list);
                                myRecyclerView.setLayoutManager(new GridLayoutManager(context, 3));
                                myRecyclerView.setAdapter(timeSlotAdapter);
                            }

                            //   scrollView.setVisibility(View.VISIBLE);

                        } else {

                            if (jsonmainObject.getString("msg").equals(util.Invalid_Authorization)) {
                                 savePref.setAuthorization_key("");
                                Intent intent = new Intent(context, SignInActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                            } else {
                                util.showToast(context, jsonmainObject.getString("msg"));
                            }
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

}