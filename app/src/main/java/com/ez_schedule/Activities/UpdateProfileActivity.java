package com.ez_schedule.Activities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.ez_schedule.R;
import com.ez_schedule.Util.ConnectivityReceiver;
import com.ez_schedule.Util.Parameters;
import com.ez_schedule.Util.SavePref;
import com.ez_schedule.Util.util;
import com.ez_schedule.parser.AllAPIS;
import com.ez_schedule.parser.GetAsync;
import com.bumptech.glide.Glide;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class UpdateProfileActivity extends AppCompatActivity {
    UpdateProfileActivity context;
    @BindView(R.id.back_button)
    ImageView backButton;
    @BindView(R.id.image)
    CircleImageView image;
    @BindView(R.id.username)
    EditText username;
    @BindView(R.id.email_address)
    EditText emailAddress;
    @BindView(R.id.phone)
    EditText phone;
    @BindView(R.id.country)
    TextView country;
    @BindView(R.id.change)
    Button change;
    @BindView(R.id.sign_in_or_signup)
    Button signInOrSignup;
    @BindView(R.id.address)
    TextView address;
    @BindView(R.id.description)
    EditText description;
    private SavePref savePref;

    Uri fileUri;
    private String selectedimage = "";
    private final int PLACE_PICKER_REQUEST = 787;
    String latitude = "", longitude = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_profile);
        ButterKnife.bind(this);

        context = UpdateProfileActivity.this;
        savePref = new SavePref(context);

        setdata();
    }

    private void setdata() {
        username.setText(savePref.getName());
        emailAddress.setText(savePref.getEmail());
        phone.setText(savePref.getPhone());

        if (savePref.getStringLatest("user_type").equals("2")) {
            address.setText(savePref.getAddress());
            description.setText(savePref.getStringLatest("description"));

            latitude = savePref.getStringLatest("lat");
            longitude = savePref.getStringLatest("lng");
        } else {
            address.setVisibility(View.GONE);
            description.setVisibility(View.GONE);
            change.setVisibility(View.GONE);
        }


        Glide.with(context).load(savePref.getImage()).error(R.drawable.placeholder).into(image);
    }


    @OnClick({R.id.back_button, R.id.change, R.id.image, R.id.address, R.id.sign_in_or_signup})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.back_button:
                finish();
                break;
            case R.id.change:
                Intent intent = new Intent(context, SignupTimeSlotActivity.class);
                intent.putExtra("from_profile", true);
                startActivity(intent);
                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
            case R.id.image:
                CropImage.activity(fileUri)
                        .setAspectRatio(2, 2)
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .start(this);
                break;
            case R.id.address:
                Locationget();
                break;
            case R.id.sign_in_or_signup:
                UpdateProfileProcess();
                break;
        }
    }

    private void Locationget() {
        // Initialize Places.
        Places.initialize(context, "AIzaSyAA-k3KpN8PbS5u4_9qLFGIFZ_fIm52iM4");
        // Create a new Places client instance.
        PlacesClient placesClient = Places.createClient(context);
        // Set the fields to specify which types of place data to return.
        List<Place.Field> fields = Arrays.asList(com.google.android.libraries.places.api.model.Place.Field.ID,
                com.google.android.libraries.places.api.model.Place.Field.NAME, com.google.android.libraries.places.api.model.Place.Field.LAT_LNG, com.google.android.libraries.places.api.model.Place.Field.ADDRESS, com.google.android.libraries.places.api.model.Place.Field.ID, com.google.android.libraries.places.api.model.Place.Field.PHONE_NUMBER, com.google.android.libraries.places.api.model.Place.Field.RATING, com.google.android.libraries.places.api.model.Place.Field.WEBSITE_URI);
        // Start the autocomplete intent.
        Intent intent = new Autocomplete.IntentBuilder(AutocompleteActivityMode.OVERLAY, fields).build(context);

        startActivityForResult(intent, PLACE_PICKER_REQUEST);
    }


    private void UpdateProfileProcess() {
        if (ConnectivityReceiver.isConnected()) {
            if (username.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, getString(R.string.enter_name));
                signInOrSignup.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (emailAddress.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, getString(R.string.enter_email));
                signInOrSignup.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (!util.isValidEmail(emailAddress.getText().toString().trim())) {
                util.IOSDialog(context, getString(R.string.enter_vaild_email));
                signInOrSignup.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (phone.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, getString(R.string.enter_phone));
                signInOrSignup.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else {
                if (savePref.getStringLatest("user_type").equals("2")) {
                    if (description.getText().toString().trim().isEmpty()) {
                        util.IOSDialog(context, getString(R.string.enter_description));
                        signInOrSignup.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
                    } else {
                        UPDATE_PROFILE_API();
                    }
                } else {
                    UPDATE_PROFILE_API();
                }

            }
        } else {
            util.IOSDialog(context, util.internet_Connection_Error);
        }

    }


    private void UPDATE_PROFILE_API() {
        util.hideKeyboard(context);
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        if (!selectedimage.isEmpty()) {
            final MediaType MEDIA_TYPE = selectedimage.endsWith("png") ?
                    MediaType.parse("image/png") : MediaType.parse("image/jpeg");

            File file = new File(selectedimage);
            formBuilder.addFormDataPart(Parameters.PROFILE_IMAGE, file.getName(), RequestBody.create(MEDIA_TYPE, file));
        }

        formBuilder.addFormDataPart(Parameters.USERNAME, username.getText().toString().trim());

        if (savePref.getStringLatest("user_type").equals("2")) {
            formBuilder.addFormDataPart(Parameters.LAT, latitude);
            formBuilder.addFormDataPart(Parameters.LNG, longitude);
            formBuilder.addFormDataPart(Parameters.ADDRESS, address.getText().toString().trim());
            formBuilder.addFormDataPart(Parameters.DESCRIPTION, description.getText().toString().trim());
        }
        formBuilder.addFormDataPart(Parameters.EMAIL, emailAddress.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.PHONE, phone.getText().toString().trim());
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.EDIT_PROFILE, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonMainobject = new JSONObject(result);
                        if (jsonMainobject.getString("code").equalsIgnoreCase("200")) {
                            util.hideKeyboard(context);
                            JSONObject body = jsonMainobject.getJSONObject("body");
                            savePref.setEmail(body.getString("email"));
                            savePref.setName(body.getString("username"));
                            savePref.setPhone(body.getString("phone"));
                            savePref.setImage(body.getString("profile_image"));

                            if (savePref.getStringLatest("user_type").equals("2")) {
                                savePref.setAddress(body.getString("address"));
                                savePref.setStringLatest("lat", body.getString("lat"));
                                savePref.setStringLatest("lng", body.getString("lng"));
                                savePref.setStringLatest("description", body.getString("description"));
                            }

                            finish();
                            util.showToast(context, getString(R.string.profile_updated));

                        } else {
                            signInOrSignup.startAnimation(AnimationUtils.loadAnimation(context, R.anim.shake));
                            util.IOSDialog(context, jsonMainobject.getString("msg"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
                CropImage.ActivityResult result = CropImage.getActivityResult(data);
                if (resultCode == RESULT_OK) {
                    Uri resultUri = result.getUri();

                    selectedimage = getAbsolutePath(this, resultUri);

                    Glide.with(this).load(selectedimage).into(image);


                } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                    Exception error = result.getError();
                }
            } else if (resultCode == context.RESULT_OK) {
                switch (requestCode) {
                    case PLACE_PICKER_REQUEST:
                        Place place = Autocomplete.getPlaceFromIntent(data);
                        String placeName = String.valueOf(place.getName());
                        address.setText(placeName);
                        latitude = String.valueOf(place.getLatLng().latitude);
                        longitude = String.valueOf(place.getLatLng().longitude);

                }
            }
        }
    }

    public String getAbsolutePath(Context activity, Uri uri) {

        if ("content".equalsIgnoreCase(uri.getScheme())) {
            String[] projection = {"_data"};
            Cursor cursor = null;
            try {
                cursor = activity.getContentResolver().query(uri, projection, null, null, null);
                int column_index = cursor.getColumnIndexOrThrow("_data");
                if (cursor.moveToFirst()) {
                    return cursor.getString(column_index);
                }
            } catch (Exception e) {
                // Eat it
                e.printStackTrace();
            }
        } else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

}
