package com.ez_schedule.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.ez_schedule.MainActivity;
import com.ez_schedule.R;
import com.ez_schedule.Util.Parameters;
import com.ez_schedule.Util.SavePref;
import com.ez_schedule.Util.util;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class ConnectPaymentGatewayActivity extends AppCompatActivity {

    ConnectPaymentGatewayActivity context;
    private SavePref savePref;
    @BindView(R.id.connect_with_paypal)
    TextView connectWithPaypal;
    @BindView(R.id.connect_with_stripe)
    TextView connectWithStripe;
    @BindView(R.id.back_button)
    ImageView backButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connect_payment_gateway);
        ButterKnife.bind(this);

        context = ConnectPaymentGatewayActivity.this;
        savePref = new SavePref(context);

       /* if (!savePref.getStringLatest(Parameters.PAYPALID).isEmpty()) {
            connectWithPaypal.setCompoundDrawablesWithIntrinsicBounds(R.drawable.tick, 0, 0, 0);
        }*/


    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!savePref.getStringLatest(Parameters.SQUARE_ID).isEmpty()|| savePref.getStringLatest(Parameters.SQUARE_ID).equals("0")) {
            connectWithStripe.setCompoundDrawablesWithIntrinsicBounds(R.drawable.tick_b, 0, 0, 0);
        }
    }

    @OnClick({R.id.connect_with_paypal, R.id.connect_with_stripe, R.id.back_button})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.connect_with_paypal:
                //showDialog();
                break;
            case R.id.connect_with_stripe:
                if (!savePref.getStringLatest(Parameters.SQUARE_ID).isEmpty()) {
                    util.IOSDialog(context, getString(R.string.account_attached));
                } else {
                    Intent intent = new Intent(context, WebViewActivity.class);
                    startActivity(intent);
                    overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                }

                break;
            case R.id.back_button:
                onBackPressed();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        finish();

    }

  /*  public void showDialog() {

        final Dialog dialog = new Dialog(context);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog);

        ImageView close = (ImageView) dialog.findViewById(R.id.close);
        EditText paypal_email_address = (EditText) dialog.findViewById(R.id.paypal_email_address);
        Button done = (Button) dialog.findViewById(R.id.done);
        dialog.show();

        paypal_email_address.setText(savePref.getStringLatest(Parameters.PAYPALID));


        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (paypal_email_address.getText().toString().trim().isEmpty()) {
                    util.IOSDialog(context, "Please Enter Your Paypal Email Address");
                } else if (!util.isValidEmail(paypal_email_address.getText().toString().trim())) {
                    util.IOSDialog(context, "Please Enter a Vaild Paypal Email Address");
                } else {
                  *//*  savePref.setStringLatest(Parameters.PAYPALID, paypal_email_address.getText().toString().trim());
                    connectWithPaypal.setCompoundDrawablesWithIntrinsicBounds(R.drawable.tick, 0, 0, 0);
                    dialog.dismiss();
                    EDIT_PROFILE_API(paypal_email_address.getText().toString().trim());*//*
                }
            }
        });


    }*/


}