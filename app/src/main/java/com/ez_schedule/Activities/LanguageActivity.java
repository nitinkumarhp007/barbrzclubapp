package com.ez_schedule.Activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.app.LocaleChangerAppCompatDelegate;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.ez_schedule.R;
import com.ez_schedule.Util.AppController;
import com.ez_schedule.Util.LocaleHelper;
import com.ez_schedule.Util.SavePref;
import com.ez_schedule.Util.util;
import com.franmontiel.localechanger.LocaleChanger;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LanguageActivity extends AppCompatActivity {
    LanguageActivity context;
    private SavePref savePref;
    @BindView(R.id.english)
    Button english;
    @BindView(R.id.spanish)
    Button spanish;
    Context context1;
    Resources resources;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_language);
        ButterKnife.bind(this);

        context = LanguageActivity.this;
        savePref = new SavePref(context);

        LocaleChanger.setLocale(AppController.SUPPORTED_LOCALES.get(0));

    }

    @OnClick({R.id.english, R.id.spanish})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.english:
                savePref.setLang("en");//English
                // LocaleChanger.setLocale(AppController.SUPPORTED_LOCALES.get(1));
                break;
            case R.id.spanish:
                savePref.setLang("es");//Spanish
                //LocaleChanger.setLocale(AppController.SUPPORTED_LOCALES.get(0));
                break;
        }
        savePref.setStringLatest("language_hit", "1");
        LocaleHelper.setLocale(LanguageActivity.this, savePref.getLang());

        //  Locale locale = new Locale();
        // Locale.setDefault(AppController.SUPPORTED_LOCALES.get(0));


        startActivity(new Intent(this, SignInActivity.class));
        overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
    }
}