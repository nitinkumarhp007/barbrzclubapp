package com.ez_schedule.Activities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import com.ez_schedule.R;
import com.ez_schedule.Util.ConnectivityReceiver;
import com.ez_schedule.Util.Parameters;
import com.ez_schedule.Util.SavePref;
import com.ez_schedule.Util.util;
import com.ez_schedule.parser.AllAPIS;
import com.ez_schedule.parser.GetAsync;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class ChangePasswordActivity extends AppCompatActivity {

    @BindView(R.id.back_button)
    ImageView backButton;
    @BindView(R.id.current_password)
    EditText currentPassword;
    @BindView(R.id.new_password)
    EditText newPassword;
    @BindView(R.id.confirm_new_password)
    EditText confirmNewPassword;
    @BindView(R.id.change)
    Button change;

    ChangePasswordActivity context;
    private SavePref savePref;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        ButterKnife.bind(this);

        context=ChangePasswordActivity.this;
        savePref=new SavePref(context);
    }

    @OnClick({R.id.back_button, R.id.change})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.back_button:
                finish();
                break;
            case R.id.change:
                if (ConnectivityReceiver.isConnected()) {
                    if (currentPassword.getText().toString().isEmpty()) {
                        util.IOSDialog(context, getString(R.string.enter_current_password));
                        change.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
                    } else if (newPassword.getText().toString().isEmpty()) {
                        util.IOSDialog(context, getString(R.string.enter_new_password));
                        change.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
                    } else if (confirmNewPassword.getText().toString().isEmpty()) {
                        util.IOSDialog(context, getString(R.string.enter_confirm_password));
                        change.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
                    } else if (!newPassword.getText().toString().equals(confirmNewPassword.getText().toString())) {
                        util.IOSDialog(context, getString(R.string.passwod_not_match));
                        change.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
                    } else {
                        CHANGE_PASSWORD_API();
                    }
                }
                break;
        }
    }
    private void CHANGE_PASSWORD_API() {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.OLD_PASSWORD, currentPassword.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.NEW_PASSWORD, newPassword.getText().toString().trim());
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.CHANGEPASSWORD, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonMainobject = new JSONObject(result);
                        if (jsonMainobject.getString("code").equalsIgnoreCase("200")) {
                            util.showToast(context, jsonMainobject.getString("msg"));
                            finish();
                            util.hideKeyboard(context);
                        } else {
                            util.showToast(context, jsonMainobject.getString("msg"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }
}
