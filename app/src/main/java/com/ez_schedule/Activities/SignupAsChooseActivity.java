package com.ez_schedule.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import com.ez_schedule.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SignupAsChooseActivity extends AppCompatActivity {

    @BindView(R.id.signup_as_user)
    Button signupAsUser;
    @BindView(R.id.signup_as_barbar)
    Button signupAsBarbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup_as_choose);
        ButterKnife.bind(this);
    }

    @OnClick({R.id.signup_as_user, R.id.signup_as_barbar})
    public void onClick(View view) {
        Intent intent = new Intent(this, SignUpActivity.class);
        switch (view.getId()) {
            case R.id.signup_as_user:
                intent.putExtra("type", "1");
                break;
            case R.id.signup_as_barbar:
                intent.putExtra("type", "2");
                break;
        }
        startActivity(intent);
        overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
    }
}
