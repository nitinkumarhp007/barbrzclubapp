package com.ez_schedule.Activities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ez_schedule.BarbarActivities.BarbarMainActivity;
import com.ez_schedule.BarbarAdapters.BarbarOrdersAdapter;
import com.ez_schedule.BarbarAdapters.BarbarSlotSwapAdapter;
import com.ez_schedule.MainActivity;
import com.ez_schedule.ModelClasses.BarbarServiceListModel;
import com.ez_schedule.ModelClasses.SwapRequestModel;
import com.ez_schedule.ModelClasses.UserOrdersModel;
import com.ez_schedule.R;
import com.ez_schedule.Util.ConnectivityReceiver;
import com.ez_schedule.Util.Parameters;
import com.ez_schedule.Util.SavePref;
import com.ez_schedule.Util.util;
import com.ez_schedule.parser.AllAPIS;
import com.ez_schedule.parser.GetAsync;
import com.ligl.android.widget.iosdialog.IOSDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class TimeSlotRequestListActivity extends AppCompatActivity {
    TimeSlotRequestListActivity context;
    @BindView(R.id.my_recycler_view)
    RecyclerView myRecyclerView;
    @BindView(R.id.error_message)
    TextView errorMessage;
    private SavePref savePref;
    BarbarSlotSwapAdapter adapter = null;
    ArrayList<SwapRequestModel> list_main;

    boolean is_from_push = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_time_slot_request_list);
        ButterKnife.bind(this);

        setToolbar();

        is_from_push = getIntent().getBooleanExtra("is_from_push", false);


        context = TimeSlotRequestListActivity.this;
        savePref = new SavePref(context);

        if (ConnectivityReceiver.isConnected()) {
            USER_SWAP_REQUEST();
        } else {
            util.IOSDialog(context, util.internet_Connection_Error);
        }

    }

    private void USER_SWAP_REQUEST() {
        ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.REQUEST_TYPE, "1");
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.USER_SWAP_REQUEST, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                list_main = new ArrayList<>();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {
                            JSONArray body = jsonmainObject.getJSONArray("body");
                            for (int i = 0; i < body.length(); i++) {
                                JSONObject object = body.getJSONObject(i);
                                SwapRequestModel swapRequestModel = new SwapRequestModel();
                                swapRequestModel.setFrom_request(object.getJSONObject("from_request").getJSONObject("slot_Detail").getString("start_time")+"-"+object.getJSONObject("from_request").getJSONObject("slot_Detail").getString("end_time"));
                                swapRequestModel.setTo_request(object.getJSONObject("to_request").getJSONObject("slot_Detail").getString("start_time")+"-"+object.getJSONObject("to_request").getJSONObject("slot_Detail").getString("end_time"));
                                swapRequestModel.setStatus("0");
                                swapRequestModel.setDate(object.getJSONObject("to_request").getString("date"));
                                swapRequestModel.setDate_to(object.getJSONObject("from_request").getString("date"));
                                swapRequestModel.setImage(object.getJSONObject("to_request").getJSONObject("barber").getString("profile_image"));
                                swapRequestModel.setName(object.getJSONObject("to_request").getJSONObject("barber").getString("username"));
                                swapRequestModel.setPrice(object.getJSONObject("to_request").getString("total_amount"));
                                swapRequestModel.setId(object.getJSONObject("request_detail").getString("id"));
                                list_main.add(swapRequestModel);
                            }


                            if (list_main.size() > 0) {
                                adapter = new BarbarSlotSwapAdapter(context, list_main);
                                myRecyclerView.setLayoutManager(new LinearLayoutManager(context));
                                myRecyclerView.setAdapter(adapter);

                                myRecyclerView.setVisibility(View.VISIBLE);
                                errorMessage.setVisibility(View.GONE);

                            } else {
                                errorMessage.setVisibility(View.VISIBLE);
                                myRecyclerView.setVisibility(View.GONE);
                            }

                        } else {
                            if (jsonmainObject.getString("msg").equals(util.Invalid_Authorization)) {
                                 savePref.setAuthorization_key("");
                                Intent intent = new Intent(context, SignInActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                            } else {
                                util.showToast(context, jsonmainObject.getString("msg"));
                            }
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    public void SWAP_REQUEST_STATUS_API(String request_id, String status) {
        int position = 0;
        for (int i = 0; i < list_main.size(); i++) {
            if (list_main.get(i).getId().equals(request_id))
                position = i;
        }
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.REQUEST_ID, request_id);
        formBuilder.addFormDataPart(Parameters.STATUS, status);
        RequestBody formBody = formBuilder.build();
        int finalPosition = position;
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.SWAP_REQUEST_STATUS, formBody, savePref.getAuthorization_key()) {

            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonObject = new JSONObject(result);
                        String s = "";
                        if (status.equals("1"))
                            s = "Request Accepted Successfully , Your Order timeslot Updated!";
                        else
                            s = "Request Declined Successfully";

                        new IOSDialog.Builder(context)
                                .setCancelable(false)
                                .setMessage(s).setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                list_main.get(finalPosition).setStatus(status);
                                adapter.notifyDataSetChanged();
                            }
                        }).show();
                    } catch (Exception e) {
                    }
                }

            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView title = (TextView) toolbar.findViewById(R.id.title);
        title.setText(R.string.Timeslot_swap_requests);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back_w);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    @Override
    public void onBackPressed() {
        if (is_from_push) {
            Intent intent = new Intent(context, MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
        } else {
            super.onBackPressed();
        }

    }
}
