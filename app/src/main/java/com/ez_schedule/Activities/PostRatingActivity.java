package com.ez_schedule.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;

import com.ez_schedule.R;
import com.ez_schedule.Util.ConnectivityReceiver;
import com.ez_schedule.Util.Parameters;
import com.ez_schedule.Util.SavePref;
import com.ez_schedule.Util.util;
import com.ez_schedule.parser.AllAPIS;
import com.ez_schedule.parser.GetAsync;
import com.ligl.android.widget.iosdialog.IOSDialog;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class PostRatingActivity extends AppCompatActivity {
    PostRatingActivity context;
    private SavePref savePref;

    @BindView(R.id.rating_bar)
    RatingBar ratingBar;
    @BindView(R.id.text)
    EditText text;
    @BindView(R.id.review)
    Button review;

    private String order_id = "",to_id="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_rating);
        ButterKnife.bind(this);

        context = PostRatingActivity.this;
        savePref = new SavePref(context);

        order_id = getIntent().getStringExtra("order_id");
        to_id = getIntent().getStringExtra("to_id");
        setToolbar();
    }

    @OnClick(R.id.review)
    public void onClick() {
        if (ConnectivityReceiver.isConnected()) {
            if (String.valueOf(ratingBar.getRating()).equals("0.0")) {
                util.IOSDialog(context, getString(R.string.select_rating));
                review.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (text.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, getString(R.string.enter_review));
                review.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else {
                RATE_PROVIDER_API();
            }
        } else {
            util.IOSDialog(context, util.internet_Connection_Error);
        }
    }

    public void RATE_PROVIDER_API() {
        util.hideKeyboard(context);
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.TO_ID, to_id);
        formBuilder.addFormDataPart(Parameters.RATING, String.valueOf(ratingBar.getRating()));
        formBuilder.addFormDataPart(Parameters.TYPE, "1");//3=strike
        formBuilder.addFormDataPart(Parameters.ORDER_ID, order_id);
        formBuilder.addFormDataPart(Parameters.REVIEW, text.getText().toString().trim());
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.POST_RATING, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonObject = new JSONObject(result);
                        if (jsonObject.getString("code").equalsIgnoreCase("200")) {
                            new IOSDialog.Builder(context)
                                    .setCancelable(false)
                                    .setMessage(getString(R.string.review_posted)).setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    finish();
                                }
                            }).show();
                        } else {
                            util.IOSDialog(context, jsonObject.getString("msg"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }


    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView title = (TextView) toolbar.findViewById(R.id.title);
        title.setText(R.string.write_review);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back_w);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }


}
