package com.ez_schedule.Activities;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.ez_schedule.MainActivity;
import com.ez_schedule.R;
import com.ez_schedule.Util.ConnectivityReceiver;
import com.ez_schedule.Util.Parameters;
import com.ez_schedule.Util.SavePref;
import com.ez_schedule.Util.util;
import com.ez_schedule.parser.AllAPIS;
import com.ez_schedule.parser.GetAsync;
import com.bumptech.glide.Glide;
import com.ligl.android.widget.iosdialog.IOSDialog;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import sqip.Card;
import sqip.CardDetails;
import sqip.CardEntry;

import static sqip.CardEntry.DEFAULT_CARD_ENTRY_REQUEST_CODE;

public class CharityActivity extends AppCompatActivity {
    CharityActivity context;
    @BindView(R.id.scrollView)
    ScrollView scrollView;
    private SavePref savePref;

    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.description)
    TextView description;
    @BindView(R.id.amount)
    EditText amount;
    @BindView(R.id.submit)
    Button submit;
    @BindView(R.id.image)
    ImageView image;

    String charity_id = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_charity);
        ButterKnife.bind(this);

        context = CharityActivity.this;
        savePref = new SavePref(context);

        setToolbar();

        GET_DEFAULT_CHARITY(true);


    }


    private void Alert(String name_text, String description_text, String image_text) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.charity_layout);

        ImageView image_1 = (ImageView) dialog.findViewById(R.id.image);
        TextView name = (TextView) dialog.findViewById(R.id.name);
        TextView description = (TextView) dialog.findViewById(R.id.description);
        TextView accept = (TextView) dialog.findViewById(R.id.accept);
        TextView decline = (TextView) dialog.findViewById(R.id.decline);


        Glide.with(context).load(image_text).into(image_1);
        name.setText(name_text);
        description.setText(description_text);


        accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scrollView.setVisibility(View.VISIBLE);
                dialog.dismiss();
            }
        });
        decline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                onBackPressed();
            }
        });

        dialog.show();
    }


    private void GET_DEFAULT_CHARITY(boolean is_first) {
        ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.AUTH_KEY, "");
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.GET_DEFAULT_CHARITY, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();

                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {
                            JSONObject body = jsonmainObject.getJSONObject("body");

                            scrollView.setVisibility(View.INVISIBLE);

                            charity_id = body.getString("id");
                            name.setText(body.getString("name"));
                            description.setText(body.getString("description"));
                            Glide.with(context).load(body.getString("image")).into(image);

                            if (is_first)
                                Alert(body.getString("name"), body.getString("description"), body.getString("image"));

                        } else {
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }


    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView title = (TextView) toolbar.findViewById(R.id.title);
        title.setText(R.string.donate_money_to_charity);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back_w);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    @OnClick(R.id.submit)
    public void onClick() {
        if (ConnectivityReceiver.isConnected()) {
            /*if (amount.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, getString(R.string.enter_amount));
            } else if (Integer.parseInt(amount.getText().toString().trim()) < 5) {
                util.IOSDialog(context, getString(R.string.minimum_shoule_be));
            } else {
                CardEntry.startCardEntryActivity(context, true,
                        DEFAULT_CARD_ENTRY_REQUEST_CODE);

            }*/

            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.aplos.com/aws/give/ComfortShield/ez"));
            startActivity(browserIntent);

        } else {
            util.IOSDialog(context, util.internet_Connection_Error);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        CardEntry.handleActivityResult(data, result -> {
            if (result.isSuccess()) {
                CardDetails cardResult = result.getSuccessValue();
                Card card = cardResult.getCard();
                String nonce = cardResult.getNonce();

                /*Toast.makeText(context,
                        "Payment Successfully Done",
                        Toast.LENGTH_SHORT)
                        .show();*/

                Log.e("position__", nonce);

                CHARITY_PAYMENT(nonce);


            } else if (result.isCanceled()) {
                Toast.makeText(context,
                        R.string.canceled,
                        Toast.LENGTH_SHORT)
                        .show();
            }
        });
    }

    public void CHARITY_PAYMENT(String payment_nonce) {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.CHARITY_ID, charity_id);
        formBuilder.addFormDataPart(Parameters.PAYMENT_NONCE, payment_nonce);
        formBuilder.addFormDataPart(Parameters.AMOUNT, amount.getText().toString().trim());
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.CHARITY_PAYMENT, formBody, savePref.getAuthorization_key()) {

            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonMainobject = new JSONObject(result);
                        if (jsonMainobject.getString("code").equalsIgnoreCase("200")) {
                            new IOSDialog.Builder(context)
                                    .setCancelable(false)
                                    .setMessage(getString(R.string.payment_done)).setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    onBackPressed();
                                }
                            }).show();
                        } else {
                            util.showToast(context, jsonMainobject.getString("msg"));
                        }


                    } catch (Exception e) {
                    }
                }

            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();

    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(context, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
    }
}