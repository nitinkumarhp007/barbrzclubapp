package com.ez_schedule.Activities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import com.ez_schedule.BarbarActivities.BarbarMainActivity;
import com.ez_schedule.MainActivity;
import com.ez_schedule.R;
import com.ez_schedule.Util.ConnectivityReceiver;
import com.ez_schedule.Util.Parameters;
import com.ez_schedule.Util.SavePref;
import com.ez_schedule.Util.util;
import com.ez_schedule.parser.AllAPIS;
import com.ez_schedule.parser.GetAsync;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class OTPActivity extends AppCompatActivity {
    OTPActivity context;
    private SavePref savePref;

    @BindView(R.id.back_button)
    ImageView backButton;
    @BindView(R.id.num1)
    EditText num1;
    @BindView(R.id.num2)
    EditText num2;
    @BindView(R.id.num3)
    EditText num3;
    @BindView(R.id.num4)
    EditText num4;
    @BindView(R.id.submit)
    Button submit;
    String type = "", authorization_key = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp);
        ButterKnife.bind(this);

        context = OTPActivity.this;
        savePref = new SavePref(context);
        authorization_key = getIntent().getStringExtra("authorization_key");
        type = getIntent().getStringExtra("type");

       task();
    }

    private void task()
    {
        num1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!s.toString().isEmpty()) {
                    num2.requestFocus();
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        num2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!s.toString().isEmpty()) {
                    num3.requestFocus();
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        num3.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!s.toString().isEmpty()) {
                    num4.requestFocus();
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        num4.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!s.toString().isEmpty()) {
                    util.hideKeyboard(context);
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void OTPTASK() {
        util.hideKeyboard(context);
        if (ConnectivityReceiver.isConnected()) {
            if (num1.getText().toString().isEmpty() || num2.getText().toString().isEmpty() ||
                    num3.getText().toString().isEmpty() || num4.getText().toString().isEmpty()) {
                util.IOSDialog(context, getString(R.string.otp_password));
                submit.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else {
                String entered_otp = num1.getText().toString() + num2.getText().toString() + num3.getText().toString() + num4.getText().toString();
                VERIFY_OTP_API(entered_otp);
            }
        } else
            util.showToast(context, util.internet_Connection_Error);
    }

    private void VERIFY_OTP_API(String otp) {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.OTP, otp);
        formBuilder.addFormDataPart(Parameters.DEVICE_TOKEN, SavePref.getDeviceToken(this, "token"));
        formBuilder.addFormDataPart(Parameters.DEVICE_TYPE, "1");
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.VERIFY_OTP, formBody, authorization_key) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonMainobject = new JSONObject(result);
                        if (jsonMainobject.getString("code").equalsIgnoreCase("200")) {
                            util.hideKeyboard(context);
                            JSONObject body = jsonMainobject.getJSONObject("body");
                            savePref.setAuthorization_key(body.getString("auth_key"));
                            savePref.setID(body.getString("id"));
                            savePref.setEmail(body.getString("email"));
                            savePref.setName(body.getString("username"));
                            if(body.getString("user_type").equals("2"))
                            {
                                savePref.setStringLatest(Parameters.START_TIME_CALL, body.optString(Parameters.START_TIME_CALL));
                                savePref.setStringLatest(Parameters.END_TIME_CALL, body.optString(Parameters.END_TIME_CALL));
                                savePref.setAddress(body.getString("address"));
                                savePref.setStringLatest("lat", body.getString("lat"));
                                savePref.setStringLatest("lng", body.getString("lng"));
                                savePref.setStringLatest("description", body.getString("description"));
                                savePref.setStringLatest(Parameters.REWARD_ORDER_COUNT, body.optString(Parameters.REWARD_ORDER_COUNT));
                                savePref.setStringLatest(Parameters.REWARD_PERCENTANGE, body.optString(Parameters.REWARD_PERCENTANGE));

                            }

                            savePref.setStringLatest("user_type", body.getString("user_type"));
                            savePref.setPhone(body.getString("phone"));
                            savePref.setImage(body.getString("profile_image"));
                            savePref.setStringLatest("is_from_social", "0");
                            if (type.equals("1")) {
                                Intent intent = new Intent(context, MainActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                                util.showToast(context, getString(R.string.welcome_to) + getResources().getString(R.string.app_name));
                            } else {
                                Intent intent = new Intent(context, BarbarMainActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                                util.showToast(context, getString(R.string.welcome_to) + getResources().getString(R.string.app_name));
                            }

                        } else {
                            util.IOSDialog(context, jsonMainobject.getString("msg"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    @OnClick({R.id.back_button, R.id.submit})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.back_button:
                finish();
                break;
            case R.id.submit:
                OTPTASK();
                break;
        }
    }
}
