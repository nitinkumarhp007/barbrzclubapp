package com.ez_schedule.Activities;

import android.annotation.SuppressLint;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.ez_schedule.R;
import com.ez_schedule.Util.ConnectivityReceiver;
import com.ez_schedule.Util.Parameters;
import com.ez_schedule.Util.SavePref;
import com.ez_schedule.Util.util;
import com.ez_schedule.parser.AllAPIS;
import com.ez_schedule.parser.GetAsync;
import com.bumptech.glide.Glide;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.mcsoft.timerangepickerdialog.RangeTimePickerDialog;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;
import com.ybs.countrypicker.CountryPicker;
import com.ybs.countrypicker.CountryPickerListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class SignUpActivity extends AppCompatActivity implements RangeTimePickerDialog.ISelectedTime {
    SignUpActivity context;
    @BindView(R.id.back_button)
    ImageView backButton;
    @BindView(R.id.image)
    CircleImageView image;
    @BindView(R.id.username)
    EditText username;
    @BindView(R.id.email_address)
    EditText emailAddress;
    @BindView(R.id.phone)
    EditText phone;
    @BindView(R.id.country)
    TextView country;
    @BindView(R.id.password)
    EditText password;
    @BindView(R.id.sign_in_or_signup)
    Button signInOrSignup;
    @BindView(R.id.sign_in)
    Button signIn;
    @BindView(R.id.login_text)
    TextView loginText;
    String start_time_call = "", end_time_call = "";
    String type = "";
    @BindView(R.id.select_opening_closing_time)
    TextView select_opening_closing_time;
    @BindView(R.id.address)
    TextView address;
    @BindView(R.id.description)
    EditText description;

    @BindView(R.id.term_check)
    CheckBox termCheck;
    @BindView(R.id.term_check_text)
    TextView termCheckText;
    @BindView(R.id.privacy)
    CheckBox privacy;
    @BindView(R.id.privacy_text)
    TextView privacyText;
    @BindView(R.id.country_code)
    TextView countryCode;


    private String selectedimage = "";
    Uri fileUri;
    private final int PLACE_PICKER_REQUEST = 787;
    String latitude = "", longitude = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        ButterKnife.bind(this);

        context = SignUpActivity.this;

        type = getIntent().getStringExtra("type");

        if (type.equals("1")) {
            loginText.setText(getString(R.string.signup_as_user));

            address.setVisibility(View.GONE);
            description.setVisibility(View.GONE);
            select_opening_closing_time.setVisibility(View.GONE);
        } else {
            loginText.setText(getString(R.string.signup_as_barbar));
        }


    }

    @OnClick({R.id.country_code, R.id.select_opening_closing_time, R.id.term_check_text, R.id.privacy_text,
            R.id.back_button, R.id.image, R.id.address, R.id.sign_in_or_signup, R.id.sign_in})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.country_code:
                CountryPicker picker = CountryPicker.newInstance(getString(R.string.select_country));  // dialog title
                picker.setListener(new CountryPickerListener() {
                    @Override
                    public void onSelectCountry(String name, String code1, String dialCode, int flagDrawableResID) {
                        picker.dismiss();
                        countryCode.setText(dialCode);
                    }
                });
                picker.show(getSupportFragmentManager(), "COUNTRY_PICKER");
                break;
            case R.id.select_opening_closing_time:
                RangeTimePickerDialog dialog = new RangeTimePickerDialog();
                dialog.newInstance();
                dialog.setRadiusDialog(20); // Set radius of dialog (default is 50)
                dialog.setIs24HourView(false); // Indicates if the format should be 24 hours
                dialog.setColorBackgroundHeader(R.color.colorPrimary); // Set Color of Background header dialog
                dialog.setColorTextButton(R.color.colorPrimaryDark); // Set Text color of button
                FragmentManager fragmentManager = getFragmentManager();
                dialog.show(fragmentManager, "");
                break;
            case R.id.back_button:
                finish();
                break;
            case R.id.image:
                CropImage.activity(fileUri)
                        .setAspectRatio(2, 2)
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .start(this);
                break;
            case R.id.address:
                Locationget();
                break;
            case R.id.sign_in_or_signup:
                SignUpProcess();
                break;
            case R.id.sign_in:
                startActivity(new Intent(this, SignInActivity.class));
                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
            case R.id.term_check_text:
                Intent intent11 = new Intent(context, TermConditionActivity.class);
                intent11.putExtra("type", "term");
                startActivity(intent11);
                break;
            case R.id.privacy_text:
                Intent intent11111 = new Intent(context, TermConditionActivity.class);
                intent11111.putExtra("type", "privacy");
                startActivity(intent11111);
                break;
        }
    }

    private void Locationget() {
        // Initialize Places.
        Places.initialize(getApplicationContext(), "AIzaSyAA-k3KpN8PbS5u4_9qLFGIFZ_fIm52iM4");
        // Create a new Places client instance.
        PlacesClient placesClient = Places.createClient(this);
        // Set the fields to specify which types of place data to return.
        List<Place.Field> fields = Arrays.asList(Place.Field.ID,
                Place.Field.NAME, Place.Field.LAT_LNG, Place.Field.ADDRESS, Place.Field.ID, Place.Field.PHONE_NUMBER, Place.Field.RATING, Place.Field.WEBSITE_URI);
        // Start the autocomplete intent.
        Intent intent = new Autocomplete.IntentBuilder(AutocompleteActivityMode.OVERLAY, fields).build(this);

        startActivityForResult(intent, PLACE_PICKER_REQUEST);
    }

    @Override
    public void onSelectedTime(int hourStart, int minuteStart, int hourEnd, int minuteEnd) {

        start_time_call = String.valueOf(hourStart);
        end_time_call = String.valueOf(hourEnd);

        if (start_time_call.length() == 1)
            start_time_call = "0" + start_time_call;
        if (end_time_call.length() == 1)
            end_time_call = "0" + end_time_call;

        String start_time_12 = time_24_12(Integer.parseInt(start_time_call), minuteStart);
        String end_time_12 = time_24_12(Integer.parseInt(end_time_call), minuteEnd);


        //  start_time_call = start_time_call + ":" + minuteStart;
        //end_time_call = end_time_call + ":" + minuteEnd;

        Log.e("start_time_call", "Now" + start_time_12 + "    " + end_time_12);


        select_opening_closing_time.setText(start_time_12 + " To " + end_time_12);

    }

    private String time_24_12(int hourOfDay, int minute) {
       /* try {
            final SimpleDateFormat sdf = new SimpleDateFormat("H:mm");
            final Date dateObj = sdf.parse(time);
            System.out.println(dateObj);
            System.out.println(new SimpleDateFormat("K:mm").format(dateObj));
            return new SimpleDateFormat("K:mm").format(dateObj);
        } catch (final ParseException e) {
            e.printStackTrace();
        }
        return null;*/

        return ((hourOfDay > 12) ? hourOfDay % 12 : hourOfDay) + ":" + (minute < 10 ? ("0" + minute) : minute) + " " + ((hourOfDay >= 12) ? "PM" : "AM");
    }

    private void SignUpProcess() {
        if (ConnectivityReceiver.isConnected()) {
            if (selectedimage.isEmpty()) {
                util.IOSDialog(context, getString(R.string.select_image));
                signInOrSignup.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (username.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, getString(R.string.enter_name));
                signInOrSignup.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (emailAddress.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, getString(R.string.enter_email));
                signInOrSignup.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (!util.isValidEmail(emailAddress.getText().toString().trim())) {
                util.IOSDialog(context, getString(R.string.enter_vaild_email));
                signInOrSignup.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (phone.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, getString(R.string.enter_phone));
                signInOrSignup.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (password.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context,getString(R.string.enter_password));
                signInOrSignup.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (!termCheck.isChecked()) {
                util.IOSDialog(context, getString(R.string.accept_terms_of_use));
                signInOrSignup.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (!privacy.isChecked()) {
                util.IOSDialog(context, getString(R.string.accept_privacy_policy));
                signInOrSignup.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else {
                if (type.equals("2")) {
                    if (address.getText().toString().trim().isEmpty()) {
                        util.IOSDialog(context, getString(R.string.select_address));
                        signInOrSignup.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
                    } /*else if (select_opening_closing_time.getText().toString().trim().isEmpty()) {
                        util.IOSDialog(context, "Please Select Opening Closing Time");
                        signInOrSignup.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
                    }*/ else if (description.getText().toString().trim().isEmpty()) {
                        util.IOSDialog(context, getString(R.string.enter_description));
                        signInOrSignup.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
                    } else {
                        // USER_SIGNUP_API();
                        Intent intent = new Intent(context, SignupTimeSlotActivity.class);
                        intent.putExtra("selectedimage", selectedimage);
                        intent.putExtra("username", username.getText().toString().trim());
                        intent.putExtra("emailAddress", emailAddress.getText().toString().trim());
                        intent.putExtra("password", password.getText().toString().trim());
                        intent.putExtra("phone", countryCode.getText().toString().trim().substring(1) + phone.getText().toString().trim());
                        intent.putExtra("address", address.getText().toString().trim());
                        intent.putExtra("description", description.getText().toString().trim());
                        intent.putExtra("latitude", latitude);
                        intent.putExtra("longitude", longitude);
                        intent.putExtra("type", type);
                        startActivity(intent);
                        overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);


                    }
                } else {
                    USER_SIGNUP_API();
                }

            }
        } else {
            util.IOSDialog(context, util.internet_Connection_Error);
        }

    }


    private void USER_SIGNUP_API() {
        util.hideKeyboard(context);
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        if (!selectedimage.isEmpty()) {
            final MediaType MEDIA_TYPE = selectedimage.endsWith("png") ?
                    MediaType.parse("image/png") : MediaType.parse("image/jpeg");

            File file = new File(selectedimage);
            formBuilder.addFormDataPart(Parameters.PROFILE_IMAGE, file.getName(), RequestBody.create(MEDIA_TYPE, file));
        }

        formBuilder.addFormDataPart(Parameters.USERNAME, username.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.EMAIL, emailAddress.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.PASSWORD, password.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.PHONE, countryCode.getText().toString().trim().substring(1) + phone.getText().toString().trim());

        formBuilder.addFormDataPart(Parameters.USER_TYPE, type);
        formBuilder.addFormDataPart(Parameters.SCHEDULE, type);

        if (type.equals("2")) {
            formBuilder.addFormDataPart(Parameters.ADDRESS, address.getText().toString().trim());
            formBuilder.addFormDataPart(Parameters.LAT, latitude);
            formBuilder.addFormDataPart(Parameters.LNG, longitude);
            formBuilder.addFormDataPart(Parameters.DESCRIPTION, description.getText().toString().trim());
            formBuilder.addFormDataPart(Parameters.OPEN_TIME, start_time_call);
            formBuilder.addFormDataPart(Parameters.CLOSE_TIME, end_time_call);
        } else {
            formBuilder.addFormDataPart(Parameters.OPEN_TIME, "9:00");
            formBuilder.addFormDataPart(Parameters.CLOSE_TIME, "11:00");
        }

        formBuilder.addFormDataPart(Parameters.DEVICE_TYPE, "1");
        formBuilder.addFormDataPart(Parameters.DEVICE_TOKEN, SavePref.getDeviceToken(this, "token"));
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.USER_SIGNUP, formBody, "") {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonMainobject = new JSONObject(result);
                        if (jsonMainobject.getString("code").equalsIgnoreCase("200")) {
                            util.hideKeyboard(context);
                            Intent intent = new Intent(context, OTPActivity.class);
                            intent.putExtra("authorization_key", jsonMainobject.getJSONObject("body").getString("auth_key"));
                            intent.putExtra("type", type);
                            startActivity(intent);
                            overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                            // util.showToast(context, "User Registered Sucessfully!!!");

                        } else {
                            signInOrSignup.startAnimation(AnimationUtils.loadAnimation(context, R.anim.shake));
                            util.IOSDialog(context, jsonMainobject.getString("msg"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
                CropImage.ActivityResult result = CropImage.getActivityResult(data);
                if (resultCode == RESULT_OK) {
                    Uri resultUri = result.getUri();

                    selectedimage = getAbsolutePath(this, resultUri);

                    Glide.with(this).load(selectedimage).into(image);


                } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                    Exception error = result.getError();
                }
            } else if (resultCode == context.RESULT_OK) {
                switch (requestCode) {
                    case PLACE_PICKER_REQUEST:
                        Place place = Autocomplete.getPlaceFromIntent(data);
                        address.setText(String.valueOf(place.getName()));
                        latitude = String.valueOf(place.getLatLng().latitude);
                        longitude = String.valueOf(place.getLatLng().longitude);

                }
            }
        }
    }

    public String getAbsolutePath(Context activity, Uri uri) {

        if ("content".equalsIgnoreCase(uri.getScheme())) {
            String[] projection = {"_data"};
            Cursor cursor = null;
            try {
                cursor = activity.getContentResolver().query(uri, projection, null, null, null);
                int column_index = cursor.getColumnIndexOrThrow("_data");
                if (cursor.moveToFirst()) {
                    return cursor.getString(column_index);
                }
            } catch (Exception e) {
                // Eat it
                e.printStackTrace();
            }
        } else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }


}
