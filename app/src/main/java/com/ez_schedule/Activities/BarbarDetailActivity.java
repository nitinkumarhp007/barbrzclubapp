package com.ez_schedule.Activities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ez_schedule.BarbarActivities.CheckOutActivity;
import com.ez_schedule.MainActivity;
import com.ez_schedule.ModelClasses.BarbarServiceListModel;
import com.ez_schedule.R;
import com.ez_schedule.UserAdapters.DetailServicesAdapter;
import com.ez_schedule.Util.ConnectivityReceiver;
import com.ez_schedule.Util.Parameters;
import com.ez_schedule.Util.SavePref;
import com.ez_schedule.Util.util;
import com.ez_schedule.parser.AllAPIS;
import com.ez_schedule.parser.GetAsync;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class BarbarDetailActivity extends AppCompatActivity {
    BarbarDetailActivity context;
    @BindView(R.id.profile_pic)
    ImageView image;
    @BindView(R.id.rating_bar)
    RatingBar rating_bar;
    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.location)
    TextView location;
    @BindView(R.id.description)
    TextView description;
    @BindView(R.id.share)
    ImageView share;
    @BindView(R.id.my_recycler_view)
    RecyclerView myRecyclerView;
    @BindView(R.id.next)
    Button next;
    ArrayList<BarbarServiceListModel> list;
    String id = "";
    @BindView(R.id.scrollView)
    NestedScrollView scrollView;
    @BindView(R.id.rating_bar_layout)
    RelativeLayout ratingBarLayout;

    String access_token = "";

    private SavePref savePref;

    boolean is_from_push = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_barbar_detail);
        ButterKnife.bind(this);
        setToolbar();

        context = this;
        savePref = new SavePref(context);

        id = getIntent().getStringExtra("id");
        is_from_push = getIntent().getBooleanExtra("is_from_push", false);


        if (ConnectivityReceiver.isConnected()) {
            BARBAR_PROFILE();
        } else {
            util.IOSDialog(context, util.internet_Connection_Error);
        }


        ratingBarLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ReviewListActivity.class);
                intent.putExtra("user_id", id);
                startActivity(intent);
                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
            }
        });

        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sharepost(id);
            }
        });
    }

    private void sharepost(String post_id) {


        Log.e("post_id", post_id);

        // Sending side
        byte[] data = post_id.getBytes(StandardCharsets.UTF_8);
        String post_id_encode = Base64.encodeToString(data, Base64.DEFAULT);

        Log.e("post_id", "yes" + post_id_encode);

        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        //  sharingIntent.putExtra(Intent.EXTRA_TEXT, "http://3.22.158.181/spatify/users/open/" + post_id);
        sharingIntent.putExtra(Intent.EXTRA_TEXT, "http://ezschedule43.com:8081/deeplink?url=app" + post_id_encode);
        context.startActivity(Intent.createChooser(sharingIntent, getString(R.string.share_profile_external)));
    }

    private void BARBAR_PROFILE() {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.BARBER_ID, id);
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.BARBAR_PROFILE, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                list = new ArrayList<>();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {

                            JSONObject body = jsonmainObject.getJSONObject("body");

                            name.setText(body.getString("username"));
                            description.setText(body.getString("description"));

                            access_token = body.optString("access_token");


                            location.setText(body.getString("address"));
                            if (!body.getString("avg_rating").isEmpty())
                                rating_bar.setRating(Float.parseFloat(body.getString("avg_rating")));

                            Glide.with(context)
                                    .load(body.getString("profile_image"))
                                    .listener(new RequestListener<Drawable>() {
                                        @Override
                                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                            // holder.progressBar.setVisibility(View.GONE);
                                            return false;
                                        }

                                        @Override
                                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                            //holder.progressBar.setVisibility(View.GONE);
                                            return false;
                                        }
                                    })
                                    .into(image);


                            JSONArray services = body.getJSONArray("services");
                            for (int i = 0; i < services.length(); i++) {
                                JSONObject object = services.getJSONObject(i);

                                BarbarServiceListModel barbarListModel = new BarbarServiceListModel();
                                barbarListModel.setCategory_id(object.optString("category_id"));
                                //  barbarListModel.setCategory_name(object.getString("category_name"));
                                barbarListModel.setDescription(object.getString("description"));
                                barbarListModel.setId(object.getString("id"));
                                barbarListModel.setImage(object.optString("image"));
                                barbarListModel.setName(object.getString("name"));
                                barbarListModel.setPrice(object.getString("price"));
                                barbarListModel.setDuration(object.optString("duration"));
                                list.add(barbarListModel);

                            }

                            DetailServicesAdapter adapter = new DetailServicesAdapter(context, list);
                            myRecyclerView.setLayoutManager(new GridLayoutManager(context, 2));
                            myRecyclerView.setAdapter(adapter);

                            scrollView.setVisibility(View.VISIBLE);
                            next.setVisibility(View.VISIBLE);

                        } else {
                            if (jsonmainObject.getString("msg").equals(util.Invalid_Authorization)) {
                                 savePref.setAuthorization_key("");
                                Intent intent = new Intent(context, SignInActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                            } else {
                                util.showToast(context, jsonmainObject.getString("msg"));
                            }
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView title = (TextView) toolbar.findViewById(R.id.title);
        title.setText(R.string.detail);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back_w);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    @Override
    public void onBackPressed() {
        if (is_from_push) {
            Intent intent = new Intent(context, MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        } else
            super.onBackPressed();

    }

    @OnClick(R.id.next)
    public void onClick() {
        if (list.size() > 0) {
            Intent intent = new Intent(this, CheckOutActivity.class);
            intent.putExtra("barber_id", id);
            intent.putParcelableArrayListExtra("service_list", list);
            startActivity(intent);
            overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
        } else {
            util.IOSDialog(context, getString(R.string.no_service_found_to_book));
        }
    }
}
