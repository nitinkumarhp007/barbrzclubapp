package com.ez_schedule.Activities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ez_schedule.MainActivity;
import com.ez_schedule.ModelClasses.TimeslotModel;
import com.ez_schedule.R;
import com.ez_schedule.UserAdapters.RequestTimeSlotAdapter;
import com.ez_schedule.Util.Parameters;
import com.ez_schedule.Util.SavePref;
import com.ez_schedule.Util.util;
import com.ez_schedule.parser.AllAPIS;
import com.ez_schedule.parser.GetAsync;
import com.ligl.android.widget.iosdialog.IOSDialog;

import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class SlotChangeRequestActivity extends AppCompatActivity {
    SlotChangeRequestActivity context;
    @BindView(R.id.my_recycler_view)
    RecyclerView myRecyclerView;
    @BindView(R.id.error_message)
    TextView errorMessage;
    private SavePref savePref;
    String from_order_id = "";
    ArrayList<TimeslotModel> timeslot_list;
    RequestTimeSlotAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_slot_change_request);
        ButterKnife.bind(this);
        setToolbar();

        context = SlotChangeRequestActivity.this;
        savePref = new SavePref(context);

        timeslot_list = getIntent().getParcelableArrayListExtra("list");
        from_order_id = getIntent().getStringExtra("from_order_id");

        adapter = new RequestTimeSlotAdapter(context, timeslot_list);
        myRecyclerView.setLayoutManager(new LinearLayoutManager(context));
        myRecyclerView.setAdapter(adapter);


    }

    public void SWAP_REQUESTS_API(int position) {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.FROM_ORDER_ID, from_order_id);
        formBuilder.addFormDataPart(Parameters.TO_ORDER_ID, timeslot_list.get(position).getOrder_id());
        formBuilder.addFormDataPart(Parameters.TO_USER_ID, timeslot_list.get(position).getUser_id());
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.SWAP_REQUESTS, formBody, savePref.getAuthorization_key()) {

            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonMainobject = new JSONObject(result);
                        if (jsonMainobject.getString("code").equalsIgnoreCase("200")) {
                            new IOSDialog.Builder(context)
                                    .setCancelable(false)
                                    .setMessage(getString(R.string.request_sent)).setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    timeslot_list.get(position).setSwap_request_sent("1");
                                    adapter.notifyDataSetChanged();
                                }
                            }).show();
                        } else {
                            util.showToast(context, jsonMainobject.getString("msg"));
                        }


                    } catch (Exception e) {
                    }
                }

            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView title = (TextView) toolbar.findViewById(R.id.title);
        title.setText(R.string.change_time_slot);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back_w);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }

}
