package com.ez_schedule.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.ez_schedule.R;
import com.ez_schedule.Util.ConnectivityReceiver;
import com.ez_schedule.Util.Parameters;
import com.ez_schedule.Util.SavePref;
import com.ez_schedule.Util.util;
import com.ez_schedule.parser.AllAPIS;
import com.ez_schedule.parser.GetAsync;
import com.ligl.android.widget.iosdialog.IOSDialog;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import sqip.Card;
import sqip.CardDetails;
import sqip.CardEntry;

import static sqip.CardEntry.DEFAULT_CARD_ENTRY_REQUEST_CODE;

public class SubscriptionActivity extends AppCompatActivity {
    SubscriptionActivity context;
    SavePref savePref;
    @BindView(R.id.back_button)
    ImageView back_button;
    @BindView(R.id.subscription_price_text)
    TextView subscription_price_text;
    @BindView(R.id.proceed)
    Button proceed;

    String subscription_price = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subscription);

        ButterKnife.bind(this);

        context = SubscriptionActivity.this;

        subscription_price = getIntent().getStringExtra("subscription_price");

        subscription_price_text.setText("$" + subscription_price + " " + getString(R.string.per_month));

        savePref = new SavePref(context);

        proceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ProceedPayment();
            }
        });
        back_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    public void ProceedPayment() {
        if (ConnectivityReceiver.isConnected()) {
            CardEntry.startCardEntryActivity(context, true,
                    DEFAULT_CARD_ENTRY_REQUEST_CODE);
        } else {
            util.IOSDialog(context, util.internet_Connection_Error);
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        CardEntry.handleActivityResult(data, result -> {
            if (result.isSuccess()) {
                CardDetails cardResult = result.getSuccessValue();
                Card card = cardResult.getCard();
                String nonce = cardResult.getNonce();

                /*Toast.makeText(context,
                        "Payment Successfully Done",
                        Toast.LENGTH_SHORT)
                        .show();*/

                Log.e("position__", nonce);

                ORDER_PAYMENT(nonce);


            } else if (result.isCanceled()) {
                Toast.makeText(context,
                        R.string.cancelled,
                        Toast.LENGTH_SHORT)
                        .show();
            }
        });
    }

    public void ORDER_PAYMENT(String payment_nonce) {

        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.FIRST_NAME, savePref.getName());
        formBuilder.addFormDataPart(Parameters.EMAIL_ADDRESS, savePref.getEmail());
        formBuilder.addFormDataPart(Parameters.LAST_NAME, savePref.getName());
        formBuilder.addFormDataPart(Parameters.PHONE_NUMNBER, savePref.getPhone());
        formBuilder.addFormDataPart(Parameters.CARD_NONCE, payment_nonce);
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.BARBAR_SUBSCRIPTION, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonMainobject = new JSONObject(result);
                        if (jsonMainobject.getString("code").equalsIgnoreCase("200")) {
                            new IOSDialog.Builder(context)
                                    .setCancelable(false)
                                    .setMessage(getString(R.string.monthly_subscription_activated)).setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    savePref.setStringLatest("subscription_status", "1");
                                    dialog.dismiss();
                                    finish();
                                }
                            }).show();

                        } else {
                            util.showToast(context, jsonMainobject.getString("msg"));
                        }


                    } catch (Exception e) {
                    }
                }

            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();

    }
}