package com.ez_schedule.Activities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ez_schedule.BarbarActivities.BarbarMainActivity;
import com.ez_schedule.BarbarAdapters.DetailservicesAdapter;
import com.ez_schedule.MainActivity;
import com.ez_schedule.ModelClasses.BarbarServiceListModel;
import com.ez_schedule.ModelClasses.TimeslotModel;
import com.ez_schedule.ModelClasses.UserOrdersModel;
import com.ez_schedule.R;
import com.ez_schedule.Util.ConnectivityReceiver;
import com.ez_schedule.Util.Parameters;
import com.ez_schedule.Util.SavePref;
import com.ez_schedule.Util.util;
import com.ez_schedule.parser.AllAPIS;
import com.ez_schedule.parser.GetAsync;
import com.bumptech.glide.Glide;
import com.ligl.android.widget.iosdialog.IOSDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class OrderDetailActivity extends AppCompatActivity {
    OrderDetailActivity context;
    @BindView(R.id.scrollView)
    ScrollView scrollView;
    private SavePref savePref;
    @BindView(R.id.image)
    CircleImageView image;
    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.status)
    TextView status;
    @BindView(R.id.date)
    TextView date;
    @BindView(R.id.time)
    TextView time;
    @BindView(R.id.price)
    TextView price;
    @BindView(R.id.change)
    Button change;
    @BindView(R.id.info)
    TextView info;
    @BindView(R.id.cancel_appointment)
    Button cancel_appointment;
    @BindView(R.id.my_recycler_view)
    RecyclerView myRecyclerView;

    ArrayList<TimeslotModel> timeslot_list;
    boolean is_from_push = false;
    boolean barber_side = false;
    UserOrdersModel userOrdersModel;

    String order_id = "";
    String to_id = "";

    String status_order = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_detail);
        ButterKnife.bind(this);

        context = OrderDetailActivity.this;
        savePref = new SavePref(context);
        is_from_push = getIntent().getBooleanExtra("is_from_push", false);
        barber_side = getIntent().getBooleanExtra("barber_side", false);


        setToolbar();


        change.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (barber_side) {

                    if (status_order.equals("3")) {

                        new IOSDialog.Builder(context)
                                .setTitle(context.getResources().getString(R.string.app_name))
                                .setMessage(getString(R.string.are_you_sure))
                                .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        if (ConnectivityReceiver.isConnected()) {
                                            BARBER_REQURST_STATUS_API(order_id, "4");
                                        } else {
                                            util.IOSDialog(context, util.internet_Connection_Error);
                                        }
                                    }
                                })
                                .setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                }).show();

                    } else if (status_order.equals("4")) {
                        Intent intent = new Intent(context, PostRatingActivity.class);
                        intent.putExtra("to_id", to_id);
                        intent.putExtra("order_id", order_id);
                        startActivity(intent);
                        overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                    }
                } else {
                    if (status_order.equals("3")) {
                        if (timeslot_list.size() > 0) {
                            Intent intent = new Intent(context, SlotChangeRequestActivity.class);
                            intent.putExtra("list", timeslot_list);
                            intent.putExtra("from_order_id", order_id);
                            startActivity(intent);
                            overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                        } else {
                            util.IOSDialog(context, getString(R.string.no_other_bookings));
                        }

                    } else if (status_order.equals("4")) {
                        Intent intent = new Intent(context, PostRatingActivity.class);
                        intent.putExtra("to_id", to_id);
                        intent.putExtra("order_id", order_id);
                        startActivity(intent);
                        overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                    }
                }


            }
        });

        cancel_appointment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new IOSDialog.Builder(context)
                        .setTitle(context.getResources().getString(R.string.app_name))
                        .setMessage(getString(R.string.are_you_sure_to_cancel))
                        .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if (ConnectivityReceiver.isConnected()) {
                                    CANCEL_ORDER_API(order_id);
                                } else {
                                    util.IOSDialog(context, util.internet_Connection_Error);
                                }
                            }
                        })
                        .setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        }).show();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (is_from_push) {
            order_id = getIntent().getStringExtra("order_id");
            ORDER_DETAIL();
        } else {
            userOrdersModel = getIntent().getExtras().getParcelable("data");
           /* Glide.with(context).load(userOrdersModel.getImage()).into(image);

            name.setText(userOrdersModel.getUsername());
            price.setText("Total Amount Paid : $" + userOrdersModel.getCost());
            time.setText("Time Slot: " + userOrdersModel.getSlot_Detail());
            date.setText("Date of Booking: " + util.convertTimeStampDateTime(Long.parseLong(userOrdersModel.getDate())));


            myRecyclerView.setLayoutManager(new LinearLayoutManager(context));
            myRecyclerView.setAdapter(new DetailservicesAdapter(context, userOrdersModel.getList()));

            scrollView.setVisibility(View.VISIBLE);*/

            order_id = userOrdersModel.getId();
            ORDER_DETAIL();

        }
    }

    public void BARBER_REQURST_STATUS_API(String request_id, String status) {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.REQUEST_ID, request_id);
        formBuilder.addFormDataPart(Parameters.STATUS, status);
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.BARBER_REQURST_STATUS, formBody, savePref.getAuthorization_key()) {

            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonObject = new JSONObject(result);
                        String t = "";
                        if (status.equals("4")) {
                            t = getString(R.string.order_completed_successfully);
                        } else if (status.equals("5")) {
                            t = "Order Cancelled Successfully!";
                        }

                        new IOSDialog.Builder(context)
                                .setCancelable(false)
                                .setMessage(t).setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                if (status.equals("4")) {
                                    status_order = "4";
                                    change.setVisibility(View.VISIBLE);
                                    change.setText(R.string.post_review);
                                    cancel_appointment.setVisibility(View.GONE);
                                } else if (status.equals("5")) {
                                    status_order = "5";
                                    change.setVisibility(View.VISIBLE);
                                    change.setText(R.string.canceled);
                                    cancel_appointment.setVisibility(View.GONE);
                                }
                            }
                        }).show();


                    } catch (Exception e) {
                    }
                }

            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    public void CANCEL_ORDER_API(String request_id) {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.REQUEST_ID, request_id);
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.CANCEL_ORDER, formBody, savePref.getAuthorization_key()) {

            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonObject = new JSONObject(result);
                        String t = getString(R.string.order_cancelled_successfully);
                        new IOSDialog.Builder(context)
                                .setCancelable(false)
                                .setMessage(t).setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                status_order = "5";
                                change.setVisibility(View.VISIBLE);
                                change.setText(R.string.canceled);
                                cancel_appointment.setVisibility(View.GONE);
                            }
                        }).show();


                    } catch (Exception e) {
                    }
                }

            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    private void ORDER_DETAIL() {
        ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.ORDER_ID, order_id);
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.ORDER_DETAIL, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                timeslot_list = new ArrayList<>();
                if (result != null && !result.equalsIgnoreCase("")) {

                    Log.e("detail_data", result + " order_id: " + order_id);
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {
                            JSONObject object = jsonmainObject.getJSONObject("body").getJSONObject("order_detail");
                            userOrdersModel = new UserOrdersModel();
                            userOrdersModel.setStatus(object.getJSONObject("order").getString("status"));
                            userOrdersModel.setDate(object.getJSONObject("order").getString("date"));
                            userOrdersModel.setCost(object.getJSONObject("order").getString("total_amount"));
                            userOrdersModel.setId(object.getJSONObject("order").getString("id"));
                            if (!barber_side) {
                                to_id = object.getJSONObject("order").getJSONObject("barber").getString("id");
                                userOrdersModel.setImage(object.getJSONObject("order").getJSONObject("barber").getString("profile_image"));
                                userOrdersModel.setUsername(object.getJSONObject("order").getJSONObject("barber").getString("username"));
                            } else {
                                to_id = object.getJSONObject("order").getJSONObject("user").getString("id");
                                userOrdersModel.setImage(object.getJSONObject("order").getJSONObject("user").getString("profile_image"));
                                userOrdersModel.setUsername(object.getJSONObject("order").getJSONObject("user").getString("username"));
                            }

                            userOrdersModel.setSlot_Detail(object.getJSONObject("slot_Detail").getString("start_time") + "-" + object.getJSONObject("slot_Detail").getString("end_time"));


                            if (object.getJSONObject("order").getString("info").isEmpty())
                                info.setVisibility(View.GONE);
                            else {
                                info.setVisibility(View.VISIBLE);
                                info.setText(getString(R.string.additional_info) + object.getJSONObject("order").getString("info"));
                            }


                            Glide.with(context).load(userOrdersModel.getImage()).into(image);
                            name.setText(userOrdersModel.getUsername());
                            price.setText(getString(R.string.total_amount_paid) + userOrdersModel.getCost());
                            time.setText(getString(R.string.time_slot) + userOrdersModel.getSlot_Detail());
                            date.setText(getString(R.string.date_of_booking) + util.convertTimeStampDate(Long.parseLong(userOrdersModel.getDate())));


                            ArrayList<BarbarServiceListModel> list = new ArrayList<>();
                            for (int j = 0; j < object.getJSONArray("services_detail").length(); j++) {
                                JSONObject obj = object.getJSONArray("services_detail").getJSONObject(j);
                                BarbarServiceListModel barbarServiceListModel = new BarbarServiceListModel();
                                barbarServiceListModel.setId(obj.getString("id"));
                                barbarServiceListModel.setName(obj.getString("name"));
                                barbarServiceListModel.setPrice(obj.getString("price"));
                                barbarServiceListModel.setDuration(obj.optString("duration"));
                                list.add(barbarServiceListModel);
                            }
                            userOrdersModel.setList(list);

                            status_order = userOrdersModel.getStatus();
                            //status_order ="4";

                           /* status
                            0 => Requested to barber
                            1 => Request accepted by barber
                            2 => Request declined by barber
                            3 => Payment done by user and order placed
                            4 => Mark as complete by barber*/

                            if (barber_side) {

                                if (status_order.equals("3")) {
                                    change.setVisibility(View.VISIBLE);
                                    change.setText(R.string.complete_order);
                                    cancel_appointment.setVisibility(View.VISIBLE);
                                } else if (status_order.equals("4")) {
                                    cancel_appointment.setVisibility(View.GONE);
                                    change.setVisibility(View.VISIBLE);
                                    change.setText(R.string.post_review);

                                    if (object.getString("is_rated").equals("1")) {
                                        change.setVisibility(View.INVISIBLE);
                                    } else {
                                        change.setVisibility(View.VISIBLE);
                                    }

                                } else if (status_order.equals("5")) {
                                    change.setText(R.string.cancelled);
                                }

                            } else {
                                cancel_appointment.setVisibility(View.GONE);
                                if (status_order.equals("3")) {
                                    change.setVisibility(View.VISIBLE);
                                    change.setText(R.string.change_time_slot);
                                } else if (status_order.equals("4")) {
                                    change.setVisibility(View.VISIBLE);
                                    change.setText(R.string.post_review);

                                    if (object.getString("is_rated").equals("1")) {
                                        change.setVisibility(View.INVISIBLE);
                                    } else {
                                        change.setVisibility(View.VISIBLE);
                                    }
                                } else if (status_order.equals("5")) {
                                    change.setText(R.string.cancelled);
                                }
                            }

                            //deleted
                            JSONArray booked_slots = jsonmainObject.getJSONObject("body").getJSONArray("booked_slots");
                            for (int i = 0; i < booked_slots.length(); i++) {
                                JSONObject obj = booked_slots.getJSONObject(i);
                                TimeslotModel timeslotModel = new TimeslotModel();
                                timeslotModel.setId(obj.getJSONObject("slot_Detail").getString("slot_id"));
                                timeslotModel.setSlot(obj.getJSONObject("slot_Detail").getString("start_time") + "-" + obj.getJSONObject("slot_Detail").getString("end_time"));
                                timeslotModel.setOrder_id(obj.getJSONObject("order").getString("id"));
                                timeslotModel.setDate(obj.getJSONObject("order").getString("date"));
                                timeslotModel.setUser_id(obj.getJSONObject("order").getString("user_id"));
                                timeslotModel.setSwap_request_sent(obj.getJSONObject("order").getJSONObject("user").getString("swap_request_sent"));
                                timeslot_list.add(timeslotModel);
                            }


                            myRecyclerView.setLayoutManager(new LinearLayoutManager(context));
                            myRecyclerView.setAdapter(new DetailservicesAdapter(context, userOrdersModel.getList()));

                            scrollView.setVisibility(View.VISIBLE);
                        } else {
                            if (jsonmainObject.getString("msg").equals(util.Invalid_Authorization)) {
                                 savePref.setAuthorization_key("");
                                Intent intent = new Intent(context, SignInActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                            } else {
                                util.showToast(context, jsonmainObject.getString("msg"));
                            }
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView title = (TextView) toolbar.findViewById(R.id.title);
        title.setText(R.string.order_detail);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back_w);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    @Override
    public void onBackPressed() {
        if (is_from_push) {
            if (!barber_side) {
                Intent intent = new Intent(context, MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
            } else {
                Intent intent = new Intent(context, BarbarMainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
            }
        } else {
            super.onBackPressed();
        }

    }
}
