package com.ez_schedule.parser;


public class AllAPIS {


    /*   <!-- nitinapps2020@gmail.com (barber)
      asdfghjkL1@

              user :
      jyoti@gmail.com   asdfghjkL1@
             ->
      */
   // public static final String BASE_URL = "http://52.12.76.190:8081/api/";
    public static final String BASE_URL = "http://admin.barbrzclub.com/api/";

    public static final String USERLOGIN = BASE_URL + "signin";
    public static final String USER_SIGNUP = BASE_URL + "signUp";
    public static final String VERIFY_OTP = BASE_URL + "verify_otp";
    public static final String FORGOT_PASSWORD = BASE_URL + "forgot_password";
    public static final String LOGOUT = BASE_URL + "logout";
    public static final String CHANGEPASSWORD = BASE_URL + "ChangePassword";
    public static final String EDIT_PROFILE = BASE_URL + "editprofile";
    public static final String UPDATE_LAT_LONG = BASE_URL + "update_lat_long";
    public static final String APP_INFO = BASE_URL + "app-information";

    public static final String GETCATEGORYLIST = BASE_URL + "getcategorylist";
    public static final String ADD_SERVICES = BASE_URL + "add_services";
    public static final String UPDATE_SERVICES = BASE_URL + "update_services";
    public static final String BARBAR_SERVICES = BASE_URL + "barber_services";
    public static final String DELETE_SERVICES = BASE_URL + "delete_services";
    public static final String FAV_UNFAV = BASE_URL + "fav_unfav";
    public static final String HOME = BASE_URL + "home";
    public static final String BARBER_LIST = BASE_URL + "barber_list";
    public static final String FAV_LIST = BASE_URL + "fav_list";
    public static final String BARBAR_PROFILE = BASE_URL + "barber_profile";

    public static final String BOOK_BARBER = BASE_URL + "book_barber";
    public static final String REQURST_SENT_TO_BARBER = BASE_URL + "request_sent_to_barber";
    public static final String BARBER_REQUESTS = BASE_URL + "barber_requests";
    public static final String BARBER_REQURST_STATUS = BASE_URL + "barber_request_status";
    public static final String USER_ORDERS = BASE_URL + "user_orders";
    public static final String BARBER_ORDERS = BASE_URL + "barber_orders";
    public static final String ORDER_PAYMENT = BASE_URL + "order_payment";
    public static final String TIME_SLOT = BASE_URL + "time_slots";
    public static final String ADD_POST = BASE_URL + "add_feed";
    public static final String GET_POST = BASE_URL + "Feeds";
    public static final String ORDER_DETAIL = BASE_URL + "order_detail";
    public static final String SEND_PUSH_USER = BASE_URL + "send_push_user";
    public static final String SWAP_REQUESTS = BASE_URL + "swap_requests";
    public static final String USER_SWAP_REQUEST = BASE_URL + "user_swap_request";
    public static final String SWAP_REQUEST_STATUS = BASE_URL + "swap_request_status";
    public static final String POST_RATING = BASE_URL + "rating_and_strike";
    public static final String GET_USER_RATINGS = BASE_URL + "get_rating_and_strike";
    public static final String REWARD_SETTING = BASE_URL + "reward_setting";
    public static final String GETCONTENT = BASE_URL + "getcontent";
    public static final String GET_DEFAULT_CHARITY = BASE_URL + "get_default_charity";
    public static final String CHARITY_PAYMENT = BASE_URL + "charity_payment";
    public static final String DELETE_PROFILE = BASE_URL + "delete_profile";
    public static final String CALENDER_ORDERS = BASE_URL + "calender_orders";
    public static final String CANCEL_ORDER = BASE_URL + "cancel_order";

    public static final String ALL_TIME_SLOTS = BASE_URL + "all_time_slots";
    public static final String ADD_CATEGORY = BASE_URL + "add_category";

    public static final String OFFERS = BASE_URL + "offers";
    public static final String ADS = BASE_URL + "ads";
    public static final String TRACK_SALE = BASE_URL + "track_sale";
    public static final String BARBAR_SUBSCRIPTION = BASE_URL + "barber_subscription";
    public static final String CANCEL_SUBSCRIPTION = BASE_URL + "cancel_subscription";
    public static final String SUBSCRIPTION_SETTING = BASE_URL + "subscription_setting";
    public static final String LANGUAGE_CHANGE = BASE_URL + "language";

}
