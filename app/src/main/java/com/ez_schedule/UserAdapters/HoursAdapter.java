package com.ez_schedule.UserAdapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;


import com.ez_schedule.ModelClasses.HoursModel;
import com.ez_schedule.R;
import com.ligl.android.widget.iosdialog.IOSSheetDialog;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class HoursAdapter extends RecyclerView.Adapter<HoursAdapter.RecyclerViewHolder> {
    Context context;
    LayoutInflater Inflater;
    private View view;
    ArrayList<HoursModel> hourslist;
    ArrayList<String> time_list;

    public HoursAdapter(Context context, ArrayList<HoursModel> hourslist, ArrayList<String> time_list) {
        this.context = context;
        this.hourslist = hourslist;
        this.time_list = time_list;
        Inflater = LayoutInflater.from(context);

    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = Inflater.inflate(R.layout.hours_row, parent, false);
        RecyclerViewHolder viewHolder = new RecyclerViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, final int position) {

        holder.dayTime.setText(hourslist.get(position).getTitle());
        holder.startTime.setText(hourslist.get(position).getStart_time());
        holder.endTime.setText(hourslist.get(position).getEnd_time());

        if (!hourslist.get(position).getIs_close().equals("1")) {
            holder.open_close.setText(context.getString(R.string.open));
            holder.open_close.setBackground(context.getResources().getDrawable(R.drawable.drawable_button));
            holder.startTime.setTextColor(context.getResources().getColor(R.color.black));
            holder.endTime.setTextColor(context.getResources().getColor(R.color.black));
            holder.startTime.setEnabled(true);
            holder.endTime.setEnabled(true);

        } else {
            holder.open_close.setText(context.getString(R.string.close));
            holder.open_close.setBackground(context.getResources().getDrawable(R.drawable.drawable_button_b));
            holder.startTime.setTextColor(context.getResources().getColor(R.color.gray));
            holder.endTime.setTextColor(context.getResources().getColor(R.color.gray));
            holder.startTime.setEnabled(false);
            holder.endTime.setEnabled(false);
        }





        /*holder.startTime.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                hourslist.get(position).setStart_time(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        holder.endTime.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                hourslist.get(position).setEnd_time(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });*/

        holder.startTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Time_picker(true, position);
            }
        });
        holder.endTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Time_picker(false, position);
            }
        });

        holder.open_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Creating the instance of PopupMenu
                PopupMenu popup = new PopupMenu(context, holder.open_close);
                //Inflating the Popup using xml file
                popup.getMenuInflater().inflate(R.menu.how_long_menu, popup.getMenu());

                //registering popup with OnMenuItemClickListener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {
                        holder.open_close.setText(item.getTitle());
                        if (item.getTitle().equals(context.getString(R.string.open)))
                            hourslist.get(position).setIs_close("0");
                        else if (item.getTitle().equals(context.getString(R.string.close)))
                            hourslist.get(position).setIs_close("1");
                        notifyDataSetChanged();
                        return true;
                    }
                });
                popup.show();

                //OC_Picker(position);
            }
        });

    }

    @Override
    public int getItemCount() {
        return hourslist.size();
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.day_time)
        TextView dayTime;
        @BindView(R.id.to_text)
        TextView toText;
        @BindView(R.id.start_time)
        TextView startTime;
        @BindView(R.id.end_time)
        TextView endTime;
        @BindView(R.id.open_close)
        TextView open_close;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);


        }
    }

    private void Time_picker(boolean startTime, int position) {
        final CharSequence[] Animals = time_list.toArray(new String[time_list.size()]);
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        dialogBuilder.setTitle("Select Time");
        dialogBuilder.setItems(Animals, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                if (startTime) {
                    hourslist.get(position).setStart_time(time_list.get(item));
                } else {
                    hourslist.get(position).setEnd_time(time_list.get(item));
                }
                notifyDataSetChanged();
            }
        });
        //Create alert dialog object via builder
        AlertDialog alertDialogObject = dialogBuilder.create();
        //Show the dialog
        alertDialogObject.show();
    }

    private void OC_Picker(int position) {
        IOSSheetDialog.SheetItem[] items = new IOSSheetDialog.SheetItem[2];
        items[0] = new IOSSheetDialog.SheetItem(context.getString(R.string.open), IOSSheetDialog.SheetItem.BLUE);
        items[1] = new IOSSheetDialog.SheetItem(context.getString(R.string.close), IOSSheetDialog.SheetItem.BLUE);
        IOSSheetDialog dialog2 = new IOSSheetDialog.Builder(context)
                .setTitle("Choose").setData(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == 0) {
                            hourslist.get(position).setIs_close("0");
                        } else {
                            hourslist.get(position).setIs_close("1");
                        }
                        notifyDataSetChanged();
                    }
                }).show();


    }
}
