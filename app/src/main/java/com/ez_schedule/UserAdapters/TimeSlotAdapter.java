package com.ez_schedule.UserAdapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.ez_schedule.BarbarActivities.CheckOutActivity;
import com.ez_schedule.ModelClasses.TimeslotModel;
import com.ez_schedule.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class TimeSlotAdapter extends RecyclerView.Adapter<TimeSlotAdapter.RecyclerViewHolder> {
    CheckOutActivity context;
    LayoutInflater Inflater;

    private View view;
    ArrayList<TimeslotModel> timeslot_list;

    public TimeSlotAdapter(CheckOutActivity context, ArrayList<TimeslotModel> timeslot_list) {
        this.context = context;
        this.timeslot_list = timeslot_list;
        Inflater = LayoutInflater.from(context);

    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = Inflater.inflate(R.layout.time_slot_row, parent, false);
        RecyclerViewHolder viewHolder = new RecyclerViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, final int position) {

        if (timeslot_list.get(position).getAvailability().equals("0")) {
            holder.name.setBackground(context.getResources().getDrawable(R.drawable.drawable_button_gray));
        } else {
            if (timeslot_list.get(position).isIs_checked()) {
                holder.name.setBackground(context.getResources().getDrawable(R.drawable.drawable_button));
            } else {
                holder.name.setBackground(context.getResources().getDrawable(R.drawable.drawable_border));
            }
        }


        holder.name.setText(timeslot_list.get(position).getSlot());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (timeslot_list.get(position).getAvailability().equals("1")) {
                    for (int i = 0; i < timeslot_list.size(); i++) {
                        timeslot_list.get(i).setIs_checked(false);
                    }
                    context.slot_id = timeslot_list.get(position).getId();
                    timeslot_list.get(position).setIs_checked(true);
                    notifyDataSetChanged();
                } else {
                    context.slot_id = "";
                    for (int i = 0; i < timeslot_list.size(); i++) {
                        timeslot_list.get(i).setIs_checked(false);
                    }
                    notifyDataSetChanged();
                }


            }


        });
        /*holder.name.setText(list.get(position).getName());
        Glide.with(context).load(list.get(position).getImage()).error(R.drawable.logo).into(holder.image);

        if (list.get(position).getIsJoined().equals("1"))
            holder.join.setText("Chat");
        else
            holder.join.setText("Join");

        holder.join.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (list.get(position).getIsJoined().equals("1")) {
                    Intent intent = new Intent(context, ChattngActivity.class);
                    intent.putExtra("group_id", list.get(position).getId());
                    intent.putExtra("group_name", list.get(position).getName());
                    context.startActivity(intent);
                } else {
                    Intent intent = new Intent(context, GroupDetailActivity.class);
                    intent.putExtra("group_id", list.get(position).getId());
                    context.startActivity(intent);
                    MainActivity.context.overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                }

            }
        });
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, GroupDetailActivity.class);
                intent.putExtra("group_id", list.get(position).getId());
                context.startActivity(intent);
                MainActivity.context.overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
            }
        });
*/
    }

    @Override
    public int getItemCount() {
        return timeslot_list.size();
    }


    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.name)
        TextView name;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}
