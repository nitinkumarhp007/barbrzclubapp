package com.ez_schedule.UserAdapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.ez_schedule.Activities.BarbarDetailActivity;
import com.ez_schedule.Activities.BarbarsListingActivity;
import com.ez_schedule.MainActivity;
import com.ez_schedule.ModelClasses.BarbarModel;
import com.ez_schedule.ModelClasses.BarbarServiceListModel;
import com.ez_schedule.R;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;


public class BarbarsListAdapter extends RecyclerView.Adapter<BarbarsListAdapter.RecyclerViewHolder> {
    BarbarsListingActivity context;
    LayoutInflater Inflater;
    private View view;
    ArrayList<BarbarModel> tempList;
    ArrayList<BarbarModel> list;

    public BarbarsListAdapter(BarbarsListingActivity context, ArrayList<BarbarModel> list) {
        this.context = context;
        this.list = list;
        this.tempList = list;
        Inflater = LayoutInflater.from(context);

    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = Inflater.inflate(R.layout.barbar_list_row, parent, false);
        RecyclerViewHolder viewHolder = new RecyclerViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, final int position) {
        holder.name.setText(list.get(position).getUsername());
        holder.location.setText(list.get(position).getAddress());

        if (list.get(position).getIs_fav().equals("1")) {
            holder.heart.setImageDrawable(context.getResources().getDrawable(R.drawable.heart_r));
        } else {
            holder.heart.setImageDrawable(context.getResources().getDrawable(R.drawable.heart));
        }


        holder.price.setText("$" + list.get(position).getPhone());
        Glide.with(context)
                .load(list.get(position).getProfile_image())
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        holder.progressBar.setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        holder.progressBar.setVisibility(View.GONE);
                        return false;
                    }
                })
                .into(holder.image);
        if (!list.get(position).getAvg_rating().isEmpty())
            holder.ratingBar.setRating(Float.parseFloat(list.get(position).getAvg_rating()));


        if (list.get(position).getAvg_rating().isEmpty()) {
            holder.numberRate.setVisibility(View.INVISIBLE);
        } else {
            holder.numberRate.setText("(" + list.get(position).getAvg_rating() + ")");
            holder.numberRate.setVisibility(View.VISIBLE);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, BarbarDetailActivity.class);
                intent.putExtra("id", list.get(position).getId());
                context.startActivity(intent);
                MainActivity.context.overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
            }
        });


        holder.heart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.FAV_UNFAV_API(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.image)
        CircleImageView image;
        @BindView(R.id.progress_bar)
        ProgressBar progressBar;
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.location)
        TextView location;
        @BindView(R.id.price)
        TextView price;
        @BindView(R.id.rating_bar)
        RatingBar ratingBar;
        @BindView(R.id.number_rate)
        TextView numberRate;
        @BindView(R.id.heart)
        ImageView heart;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }

    public void filter(String charText) {
        charText = charText.toLowerCase();
        ArrayList<BarbarModel> nList = new ArrayList<BarbarModel>();
        if (charText.length() == 0) {
            nList.addAll(tempList);
        } else {
            for (BarbarModel wp : tempList) {
                if (wp.getUsername().toLowerCase().contains(charText.toLowerCase()))//contains for less accurate result nd matches for accurate result
                {
                    nList.add(wp);
                }
            }
        }
        list = nList;
        notifyDataSetChanged();
    }
}
