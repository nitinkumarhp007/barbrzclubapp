package com.ez_schedule.UserAdapters;

import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ez_schedule.BarbarAdapters.BarbarrequestservicesAdapter;
import com.ez_schedule.BarbarFragments.HomeBarbarFragment;
import com.ez_schedule.ModelClasses.RequestModel;
import com.ez_schedule.R;
import com.ez_schedule.UserFragments.OrdersFragment;
import com.ez_schedule.Util.util;
import com.bumptech.glide.Glide;
import com.ligl.android.widget.iosdialog.IOSSheetDialog;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;


public class UserSideRequetsAdapter extends RecyclerView.Adapter<UserSideRequetsAdapter.RecyclerViewHolder> {
    Context context;
    LayoutInflater Inflater;
    private View view;
    ArrayList<RequestModel> list;
    OrdersFragment ordersFragment;

    public UserSideRequetsAdapter(Context context, ArrayList<RequestModel> list, OrdersFragment ordersFragment) {
        this.context = context;
        this.list = list;
        this.ordersFragment = ordersFragment;
        Inflater = LayoutInflater.from(context);

    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = Inflater.inflate(R.layout.requests_user_side_row, parent, false);
        RecyclerViewHolder viewHolder = new RecyclerViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, final int position) {
        holder.name.setText(list.get(position).getUsername());
        holder.price.setText("Total : $" + list.get(position).getCost());
        Glide.with(context).load(list.get(position).getImage()).error(R.drawable.logo_new).into(holder.image);
        holder.date.setText("Date: " + util.convertTimeStampDate(Long.parseLong(list.get(position).getDate())) + " | " + list.get(position).getSlot_Detail());

        if (list.get(position).getInfo().isEmpty())
            holder.special.setVisibility(View.GONE);
        else {
            holder.special.setVisibility(View.VISIBLE);
            holder.special.setText(context.getString(R.string.additional_info) + list.get(position).getInfo());
        }

        holder.myRecyclerView.setLayoutManager(new LinearLayoutManager(context));
        holder.myRecyclerView.setAdapter(new BarbarrequestservicesAdapter(context, list.get(position).getList()));

        if (list.get(position).getStatus().equals("0")) {
            holder.time.setText(R.string.awaited);
        } else if (list.get(position).getStatus().equals("1")) {
            holder.time.setText(R.string.pay_now);
        } else if (list.get(position).getStatus().equals("2")) {
            holder.time.setText(R.string.declined);
        }

        holder.time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (list.get(position).getStatus().equals("1")) {
                   /* IOSSheetDialog.SheetItem[] items = new IOSSheetDialog.SheetItem[2];
                    items[0] = new IOSSheetDialog.SheetItem("Pay in person", IOSSheetDialog.SheetItem.BLUE);
                    items[1] = new IOSSheetDialog.SheetItem("Pay Online", IOSSheetDialog.SheetItem.BLUE);
                    IOSSheetDialog dialog2 = new IOSSheetDialog.Builder(context)
                            .setTitle(context.getString(R.string.choose_method)).setData(items, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    if (which == 0) {
                                        ordersFragment.ORDER_PAYMENT(position, "abc","2");
                                    } else {
                                        ordersFragment.ProceedPayment(position);
                                    }
                                }
                            }).show();*/


                    //Creating the instance of PopupMenu
                    PopupMenu popup = new PopupMenu(context, holder.time);
                    //Inflating the Popup using xml file
                    popup.getMenuInflater().inflate(R.menu.menu_option, popup.getMenu());

                    //registering popup with OnMenuItemClickListener
                    popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        public boolean onMenuItemClick(MenuItem item) {
                            popup.dismiss();
                            if (item.getTitle().equals(context.getString(R.string.pay_in_person))) {
                                ordersFragment.ORDER_PAYMENT(position, "abc", "2");
                            } else if (item.getTitle().equals(context.getString(R.string.pay_online))) {
                                ordersFragment.ProceedPayment(position);
                            }

                            return true;
                        }
                    });
                    popup.show();


                }

            }
        });


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*Intent intent = new Intent(context, GroupDetailActivity.class);
                intent.putExtra("group_id", list.get(position).getId());
                context.startActivity(intent);
                MainActivity.context.overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);*/
            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.image)
        CircleImageView image;
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.my_recycler_view)
        RecyclerView myRecyclerView;
        @BindView(R.id.price)
        TextView price;
        @BindView(R.id.time)
        TextView time;
        @BindView(R.id.date)
        TextView date;
        @BindView(R.id.special)
        TextView special;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}
