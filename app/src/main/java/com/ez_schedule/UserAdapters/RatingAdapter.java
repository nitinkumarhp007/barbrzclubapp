package com.ez_schedule.UserAdapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.ez_schedule.ModelClasses.ReviewModel;
import com.ez_schedule.R;
import com.bumptech.glide.Glide;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;


public class RatingAdapter extends RecyclerView.Adapter<RatingAdapter.RecyclerViewHolder> {
    Context context;
    LayoutInflater Inflater;
    private View view;
    ArrayList<ReviewModel> list;

    public RatingAdapter(Context context, ArrayList<ReviewModel> list) {
        this.context = context;
        this.list = list;
        Inflater = LayoutInflater.from(context);

    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = Inflater.inflate(R.layout.rating_item, parent, false);
        RecyclerViewHolder viewHolder = new RecyclerViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, final int position) {
        Glide.with(context).load(list.get(position).getImage()).into(holder.image);
        holder.name.setText(list.get(position).getName());
        holder.text.setText(list.get(position).getText());
        if (!list.get(position).getRating().isEmpty())
            holder.ratingBar.setRating(Float.parseFloat(list.get(position).getRating()));

       /* if (list.get(position).getType().equals("2")) {
            holder.type.setText("Sold: " + list.get(position).getName_product());
        } else {
            holder.type.setText("Booked: " + list.get(position).getName_product());
        }*/


    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.image)
        CircleImageView image;
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.rating_bar)
        RatingBar ratingBar;
        @BindView(R.id.type)
        TextView type;
        @BindView(R.id.date)
        TextView date;
        @BindView(R.id.text)
        TextView text;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}
