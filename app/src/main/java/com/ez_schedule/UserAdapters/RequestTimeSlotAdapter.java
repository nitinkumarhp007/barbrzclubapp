package com.ez_schedule.UserAdapters;

import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.ez_schedule.Activities.SlotChangeRequestActivity;
import com.ez_schedule.BarbarActivities.CheckOutActivity;
import com.ez_schedule.ModelClasses.TimeslotModel;
import com.ez_schedule.R;
import com.ez_schedule.Util.util;
import com.ligl.android.widget.iosdialog.IOSDialog;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class RequestTimeSlotAdapter extends RecyclerView.Adapter<RequestTimeSlotAdapter.RecyclerViewHolder> {
    SlotChangeRequestActivity context;
    LayoutInflater Inflater;
    private View view;
    ArrayList<TimeslotModel> timeslot_list;

    public RequestTimeSlotAdapter(SlotChangeRequestActivity context, ArrayList<TimeslotModel> timeslot_list) {
        this.context = context;
        this.timeslot_list = timeslot_list;
        Inflater = LayoutInflater.from(context);

    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = Inflater.inflate(R.layout.time_slot_request_row, parent, false);
        RecyclerViewHolder viewHolder = new RecyclerViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, final int position) {


        holder.name.setText(timeslot_list.get(position).getSlot());
        holder.date.setText(util.convertTimeStampDate(Long.parseLong(timeslot_list.get(position).getDate())));


        holder.request.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (timeslot_list.get(position).getSwap_request_sent().equals("0")) {
                    new IOSDialog.Builder(context)
                            .setTitle(context.getResources().getString(R.string.app_name))
                            .setMessage("Are you sure?")
                            .setPositiveButton(context.getString(R.string.yes), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    context.SWAP_REQUESTS_API(position);
                                }
                            })
                            .setNegativeButton(context.getString(R.string.no), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            }).show();
                }
            }
        });

        if (timeslot_list.get(position).getSwap_request_sent().equals("0")) {
            holder.request.setText(R.string.request);
        } else {
            holder.request.setText(R.string.request_sent_s);
        }


    }

    @Override
    public int getItemCount() {
        return timeslot_list.size();
    }


    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.date)
        TextView date;
        @BindView(R.id.request)
        TextView request;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}
