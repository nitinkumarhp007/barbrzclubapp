package com.ez_schedule.UserAdapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.ez_schedule.ModelClasses.OffersModel;
import com.ez_schedule.R;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class ViewPagerAdapter extends PagerAdapter {
    int[] img;
    LayoutInflater inflater;
    Context context;
    ArrayList<OffersModel> list;

    public ViewPagerAdapter(Context context, ArrayList<OffersModel> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((RelativeLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View itemview = inflater.inflate(R.layout.viewpager_item, container, false);

        ImageView imageView = (ImageView) itemview.findViewById(R.id.image);
        TextView offer = (TextView) itemview.findViewById(R.id.offer);
        TextView check_now = (TextView) itemview.findViewById(R.id.check_now);

        offer.setText(list.get(position).getBusinesses_name());
        Glide.with(context)
                .load(list.get(position).getImage()).into(imageView);

        if (!list.get(position).getOffer().isEmpty())
            check_now.setVisibility(View.VISIBLE);
        else
            check_now.setVisibility(View.INVISIBLE);

        check_now.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = list.get(position).getOffer();
                if (!url.startsWith("http://") && !url.startsWith("https://"))
                    url = "http://" + url;
                context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
            }
        });


        //add item.xml to viewpager
        ((ViewPager) container).addView(itemview);
        return itemview;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        // Remove viewpager_item.xml from ViewPager
        ((ViewPager) container).removeView((RelativeLayout) object);
    }
}