package com.ez_schedule.UserAdapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.ez_schedule.Activities.BlockTimeSlotActivity;
import com.ez_schedule.BarbarActivities.CheckOutActivity;
import com.ez_schedule.ModelClasses.TimeslotModel;
import com.ez_schedule.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class ChangeTimeSlotAdapter extends RecyclerView.Adapter<ChangeTimeSlotAdapter.RecyclerViewHolder> {
    BlockTimeSlotActivity context;
    LayoutInflater Inflater;
    private View view;
    ArrayList<TimeslotModel> list;

    public ChangeTimeSlotAdapter(BlockTimeSlotActivity context, ArrayList<TimeslotModel> list) {
        this.context = context;
        this.list = list;
        Inflater = LayoutInflater.from(context);

    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = Inflater.inflate(R.layout.time_slot_row_new, parent, false);
        RecyclerViewHolder viewHolder = new RecyclerViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, final int position) {
        if (list.get(position).isIs_checked()) {
            holder.name.setBackground(context.getResources().getDrawable(R.drawable.drawable_button));
            holder.name.setTextColor(context.getResources().getColor(R.color.white));
        } else {
            holder.name.setBackground(context.getResources().getDrawable(R.drawable.drawable_border));
            holder.name.setTextColor(context.getResources().getColor(R.color.black));
        }


        holder.name.setText(list.get(position).getSlot());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                context.block_whole_day_text = "0";
                context.block_whole_day.setBackground(context.getResources().getDrawable(R.drawable.drawable_border));
                context.block_whole_day.setTextColor(context.getResources().getColor(R.color.black));

                if (list.get(position).isIs_checked())
                    list.get(position).setIs_checked(false);
                else
                    list.get(position).setIs_checked(true);

                notifyDataSetChanged();

            }


        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.name)
        TextView name;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}
