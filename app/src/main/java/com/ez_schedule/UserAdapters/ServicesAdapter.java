package com.ez_schedule.UserAdapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.ez_schedule.BarbarActivities.CheckOutActivity;
import com.ez_schedule.ModelClasses.BarbarServiceListModel;
import com.ez_schedule.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class ServicesAdapter extends RecyclerView.Adapter<ServicesAdapter.RecyclerViewHolder> {
    CheckOutActivity context;
    LayoutInflater Inflater;
    private View view;
    ArrayList<BarbarServiceListModel> list;

    public ServicesAdapter(CheckOutActivity context, ArrayList<BarbarServiceListModel> list) {
        this.context = context;
        this.list = list;
        Inflater = LayoutInflater.from(context);

    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = Inflater.inflate(R.layout.services_row, parent, false);
        RecyclerViewHolder viewHolder = new RecyclerViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, final int position) {

        String duration_text = list.get(position).getDuration();

        if (duration_text.equals("1"))
            holder.name.setText(list.get(position).getName() + " (15 Mins)");
        else if (duration_text.equals("2"))
            holder.name.setText(list.get(position).getName() + " (30 Mins)");
        else if (duration_text.equals("3"))
            holder.name.setText(list.get(position).getName() + " (45 Mins)");
        else if (duration_text.equals("4"))
            holder.name.setText(list.get(position).getName() + " (1 Hour)");
        else if (duration_text.equals("5"))
            holder.name.setText(list.get(position).getName() + " (1 Hour 15 Mins)");
        else if (duration_text.equals("6"))
            holder.name.setText(list.get(position).getName() + " (1 Hour 30 Mins)");
        else if (duration_text.equals("7"))
            holder.name.setText(list.get(position).getName() + " (1 Hour 45 Mins)");
        else if (duration_text.equals("8"))
            holder.name.setText(list.get(position).getName() + " (2 Hours)");


        holder.price.setText("$" + list.get(position).getPrice());

        holder.checkboxR.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                    list.get(position).setIs_check(true);
                else
                    list.get(position).setIs_check(false);

                int total = 0;
                for (int i = 0; i < list.size(); i++) {
                    if (list.get(i).isIs_check()) {
                        total = total + Integer.parseInt(list.get(i).getPrice());
                    }
                }


                if (total > 0 && context.is_rewarded.equals("2")) {
                    context.discountAppliedText.setVisibility(View.VISIBLE);

                    int reward_percentage_i = Integer.parseInt(context.reward_percentage);
                    int new_total = total - ((total * reward_percentage_i) / 100);

                    context.total_amount = String.valueOf(new_total);
                    context.price.setText(" ($" + String.valueOf(total) + "-$" + String.valueOf(((total * reward_percentage_i) / 100)) + ") = " + "$" + new_total);


                } else {
                    context.discountAppliedText.setVisibility(View.GONE);

                    context.total_amount = String.valueOf(total);
                    context.price.setText("$" + total);

                }

            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.checkbox_r)
        CheckBox checkboxR;
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.date)
        TextView date;
        @BindView(R.id.price)
        TextView price;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}
