package com.ez_schedule;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.viewpager.widget.ViewPager;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.ez_schedule.Activities.BarbarDetailActivity;
import com.ez_schedule.Activities.SignInActivity;
import com.ez_schedule.BarbarActivities.BarbarMainActivity;
import com.ez_schedule.BarbarFragments.OffersFragment;
import com.ez_schedule.ModelClasses.OffersModel;
import com.ez_schedule.UserAdapters.OffersAdapter;
import com.ez_schedule.UserAdapters.ViewPagerAdapter;
import com.ez_schedule.UserFragments.FavoriteFragment;
import com.ez_schedule.UserFragments.FeedFragment;
import com.ez_schedule.UserFragments.HomeFragment;
import com.ez_schedule.UserFragments.OrdersFragment;
import com.ez_schedule.UserFragments.ProfileFragment;
import com.ez_schedule.Util.ConnectivityReceiver;
import com.ez_schedule.Util.GPSTracker;
import com.ez_schedule.Util.Parameters;
import com.ez_schedule.Util.SavePref;
import com.ez_schedule.Util.util;
import com.ez_schedule.parser.AllAPIS;
import com.ez_schedule.parser.GetAsync;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.angeldevil.autoscrollviewpager.AutoScrollViewPager;
import me.relex.circleindicator.CircleIndicator;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.progress_bar)
    ProgressBar progressBar;
    @BindView(R.id.pager)
    AutoScrollViewPager pager;
    @BindView(R.id.indicator)
    CircleIndicator indicator;
    private SavePref savePref;
    public static Toolbar toolbar;
    public static MainActivity context;

    private ArrayList<OffersModel> list;
    String latitude = "", longitude = "";
    public String zip = "";
    GPSTracker gpsTracker = null;

    boolean to_request = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        context = MainActivity.this;
        savePref = new SavePref(context);
        toolbar = (Toolbar) findViewById(R.id.toolbar);


        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);


        Log.e("token____", SavePref.getDeviceToken(this, "token"));
        Log.e("auth_key___", savePref.getAuthorization_key());
        to_request = getIntent().getBooleanExtra("to_request", false);
        if (!to_request) {
            navigation.setSelectedItemId(R.id.navigation_event);
        } else {
            navigation.setSelectedItemId(R.id.navigation_chat);
        }


    }

    @Override
    protected void onResume() {
        super.onResume();
        shareopencheck();
        Log.e("taskkkkk", "onResume");


        if (ConnectivityReceiver.isConnected()) {
            gpsTracker = new GPSTracker(context);

            Log.e("current_location-", String.valueOf(gpsTracker.getLatitude()) + " " + String.valueOf(gpsTracker.getLongitude()));
            latitude = String.valueOf(gpsTracker.getLatitude());
            longitude = String.valueOf(gpsTracker.getLongitude());

            Geocoder geocoder;
            List<Address> addresses = null;
            geocoder = new Geocoder(context, Locale.getDefault());

            try {
                addresses = geocoder.getFromLocation(Double.parseDouble(latitude), Double.parseDouble(longitude), 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (addresses != null)
                zip = addresses.get(0).getPostalCode();

            if(zip==null)
                zip="";


//            Log.e("current_location-", zip);
            ADS();
        } else
            util.IOSDialog(context, util.internet_Connection_Error);


    }

    private void ADS() {
        ProgressDialog mDialog = util.initializeProgress(context);
        //  mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.ZIPCODE, zip);
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.ADS, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                list = new ArrayList<>();
                if (list.size() > 0)
                    list.clear();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {

                            JSONArray time_slots = jsonmainObject.getJSONArray("body");
                            for (int i = 0; i < time_slots.length(); i++) {
                                JSONObject object = time_slots.getJSONObject(i);
                                OffersModel offersModel = new OffersModel();
                                offersModel.setId(object.getString("id"));
                                offersModel.setBusinesses_name(object.getString("title"));
                                offersModel.setImage(object.getString("image"));
                                offersModel.setOffer(object.getString("link"));
                                offersModel.setZipcode(object.getString("zipcode"));
                                list.add(offersModel);
                            }
                            if (list.size() > 0) {
                                ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(context, list);
                                pager.setAdapter(viewPagerAdapter);
                                indicator.setViewPager(pager);
                                pager.startAutoScroll(4000);

                            } else {

                            }


                        } else {

                            if (jsonmainObject.getString("msg").equals(util.Invalid_Authorization)) {
                                 savePref.setAuthorization_key("");
                                Intent intent = new Intent(context, SignInActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                            } else {
                                util.showToast(context, jsonmainObject.getString("msg"));
                            }
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        for (Fragment fragment : getSupportFragmentManager().getFragments()) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            toolbar.setVisibility(View.VISIBLE);
            switch (item.getItemId()) {
                case R.id.navigation_event:
                    title.setText(getString(R.string.home));
                    loadFragment(new HomeFragment());
                    return true;
                case R.id.navigation_notification:
                    title.setText(getString(R.string.favorite));
                    loadFragment(new FavoriteFragment());
                    return true;
                case R.id.navigation_class:
                    // title.setText("Feed");
                    title.setText(getString(R.string.offers));
                    loadFragment(new OffersFragment());
                    return true;
                case R.id.navigation_chat:
                    title.setText(getString(R.string.orders));
                    loadFragment(new OrdersFragment());
                    return true;
                case R.id.navigation_profile:
                    title.setText(getString(R.string.profile));
                    loadFragment(new ProfileFragment());
                    return true;
            }
            return false;
        }
    };

    private void loadFragment(Fragment fragment) {
        // load fragment
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_container, fragment);
        transaction.commit();
    }


    @Override
    protected void onRestart() {
        super.onRestart();
        Log.e("taskkkkk", "onRestart");
        shareopencheck();
    }

    private void shareopencheck() {

        if (!savePref.getAuthorization_key().isEmpty()) {
            if (savePref.getStringLatest("user_type").equals("2")) {
                Intent intent = new Intent(context, BarbarMainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
            } else {
                try {
                    //check_notification_flag = true;
                    final Intent intent = getIntent();
                    Uri data = intent.getData();
                    Log.e("data_share", data.toString());
                    String post_id = data.toString().substring(data.toString().lastIndexOf("app") + 3);
                    Log.e("post_id", post_id);

                    // Receiving side
                    byte[] data__ = Base64.decode(post_id, Base64.DEFAULT);
                    String text = new String(data__, StandardCharsets.UTF_8);

                    Log.e("post_id", text);

                    Intent intent1 = new Intent(context, BarbarDetailActivity.class);
                    intent1.putExtra("id", text);
                    intent1.putExtra("is_from_push", true);
                    intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    context.startActivity(intent1);

                } catch (Exception e) {
                    String s = e.toString();
                    Log.e("taskkkkk", s);
                }
            }
        } else {
            Intent intent1 = new Intent(context, SignInActivity.class);
            intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            context.startActivity(intent1);

        }


    }
}