package com.ez_schedule.Util;

public class Parameters {
    public static final String AUTHORIZATION_KEY = "auth_key";
    public static final String SECURITYKEY = "security_key";
    public static final String AUTH_KEY = "auth_key";
    public static final String USERID = "userId";
    public static final String GENDER = "gender";
    public static final String PHONE_CODE = "phoneCode";
    public static final String PROFILE_IMAGE = "profile_image";
    public static final String FCM_TOKEN = "fcmToken";
    public static final String EMAIL = "email";
    public static final String SOCIAL_ID = "social_id";
    public static final String SOCIAL_TOKEN = "social_token";
    public static final String SOCIAL_TYPE = "soical_type";
    public static final String OTP = "otp";
    public static final String DEVICE_TYPE = "device_type";
    public static final String TAG_ID = "tag_id";
    public static final String NAME = "name";
    public static final String PASSWORD = "password";
    public static final String PHONE = "phone";
    public static final String COUNTRY_CODE = "countryCode";
    public static final String COUNTRY = "country";
    public static final String OLD_PASSWORD = "old_password";
    public static final String NEW_PASSWORD = "new_password";
    public static final String PROFILE = "profile";
    public static final String START_TIME_CALL = "start_time_call";
    public static final String END_TIME_CALL = "end_time_call";
    public static final String CENTRE = "centre";
    public static final String MESSAGE_TYPE = "message_type";
    public static final String USERNAME = "username";
    public static final String DOB = "dob";
    public static final String ADDRESS = "address";

    public static final String MESSAGE = "message";
    public static final String CLASS_ID = "class_id";

    public static final String CHAT_ID = "chatMessageId";
    public static final String TYPE = "type";

    public static final String DEVICE_TOKEN = "device_token";
    public static final String DESCRIPTION = "description";
    public static final String GROUPLIMIT = "groupLimit";
    public static final String USERIDS = "userIds";
    public static final String USERS = "users";
    public static final String KEYWORD = "keyWord";
    public static final String GROUPID = "groupId";
    public static final String NUMBERS = "numbers";
    public static final String THUMB_IMAGE = "thumb_image";
    public static final String PAGE = "page";
    public static final String LIMIT = "limit";
    public static final String FIRST_NAME = "first_name";
    public static final String USER_TYPE = "user_type";
    public static final String IMAGE = "image";
    public static final String CATEGORY_ID = "category_id";
    public static final String PRICE = "price";
    public static final String SERVICE_ID = "service_id";
    public static final String BARBER_ID = "barber_id";
    public static final String LAT = "lat";
    public static final String LNG = "lng";
    public static final String REQUEST_ID = "request_id";
    public static final String STATUS = "status";
    public static final String IS_UPCOMING = "is_upcoming";
    public static final String DATE = "date";
    public static final String SLOT_ID = "slot_id";
    public static final String IS_REWARDED = "is_rewarded";
    public static final String TOTAL_AMOUNT = "total_amount";
    public static final String SERVICES = "services";
    public static final String OPEN_TIME = "open_time";
    public static final String CLOSE_TIME = "close_time";
    public static final String ADDITIONAL_INFO = "info";
    public static final String ORDER_ID = "order_id";
    public static final String FROM_ORDER_ID = "from_order_id";
    public static final String TO_ORDER_ID = "to_order_id";
    public static final String TO_USER_ID = "to_user_id";

    public static final String REQUEST_TYPE = "request_type";
    public static final String TO_ID = "to_id";
    public static final String RATING = "rating";
    public static final String REVIEW = "review";
    public static final String REWARD_PERCENTANGE = "reward_percentage";
    public static final String REWARD_ORDER_COUNT = "reward_order_count";
    public static final String PAYMENT_NONCE = "payment_nonce";
    public static final String SQUARE_ID = "square_id";
    public static final String CHARITY_ID = "charity_id";
    public static final String AMOUNT = "amount";
    public static final String DURATION = "duration";
    public static final String NO_SLOTS = "no_slots";

    public static final String SCHEDULE = "schedule";
    public static final String CURRENT_TIMESTAMP = "current_timestamp";
    public static final String IDS = "ids";
    public static final String BLOCK_DATE = "block_date";
    public static final String BLOCK_WHOLE_DAY = "block_whole_day";
    public static final String ZIPCODE = "zipcode";
    public static final String SEARCH = "search";
    public static final String LAST_NAME ="last_name" ;
    public static final String CARD_NONCE ="card_nonce" ;
    public static final String EMAIL_ADDRESS ="email_address" ;
    public static final String PHONE_NUMNBER ="phone_number" ;
    public static final String NAME_SPANISH ="name_spanish" ;
}















