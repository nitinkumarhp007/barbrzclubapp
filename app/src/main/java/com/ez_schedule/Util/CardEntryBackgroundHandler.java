package com.ez_schedule.Util;

import android.util.Log;

import com.ez_schedule.UserFragments.OrdersFragment;

import java.io.IOException;

import sqip.CardDetails;
import sqip.CardEntryActivityCommand;
import sqip.CardNonceBackgroundHandler;

public class CardEntryBackgroundHandler implements CardNonceBackgroundHandler {
    @Override
    public CardEntryActivityCommand handleEnteredCardInBackground(CardDetails cardDetails) {

        String cardDetails__ = cardDetails.getNonce();
        Log.e("cardDetails__", cardDetails__);

        return new CardEntryActivityCommand.Finish();


        /*
        OrdersFragment ordersFragment = new OrdersFragment();

        boolean is_success = ordersFragment.ORDER_PAYMENT(0, cardDetails__);

        if (is_success) {
            return new CardEntryActivityCommand.Finish();
        } else {
            return new CardEntryActivityCommand.ShowError("Something went wrong");
        }
*/
    }
}
