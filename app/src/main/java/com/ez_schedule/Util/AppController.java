package com.ez_schedule.Util;

import android.app.Application;
import android.content.Intent;
import android.content.res.Configuration;

import com.ez_schedule.UserFragments.OrdersFragment;
import com.franmontiel.localechanger.LocaleChanger;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import sqip.CardEntry;


public class AppController extends Application
{
    public static final String TAG = AppController.class.getSimpleName();
    public static final List<Locale> SUPPORTED_LOCALES =
            Arrays.asList(
                    new Locale("en", "US"),
                    new Locale("es", "ES")
            );
    private static AppController mInstance;

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        LocaleChanger.onConfigurationChanged();
    }

    @Override
    public void onCreate()
    {
        super.onCreate();
        mInstance = this;
        Intent intent= new Intent(mInstance, NetworkServices.class);
        mInstance.startService(intent);
        LocaleChanger.initialize(getApplicationContext(), SUPPORTED_LOCALES);


        CardEntryBackgroundHandler cardHandler =
                new CardEntryBackgroundHandler();

        CardEntry.setCardNonceBackgroundHandler(cardHandler);

    }

    public static synchronized AppController getInstance()
    {
        return mInstance;
    }

    public void setConnectivityListener(ConnectivityReceiver.ConnectivityReceiverListener listener)
    {
        ConnectivityReceiver.connectivityReceiverListener = listener;
    }
}