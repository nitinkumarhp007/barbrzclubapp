package com.ez_schedule.BarbarFragments;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.applandeo.materialcalendarview.EventDay;
import com.applandeo.materialcalendarview.listeners.OnDayClickListener;
import com.ez_schedule.Activities.SignInActivity;
import com.ez_schedule.BarbarAdapters.BarbarOrdersAdapter;
import com.ez_schedule.ModelClasses.BarbarServiceListModel;
import com.ez_schedule.ModelClasses.UserOrdersModel;
import com.ez_schedule.R;
import com.ez_schedule.Util.ConnectivityReceiver;
import com.ez_schedule.Util.Parameters;
import com.ez_schedule.Util.SavePref;
import com.ez_schedule.Util.util;
import com.ez_schedule.parser.AllAPIS;
import com.ez_schedule.parser.GetAsync;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class CalenderFragment extends Fragment {


    public CalenderFragment() {
        // Required empty public constructor
    }

    Context context;
    Unbinder unbinder;
    long selcted_date = 0;
    private SavePref savePref;
    @BindView(R.id.compactcalendar_view)
    com.applandeo.materialcalendarview.CalendarView compactcalendarView;
    @BindView(R.id.my_recycler_view)
    RecyclerView myRecyclerView;
    @BindView(R.id.error_message)
    TextView errorMessage;

    ArrayList<UserOrdersModel> list_main;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_calender, container, false);

        unbinder = ButterKnife.bind(this, view);
        context = getActivity();
        savePref = new SavePref(context);


        compactcalendarView.setHeaderColor(R.color.colorAccent);
        compactcalendarView.setOnDayClickListener(new OnDayClickListener() {
            @Override
            public void onDayClick(EventDay eventDay) {

                selcted_date = eventDay.getCalendar().getTimeInMillis();

                Calendar c = Calendar.getInstance();
                TimeZone tz = TimeZone.getDefault();
                c.add(c.MILLISECOND, tz.getOffset(c.getTimeInMillis()));

                /*List<Calendar> calendars = new ArrayList<>();
                calendars.add(c);
                compactcalendarView.setSelectedDates(calendars);*/

                SimpleDateFormat day = new SimpleDateFormat("dd-MM-yyyy");
                day.setTimeZone(tz);
                Date currenTimeZone = new Date(selcted_date);
                String day_text = day.format(currenTimeZone);




                Log.e("calender_time_s: ", day_text);
                if (ConnectivityReceiver.isConnected()) {
                    CALENDER_ORDERS_LIST(util.date_to_timestamp(day_text));
                } else {
                    util.IOSDialog(context, util.internet_Connection_Error);
                }


            }
        });

        if (ConnectivityReceiver.isConnected()) {
            String date = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault()).format(new Date());
            Log.e("calender_time_s", date);
            CALENDER_ORDERS_LIST(util.date_to_timestamp(date));
        } else {
            util.IOSDialog(context, util.internet_Connection_Error);
        }


        return view;
    }

    private void CALENDER_ORDERS_LIST(String date) {
        Log.e("calender_time_s", date);
        ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.DATE, date);
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.CALENDER_ORDERS, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                list_main = new ArrayList<>();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {
                            JSONArray body = jsonmainObject.getJSONArray("body");
                            for (int i = 0; i < body.length(); i++) {
                                JSONObject object = body.getJSONObject(i);
                                UserOrdersModel requestModel = new UserOrdersModel();
                                requestModel.setStatus(object.getJSONObject("order").getString("status"));
                                requestModel.setDate(object.getJSONObject("order").getString("date"));
                                requestModel.setCost(object.getJSONObject("order").getString("total_amount"));
                                requestModel.setId(object.getJSONObject("order").getString("id"));
                                requestModel.setImage(object.getJSONObject("user").getString("profile_image"));
                                requestModel.setUsername(object.getJSONObject("user").getString("username"));
                                requestModel.setSlot_Detail(object.getJSONObject("slot_Detail").getString("start_time") + "-" + object.getJSONObject("slot_Detail").getString("end_time"));
                                ArrayList<BarbarServiceListModel> list = new ArrayList<>();
                                for (int j = 0; j < object.getJSONArray("services_detail").length(); j++) {
                                    JSONObject obj = object.getJSONArray("services_detail").getJSONObject(j);
                                    BarbarServiceListModel barbarServiceListModel = new BarbarServiceListModel();
                                    barbarServiceListModel.setId(obj.getString("id"));
                                    barbarServiceListModel.setName(obj.getString("name"));
                                    barbarServiceListModel.setPrice(obj.getString("price"));
                                    barbarServiceListModel.setDuration(obj.optString("duration"));
                                    list.add(barbarServiceListModel);
                                }
                                requestModel.setList(list);
                                list_main.add(requestModel);
                            }


                            if (list_main.size() > 0) {
                                BarbarOrdersAdapter adapter = new BarbarOrdersAdapter(context, list_main, "2");
                                myRecyclerView.setLayoutManager(new LinearLayoutManager(context));
                                myRecyclerView.setAdapter(adapter);

                                myRecyclerView.setVisibility(View.VISIBLE);
                                errorMessage.setVisibility(View.GONE);

                            } else {
                                errorMessage.setVisibility(View.VISIBLE);
                                myRecyclerView.setVisibility(View.GONE);
                            }

                        } else {
                            if (jsonmainObject.getString("msg").equals(util.Invalid_Authorization)) {
                                 savePref.setAuthorization_key("");
                                Intent intent = new Intent(context, SignInActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                            } else {
                                util.showToast(context, jsonmainObject.getString("msg"));
                            }
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

}