package com.ez_schedule.BarbarFragments;


import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ez_schedule.Activities.CharityActivity;
import com.ez_schedule.Activities.ForgotPasswordActivity;
import com.ez_schedule.Activities.SignInActivity;
import com.ez_schedule.Activities.SplashActivity;
import com.ez_schedule.Activities.SubscriptionActivity;
import com.ez_schedule.BarbarActivities.BarbarMainActivity;
import com.ez_schedule.BarbarAdapters.RequetsAdapter;
import com.ez_schedule.MainActivity;
import com.ez_schedule.ModelClasses.BarbarModel;
import com.ez_schedule.ModelClasses.BarbarServiceListModel;
import com.ez_schedule.ModelClasses.OffersModel;
import com.ez_schedule.ModelClasses.RequestModel;
import com.ez_schedule.R;
import com.ez_schedule.UserAdapters.BarbarsListAdapter;
import com.ez_schedule.UserAdapters.FavoriteAdapter;
import com.ez_schedule.UserAdapters.ViewPagerAdapter;
import com.ez_schedule.UserFragments.FavoriteFragment;
import com.ez_schedule.Util.ConnectivityReceiver;
import com.ez_schedule.Util.Parameters;
import com.ez_schedule.Util.SavePref;
import com.ez_schedule.Util.util;
import com.ez_schedule.parser.AllAPIS;
import com.ez_schedule.parser.GetAsync;
import com.ez_schedule.parser.GetAsyncGet;
import com.ligl.android.widget.iosdialog.IOSDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import sqip.Card;
import sqip.CardDetails;
import sqip.CardEntry;

import static sqip.CardEntry.DEFAULT_CARD_ENTRY_REQUEST_CODE;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeBarbarFragment extends Fragment {
    Context context;
    @BindView(R.id.search_bar)
    EditText searchBar;
    @BindView(R.id.my_recycler_view)
    RecyclerView myRecyclerView;
    @BindView(R.id.error_message)
    TextView errorMessage;
    private SavePref savePref;
    Unbinder unbinder;
    RequetsAdapter adapter;

    IOSDialog iosDialog;

    ProgressDialog mDialog;

    ArrayList<RequestModel> list_main;

    public HomeBarbarFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home_barbar, container, false);

        unbinder = ButterKnife.bind(this, view);
        context = getActivity();
        savePref = new SavePref(context);

        mDialog = util.initializeProgress(context);


     /*   startActivity(new Intent(getActivity(), CharityActivity.class));
        getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);*/


        searchBar.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (adapter != null)
                    adapter.filter(s.toString().trim());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        SUBSCRIPTION_SETTINGAPI();
    }


    private void BARBER_REQUESTS_LIST() {
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.AUTH_KEY, "");
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsyncGet mAsync = new GetAsyncGet(context, AllAPIS.BARBER_REQUESTS, formBody) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                Log.e("resultresult", result);
                list_main = new ArrayList<>();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {
                            JSONArray body = jsonmainObject.getJSONArray("body");
                            for (int i = 0; i < body.length(); i++) {
                                JSONObject object = body.getJSONObject(i);
                                RequestModel requestModel = new RequestModel();
                                requestModel.setCost(object.getJSONObject("request").getString("total_amount"));
                                requestModel.setDate(object.getJSONObject("request").getString("date"));
                                requestModel.setId(object.getJSONObject("request").getString("id"));
                                requestModel.setStatus(object.getJSONObject("request").getString("status"));
                                requestModel.setInfo(object.getJSONObject("request").getString("info"));
                                requestModel.setImage(object.getJSONObject("user").getString("profile_image"));
                                requestModel.setUsername(object.getJSONObject("user").getString("username"));
                                //  requestModel.setSlot_Detail(object.getJSONObject("slot_Detail").getString("slot"));
                                requestModel.setSlot_Detail(object.getJSONObject("slot_Detail").getString("start_time") + "-" + object.getJSONObject("slot_Detail").getString("end_time"));
                                ArrayList<BarbarServiceListModel> list = new ArrayList<>();
                                for (int j = 0; j < object.getJSONArray("services_detail").length(); j++) {
                                    JSONObject obj = object.getJSONArray("services_detail").getJSONObject(j);
                                    BarbarServiceListModel barbarServiceListModel = new BarbarServiceListModel();
                                    barbarServiceListModel.setId(obj.getString("id"));
                                    barbarServiceListModel.setName(obj.getString("name"));
                                    barbarServiceListModel.setPrice(obj.getString("price"));
                                    barbarServiceListModel.setDuration(obj.optString("duration"));
                                    list.add(barbarServiceListModel);
                                }
                                requestModel.setList(list);
                                list_main.add(requestModel);
                            }


                            if (list_main.size() > 0) {
                                adapter = new RequetsAdapter(context, list_main, HomeBarbarFragment.this);
                                myRecyclerView.setLayoutManager(new LinearLayoutManager(context));
                                myRecyclerView.setAdapter(adapter);

                                myRecyclerView.setVisibility(View.VISIBLE);
                                errorMessage.setVisibility(View.GONE);

                            } else {
                                errorMessage.setVisibility(View.VISIBLE);
                                myRecyclerView.setVisibility(View.GONE);
                            }

                        } else {
                            if (jsonmainObject.getString("msg").equals(util.Invalid_Authorization)) {
                                 savePref.setAuthorization_key("");
                                Intent intent = new Intent(context, SignInActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                            } else {
                                util.showToast(context, jsonmainObject.getString("msg"));
                            }
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    public void CANCEL_ORDER_API(String request_id, int position) {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.REQUEST_ID, request_id);
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.CANCEL_ORDER, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonObject = new JSONObject(result);
                        String t = getString(R.string.order_cancelled_successfully);

                        new IOSDialog.Builder(context)
                                .setCancelable(false)
                                .setMessage(t).setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                list_main.get(position).setStatus("5");
                                adapter.notifyDataSetChanged();
                            }
                        }).show();


                    } catch (Exception e) {
                    }
                }

            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    public void BARBER_REQURST_STATUS_API(String request_id, String status) {
        int position = 0;
        for (int i = 0; i < list_main.size(); i++) {
            if (list_main.get(i).getId().equals(request_id))
                position = i;
        }
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.REQUEST_ID, request_id);
        formBuilder.addFormDataPart(Parameters.STATUS, status);
        RequestBody formBody = formBuilder.build();
        int finalPosition = position;
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.BARBER_REQURST_STATUS, formBody, savePref.getAuthorization_key()) {

            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonMainobject = new JSONObject(result);
                        if (jsonMainobject.getString("code").equalsIgnoreCase("200")) {
                            String s = "";
                            if (status.equals("1"))
                                s = getString(R.string.request_accepted_successfully);
                            else
                                s = getString(R.string.request_declined_successfully);

                            new IOSDialog.Builder(context)
                                    .setCancelable(false)
                                    .setMessage(s).setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    list_main.get(finalPosition).setStatus(status);
                                    adapter.notifyDataSetChanged();
                                }
                            }).show();
                        } else {
                            util.IOSDialog(context, jsonMainobject.getString("msg"));
                        }

                    } catch (Exception e) {
                    }
                }

            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    private void SUBSCRIPTION_SETTINGAPI() {
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.ZIPCODE, "123");
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsyncGet mAsync = new GetAsyncGet(context, AllAPIS.SUBSCRIPTION_SETTING, formBody) {
            @Override
            public void getValueParse(String result) {
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {

                            Log.e("resulttttt", result);

                            JSONObject body = jsonmainObject.getJSONObject("body");

                            String subscription_status = body.getString("subscription_status");

                           // savePref.setString("subscription_status", subscription_status);


                            String subscription_price = body.getString("subscription_price");
                            savePref.setStringLatest("subscription_price", subscription_price);


                            if (subscription_status.equals("1")) {



                                if (savePref.getStringLatest("subscription_status").equals("0") || savePref.getStringLatest("subscription_status").equals("")) {
                                    mDialog.dismiss();
                                    iosDialog = new IOSDialog.Builder(context)
                                            .setCancelable(false)
                                            .setTitle(context.getResources().getString(R.string.app_name))
                                            .setMessage(getString(R.string.sub_text)).setPositiveButton(R.string.proceed, new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialogInterface, int which) {
                                                    iosDialog.dismiss();
                                                    Intent intent = new Intent(context, SubscriptionActivity.class);
                                                    intent.putExtra("subscription_price", subscription_price);
                                                    startActivity(intent);
                                                    getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                                                }
                                            })
                                            .setNegativeButton(getString(R.string.exit), new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    savePref.setAuthorization_key("");
                                                    Intent intent = new Intent(context, SignInActivity.class);
                                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                                    startActivity(intent);
                                                    getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                                                }
                                            }).show();

                                } else {
                                    if (ConnectivityReceiver.isConnected()) {
                                        BARBER_REQUESTS_LIST();
                                    } else {
                                        util.IOSDialog(context, util.internet_Connection_Error);
                                    }
                                }

                            } else {
                                if (ConnectivityReceiver.isConnected()) {
                                    BARBER_REQUESTS_LIST();
                                } else {
                                    util.IOSDialog(context, util.internet_Connection_Error);
                                }
                            }


                        } else {

                            if (jsonmainObject.getString("msg").equals(util.Invalid_Authorization)) {
                                 savePref.setAuthorization_key("");
                                Intent intent = new Intent(context, SignInActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                            } else {
                                util.showToast(context, jsonmainObject.getString("msg"));
                            }
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }


}