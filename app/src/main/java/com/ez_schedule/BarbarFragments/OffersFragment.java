package com.ez_schedule.BarbarFragments;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ez_schedule.Activities.SignInActivity;
import com.ez_schedule.ModelClasses.OffersModel;
import com.ez_schedule.ModelClasses.TimeslotModel;
import com.ez_schedule.R;
import com.ez_schedule.UserAdapters.ChangeTimeSlotAdapter;
import com.ez_schedule.UserAdapters.HomeBarbarsAdapter;
import com.ez_schedule.UserAdapters.OffersAdapter;
import com.ez_schedule.UserFragments.HomeFragment;
import com.ez_schedule.Util.ConnectivityReceiver;
import com.ez_schedule.Util.GPSTracker;
import com.ez_schedule.Util.Parameters;
import com.ez_schedule.Util.SavePref;
import com.ez_schedule.Util.util;
import com.ez_schedule.parser.AllAPIS;
import com.ez_schedule.parser.GetAsync;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class OffersFragment extends Fragment {

    Context context;
    @BindView(R.id.my_recycler_view)
    RecyclerView myRecyclerView;
    @BindView(R.id.error_message)
    TextView errorMessage;
    private SavePref savePref;
    Unbinder unbinder;

    private ArrayList<OffersModel> list;
    String latitude = "", longitude = "";
    public String zip = "";
    GPSTracker gpsTracker = null;

    public OffersFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_offers, container, false);

        unbinder = ButterKnife.bind(this, view);
        context = getActivity();
        savePref = new SavePref(context);

        gpsTracker = new GPSTracker(context);


        if (ConnectivityReceiver.isConnected()) {
            Log.e("current_location-", String.valueOf(gpsTracker.getLatitude()) + " " + String.valueOf(gpsTracker.getLongitude()));
            latitude = String.valueOf(gpsTracker.getLatitude());
            longitude = String.valueOf(gpsTracker.getLongitude());

            Geocoder geocoder;
            List<Address> addresses = null;
            geocoder = new Geocoder(context, Locale.getDefault());

            try {
                addresses = geocoder.getFromLocation(Double.parseDouble(latitude), Double.parseDouble(longitude), 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (addresses != null)
                zip = addresses.get(0).getPostalCode();


            Log.e("current_location-", zip);

            if (ConnectivityReceiver.isConnected()) {
                OFFERS();
            } else {
                util.IOSDialog(context, util.internet_Connection_Error);
            }
        } else {
            util.IOSDialog(context, util.internet_Connection_Error);
        }

        return view;
    }

    private void OFFERS() {
        ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.ZIPCODE, zip);
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.OFFERS, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                list = new ArrayList<>();
                if (list.size() > 0)
                    list.clear();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {

                            JSONArray time_slots = jsonmainObject.getJSONArray("body");
                            for (int i = 0; i < time_slots.length(); i++) {
                                JSONObject object = time_slots.getJSONObject(i);
                                OffersModel offersModel = new OffersModel();
                                offersModel.setId(object.getString("id"));
                                offersModel.setBusinesses_name(object.getString("businesses_name"));
                                offersModel.setImage(object.getString("image"));
                                offersModel.setOffer(object.getString("offer"));
                                offersModel.setZipcode(object.getString("zipcode"));
                                list.add(offersModel);
                            }
                            if (list.size() > 0) {
                                OffersAdapter adapter = new OffersAdapter(context, list);
                                myRecyclerView.setLayoutManager(new LinearLayoutManager(context));
                                myRecyclerView.setAdapter(adapter);
                            } else {

                            }


                        } else {

                            if (jsonmainObject.getString("msg").equals(util.Invalid_Authorization)) {
                                 savePref.setAuthorization_key("");
                                Intent intent = new Intent(context, SignInActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                            } else {
                                util.showToast(context, jsonmainObject.getString("msg"));
                            }
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

}