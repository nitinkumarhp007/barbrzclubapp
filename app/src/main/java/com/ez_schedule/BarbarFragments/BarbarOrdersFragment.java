package com.ez_schedule.BarbarFragments;


import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ez_schedule.Activities.SignInActivity;
import com.ez_schedule.BarbarAdapters.BarbarOrdersAdapter;
import com.ez_schedule.ModelClasses.BarbarServiceListModel;
import com.ez_schedule.ModelClasses.UserOrdersModel;
import com.ez_schedule.R;
import com.ez_schedule.UserAdapters.OrderAdapter;
import com.ez_schedule.Util.ConnectivityReceiver;
import com.ez_schedule.Util.Parameters;
import com.ez_schedule.Util.SavePref;
import com.ez_schedule.Util.util;
import com.ez_schedule.parser.AllAPIS;
import com.ez_schedule.parser.GetAsync;
import com.ez_schedule.parser.GetAsyncGet;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

/**
 * A simple {@link Fragment} subclass.
 */
public class BarbarOrdersFragment extends Fragment {


    Context context;
    @BindView(R.id.upcoming)
    Button upcoming;
    @BindView(R.id.past)
    Button past;
    @BindView(R.id.upcoming_view)
    View upcomingView;
    @BindView(R.id.past_view)
    View pastView;
    @BindView(R.id.my_recycler_view)
    RecyclerView myRecyclerView;
    @BindView(R.id.error_message)
    TextView errorMessage;
    private SavePref savePref;
    Unbinder unbinder;
    ArrayList<UserOrdersModel> list_main;

    public BarbarOrdersFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_barbar_orders, container, false);

        unbinder = ButterKnife.bind(this, view);
        context = getActivity();
        savePref = new SavePref(context);



        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        upcomingView.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
        pastView.setBackgroundColor(getResources().getColor(R.color.white));
        if (ConnectivityReceiver.isConnected()) {
            BARBER_ORDERS_LIST("1");
        } else {
            util.IOSDialog(context, util.internet_Connection_Error);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.upcoming, R.id.past})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.upcoming:
                upcomingView.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
                pastView.setBackgroundColor(getResources().getColor(R.color.white));
                upcoming.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                past.setTextColor(getResources().getColor(R.color.black));
                if (ConnectivityReceiver.isConnected()) {
                    BARBER_ORDERS_LIST("1");
                } else {
                    util.IOSDialog(context, util.internet_Connection_Error);
                }

                break;
            case R.id.past:
                pastView.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
                upcomingView.setBackgroundColor(getResources().getColor(R.color.white));
                past.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                upcoming.setTextColor(getResources().getColor(R.color.black));
                if (ConnectivityReceiver.isConnected()) {
                    BARBER_ORDERS_LIST("2");
                } else {
                    util.IOSDialog(context, util.internet_Connection_Error);
                }
                break;
        }
    }

    private void BARBER_ORDERS_LIST(String is_upcoming) {
        ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.IS_UPCOMING, is_upcoming);
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.BARBER_ORDERS, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                list_main = new ArrayList<>();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {
                            JSONArray body = jsonmainObject.getJSONArray("body");
                            for (int i = 0; i < body.length(); i++) {
                                JSONObject object = body.getJSONObject(i);
                                UserOrdersModel requestModel = new UserOrdersModel();
                                requestModel.setStatus(object.getJSONObject("order").getString("status"));
                                requestModel.setDate(object.getJSONObject("order").getString("date"));
                                requestModel.setCost(object.getJSONObject("order").getString("total_amount"));
                                requestModel.setId(object.getJSONObject("order").getString("id"));
                                requestModel.setImage(object.getJSONObject("user").getString("profile_image"));
                                requestModel.setUsername(object.getJSONObject("user").getString("username"));
                                requestModel.setSlot_Detail(object.getJSONObject("slot_Detail").getString("start_time") + "-" + object.getJSONObject("slot_Detail").getString("end_time"));
                                ArrayList<BarbarServiceListModel> list = new ArrayList<>();
                                for (int j = 0; j < object.getJSONArray("services_detail").length(); j++) {
                                    JSONObject obj = object.getJSONArray("services_detail").getJSONObject(j);
                                    BarbarServiceListModel barbarServiceListModel = new BarbarServiceListModel();
                                    barbarServiceListModel.setId(obj.getString("id"));
                                    barbarServiceListModel.setName(obj.getString("name"));
                                    barbarServiceListModel.setPrice(obj.getString("price"));
                                    barbarServiceListModel.setDuration(obj.optString("duration"));
                                    list.add(barbarServiceListModel);
                                }
                                requestModel.setList(list);
                                list_main.add(requestModel);
                            }


                            if (list_main.size() > 0) {
                                BarbarOrdersAdapter adapter = new BarbarOrdersAdapter(context, list_main,is_upcoming);
                                myRecyclerView.setLayoutManager(new LinearLayoutManager(context));
                                myRecyclerView.setAdapter(adapter);

                                myRecyclerView.setVisibility(View.VISIBLE);
                                errorMessage.setVisibility(View.GONE);

                            } else {
                                errorMessage.setVisibility(View.VISIBLE);
                                myRecyclerView.setVisibility(View.GONE);
                            }

                        } else {
                            if (jsonmainObject.getString("msg").equals(util.Invalid_Authorization)) {
                                 savePref.setAuthorization_key("");
                                Intent intent = new Intent(context, SignInActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                            } else {
                                util.showToast(context, jsonmainObject.getString("msg"));
                            }
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }
}