package com.ez_schedule.BarbarFragments;


import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ez_schedule.Activities.WebViewActivity;
import com.ez_schedule.BarbarActivities.AddServiceActivity;
import com.ez_schedule.BarbarActivities.BarbarMainActivity;
import com.ez_schedule.BarbarAdapters.BarbarServicesAdapter;
import com.ez_schedule.ModelClasses.BarbarServiceListModel;
import com.ez_schedule.R;
import com.ez_schedule.Util.ConnectivityReceiver;
import com.ez_schedule.Util.Parameters;
import com.ez_schedule.Util.SavePref;
import com.ez_schedule.Util.util;
import com.ez_schedule.parser.AllAPIS;
import com.ez_schedule.parser.GetAsync;
import com.ez_schedule.parser.GetAsyncGet;
import com.ligl.android.widget.iosdialog.IOSDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

/**
 * A simple {@link Fragment} subclass.
 */
public class BarbarServicesFragment extends Fragment {
    Context context;
    @BindView(R.id.search_bar)
    EditText searchBar;
    @BindView(R.id.my_recycler_view)
    RecyclerView myRecyclerView;
    @BindView(R.id.error_message)
    TextView errorMessage;
    @BindView(R.id.add_service)
    ImageView addService;
    private SavePref savePref;
    Unbinder unbinder;

    BarbarServicesAdapter adapter = null;

    private ArrayList<BarbarServiceListModel> list;

    public BarbarServicesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_barbar_services, container, false);

        unbinder = ButterKnife.bind(this, view);
        context = getActivity();
        savePref = new SavePref(context);


        searchBar.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (adapter != null)
                    adapter.filter(s.toString().trim());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (savePref.getStringLatest(Parameters.SQUARE_ID).isEmpty() || savePref.getStringLatest(Parameters.SQUARE_ID).equals("0")) {
            new IOSDialog.Builder(context)
                    .setTitle(context.getResources().getString(R.string.app_name))
                    .setCancelable(false)
                    .setMessage(getString(R.string.add_payment_mthod)).setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Intent intent = new Intent(context, WebViewActivity.class);
                    startActivity(intent);
                    getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                }
            })
                    .setNegativeButton(getString(R.string.skip_for_now), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            if (ConnectivityReceiver.isConnected()) {
                                BARBAR_SERVICES();
                            } else {
                                util.IOSDialog(context, util.internet_Connection_Error);
                            }
                        }
                    }).show();
        } else {
            if (ConnectivityReceiver.isConnected()) {
                BARBAR_SERVICES();
            } else {
                util.IOSDialog(context, util.internet_Connection_Error);
            }
        }

       /* if (ConnectivityReceiver.isConnected()) {
            BARBAR_SERVICES();
        } else {
            util.IOSDialog(context, util.internet_Connection_Error);
        }*/
    }

    private void BARBAR_SERVICES() {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.AUTH_KEY, "1234567");
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.BARBAR_SERVICES, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                list = new ArrayList<>();
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {

                            JSONArray body = jsonmainObject.getJSONArray("body");
                            for (int i = 0; i < body.length(); i++) {
                                JSONObject object = body.getJSONObject(i);

                                BarbarServiceListModel barbarListModel = new BarbarServiceListModel();
                                barbarListModel.setCategory_id(object.getString("category_id"));
                                //  barbarListModel.setCategory_name(object.getString("category_name"));
                                barbarListModel.setDescription(object.getString("description"));
                                barbarListModel.setId(object.getString("id"));
                                barbarListModel.setImage(object.getString("image"));
                                barbarListModel.setName(object.getString("name"));
                                barbarListModel.setName_spanish(object.optString("name_spanish"));
                                barbarListModel.setPrice(object.getString("price"));
                                barbarListModel.setDuration(object.optString("duration"));
                                list.add(barbarListModel);

                            }
                            if (list.size() > 0) {
                                Collections.reverse(list);
                                myRecyclerView.setVisibility(View.VISIBLE);
                                errorMessage.setVisibility(View.GONE);

                                adapter = new BarbarServicesAdapter(context, list, BarbarServicesFragment.this);
                                myRecyclerView.setLayoutManager(new GridLayoutManager(context, 2));
                                myRecyclerView.setAdapter(adapter);


                            } else {
                                errorMessage.setVisibility(View.VISIBLE);
                                myRecyclerView.setVisibility(View.GONE);
                            }
                        } else {
                            util.showToast(context, jsonmainObject.getString("msg"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    public void DeleteAlert(int position) {
        new IOSDialog.Builder(context)
                .setTitle(context.getResources().getString(R.string.app_name))
                .setCancelable(false)
                .setMessage(getString(R.string.are_you_sure_to_delete))
                .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        DELETE_PRODUCT_API(position);
                    }
                })
                .setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).show();
    }

    private void DELETE_PRODUCT_API(int position) {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.SERVICE_ID, list.get(position).getId());
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.DELETE_SERVICES, formBody, savePref.getAuthorization_key()) {

            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonObject = new JSONObject(result);
                        new IOSDialog.Builder(context)
                                .setCancelable(false)
                                .setMessage(getString(R.string.service_deleted_successfully)).setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                list.remove(position);
                                adapter.notifyDataSetChanged();
                            }
                        }).show();
                    } catch (Exception e) {
                    }
                }

            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.add_service)
    public void onClick() {
        startActivity(new Intent(context, AddServiceActivity.class));
        getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
    }
}