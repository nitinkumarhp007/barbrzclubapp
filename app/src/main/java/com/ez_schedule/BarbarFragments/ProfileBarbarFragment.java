package com.ez_schedule.BarbarFragments;


import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import com.ez_schedule.Activities.BlockTimeSlotActivity;
import com.ez_schedule.Activities.CancelSubscriptionActivity;
import com.ez_schedule.Activities.ChangePasswordActivity;
import com.ez_schedule.Activities.ConnectPaymentGatewayActivity;
import com.ez_schedule.Activities.PushNotificationActivity;
import com.ez_schedule.Activities.ReviewListActivity;
import com.ez_schedule.Activities.SignInActivity;
import com.ez_schedule.Activities.TermConditionActivity;
import com.ez_schedule.Activities.UpdateProfileActivity;
import com.ez_schedule.BarbarActivities.BarbarMainActivity;
import com.ez_schedule.BarbarActivities.RewardSettingActivity;
import com.ez_schedule.BarbarActivities.TrackSaleActivity;
import com.ez_schedule.MainActivity;
import com.ez_schedule.R;
import com.ez_schedule.Util.ConnectivityReceiver;
import com.ez_schedule.Util.LocaleHelper;
import com.ez_schedule.Util.Parameters;
import com.ez_schedule.Util.SavePref;
import com.ez_schedule.Util.util;
import com.ez_schedule.parser.AllAPIS;
import com.ez_schedule.parser.GetAsync;
import com.bumptech.glide.Glide;
import com.ligl.android.widget.iosdialog.IOSDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.FormBody;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileBarbarFragment extends Fragment {


    Context context;
    @BindView(R.id.profile_pic)
    CircleImageView profilePic;
    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.email)
    TextView email;
    @BindView(R.id.phone)
    TextView phone;
    @BindView(R.id.edit_profile_text)
    TextView editProfileText;
    @BindView(R.id.edit_profile111)
    RelativeLayout editProfile111;
    @BindView(R.id.change_password)
    TextView changePassword;
    @BindView(R.id.change_password___)
    RelativeLayout changePassword__;
    @BindView(R.id.privacy_policy)
    TextView privacyPolicy;
    @BindView(R.id.privacy_policy_layout)
    RelativeLayout privacyPolicyLayout;
    @BindView(R.id.terms)
    TextView terms;
    @BindView(R.id.terms_layout)
    RelativeLayout termsLayout;
    @BindView(R.id.logout)
    TextView logout;
    @BindView(R.id.logout_layout)
    RelativeLayout logoutLayout;
    @BindView(R.id.delete_profile)
    TextView delete_profile;
    @BindView(R.id.delete_profile___)
    RelativeLayout delete_profile___;
    @BindView(R.id.send_notification)
    TextView sendNotification;
    @BindView(R.id.send_notification___)
    RelativeLayout sendNotification___;
    @BindView(R.id.rating_bar)
    RatingBar ratingBar;
    @BindView(R.id.rating_bar_layout)
    RelativeLayout ratingBarLayout;
    @BindView(R.id.reward_setting)
    TextView rewardSetting;
    @BindView(R.id.reward_setting___)
    RelativeLayout rewardSetting__;
    @BindView(R.id.payment_gateways)
    TextView paymentGateways;
    @BindView(R.id.payment_gateways___)
    RelativeLayout payment_gateways___;
    @BindView(R.id.change_time_slot)
    TextView change_time_slot;
    @BindView(R.id.change_time_slot___)
    RelativeLayout change_time_slot___;
    @BindView(R.id.track_sale)
    TextView track_sale;
    @BindView(R.id.subscription___)
    RelativeLayout subscription___;
    @BindView(R.id.subscription)
    TextView subscription;
    @BindView(R.id.track_sale___)
    RelativeLayout track_sale___;
    @BindView(R.id.share)
    ImageView share;
    @BindView(R.id.language)
    TextView language;
    @BindView(R.id.language___)
    RelativeLayout language___;
    @BindView(R.id.subscription_cardview)
    CardView subscription_cardview;

    private SavePref savePref;
    Unbinder unbinder;

    Resources resources;
    Context context1;

    public ProfileBarbarFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_profile_barbar, container, false);

        unbinder = ButterKnife.bind(this, view);
        context = getActivity();
        savePref = new SavePref(context);

        if (savePref.getLang().equals("en"))
            language.setText(context.getResources().getString(R.string.language) + " (English)");
        else
            language.setText(context.getResources().getString(R.string.language) + " (Spanish)");

        if (savePref.getStringLatest("subscription_status").equals("1")) {
            subscription_cardview.setVisibility(View.VISIBLE);
        } else {
            subscription_cardview.setVisibility(View.GONE);
        }

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onResume() {
        super.onResume();
        setdata();
    }

    private void setdata() {
        name.setText(savePref.getName());
        email.setText(savePref.getEmail());
        phone.setText("+" + savePref.getPhone());

        Glide.with(context).load(savePref.getImage()).error(R.drawable.placeholder).into(profilePic);

        if (ConnectivityReceiver.isConnected())
            PROVIDER_RATINGS();
        else
            util.IOSDialog(context, util.internet_Connection_Error);

    }

    private void PROVIDER_RATINGS() {
        ProgressDialog mDialog = util.initializeProgress(context);
        /*try {
            mDialog.show();
        } catch (Exception e) {

        }*/

        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.TO_ID, savePref.getID());
        // formBuilder.addFormDataPart(Parameters.TYPE, "1");
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.GET_USER_RATINGS, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                try {
                    mDialog.dismiss();
                } catch (Exception e) {

                }
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {
                            JSONObject body = jsonmainObject.getJSONObject("body");

                            if (!body.getJSONObject("average_rating").getString("avg_rating").isEmpty())
                                ratingBar.setRating(Float.parseFloat(body.getJSONObject("average_rating").getString("avg_rating")));


                        } else {
                            if (jsonmainObject.getString("error_message").equals(util.Invalid_Authorization)) {
                                SavePref savePref = new SavePref(context);
                                savePref.setAuthorization_key("");
                                Intent intent = new Intent(context, SignInActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                            } else {
                                util.showToast(context, jsonmainObject.getString("msg"));
                            }
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }


    @OnClick({R.id.subscription, R.id.subscription___, R.id.language, R.id.language___, R.id.change_time_slot, R.id.share, R.id.change_time_slot___, R.id.track_sale, R.id.track_sale___, R.id.delete_profile, R.id.delete_profile___, R.id.payment_gateways, R.id.payment_gateways___, R.id.rating_bar_layout, R.id.send_notification, R.id.send_notification___, R.id.reward_setting, R.id.reward_setting___, R.id.edit_profile_text, R.id.edit_profile111, R.id.change_password, R.id.change_password___, R.id.privacy_policy, R.id.privacy_policy_layout, R.id.terms, R.id.terms_layout, R.id.logout, R.id.logout_layout})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.subscription:
                startActivity(new Intent(context, CancelSubscriptionActivity.class));
                getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
            case R.id.subscription___:
                startActivity(new Intent(context, CancelSubscriptionActivity.class));
                getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
            case R.id.change_time_slot:
                startActivity(new Intent(context, BlockTimeSlotActivity.class));
                getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
            case R.id.share:
                sharepost(savePref.getID());
                break;
            case R.id.change_time_slot___:
                startActivity(new Intent(context, BlockTimeSlotActivity.class));
                getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
            case R.id.track_sale:
                startActivity(new Intent(context, TrackSaleActivity.class));
                getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
            case R.id.track_sale___:
                startActivity(new Intent(context, TrackSaleActivity.class));
                getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
            case R.id.payment_gateways:
                startActivity(new Intent(context, ConnectPaymentGatewayActivity.class));
                getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
            case R.id.payment_gateways___:
                startActivity(new Intent(context, ConnectPaymentGatewayActivity.class));
                getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
            case R.id.rating_bar_layout:
                Intent intent = new Intent(getActivity(), ReviewListActivity.class);
                intent.putExtra("user_id", savePref.getID());
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
            case R.id.send_notification:
                startActivity(new Intent(getActivity(), PushNotificationActivity.class));
                getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
            case R.id.send_notification___:
                startActivity(new Intent(getActivity(), PushNotificationActivity.class));
                getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
            case R.id.reward_setting:
                startActivity(new Intent(getActivity(), RewardSettingActivity.class));
                getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
            case R.id.reward_setting___:
                startActivity(new Intent(getActivity(), RewardSettingActivity.class));
                getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
            case R.id.edit_profile_text:
                startActivity(new Intent(getActivity(), UpdateProfileActivity.class));
                getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
            case R.id.edit_profile111:
                startActivity(new Intent(getActivity(), UpdateProfileActivity.class));
                getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
            case R.id.change_password:
                startActivity(new Intent(getActivity(), ChangePasswordActivity.class));
                getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
            case R.id.change_password___:
                startActivity(new Intent(getActivity(), ChangePasswordActivity.class));
                getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
            case R.id.privacy_policy:
                Intent intent11111 = new Intent(context, TermConditionActivity.class);
                intent11111.putExtra("type", "privacy");
                startActivity(intent11111);
                getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
            case R.id.privacy_policy_layout:
                Intent intent111112 = new Intent(context, TermConditionActivity.class);
                intent111112.putExtra("type", "privacy");
                startActivity(intent111112);
                getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
            case R.id.terms:
                Intent intent11 = new Intent(context, TermConditionActivity.class);
                intent11.putExtra("type", "term");
                startActivity(intent11);
                getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
            case R.id.terms_layout:
                Intent intent111 = new Intent(context, TermConditionActivity.class);
                intent111.putExtra("type", "term");
                startActivity(intent111);
                getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
            case R.id.delete_profile:
                DELETE_PROFILE_Alert();
                break;
            case R.id.delete_profile___:
                DELETE_PROFILE_Alert();
                break;
            case R.id.language___:
                LanguageDialog();
                break;
            case R.id.language:
                LanguageDialog();
                break;
            case R.id.logout:
                LogoutAlert();
                break;
            case R.id.logout_layout:
                LogoutAlert();
                break;
        }
    }

    private void sharepost(String post_id) {

        Log.e("post_id", post_id);

        // Sending side
        byte[] data = post_id.getBytes(StandardCharsets.UTF_8);
        String post_id_encode = Base64.encodeToString(data, Base64.DEFAULT);

        Log.e("post_id", "yes" + post_id_encode);

        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        //  sharingIntent.putExtra(Intent.EXTRA_TEXT, "http://3.22.158.181/spatify/users/open/" + post_id);
        sharingIntent.putExtra(Intent.EXTRA_TEXT, "http://ezschedule43.com:8081/deeplink?url=app" + post_id_encode);
        context.startActivity(Intent.createChooser(sharingIntent, getString(R.string.share_profile_external)));
    }

    private void LogoutAlert() {
        new IOSDialog.Builder(context)
                .setTitle(context.getResources().getString(R.string.app_name))
                .setMessage(getString(R.string.are_you_sure_logout))
                .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        LOGOUT_API();
                    }
                })
                .setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).show();
    }

    private void LOGOUT_API() {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        FormBody.Builder formBuilder = new FormBody.Builder();
        formBuilder.add(Parameters.AUTH_KEY, "");
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.LOGOUT, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonObject = new JSONObject(result);
                        if (jsonObject.getString("code").equalsIgnoreCase("200")) {
                            savePref.setAuthorization_key("");
                            util.showToast(context, getString(R.string.professional_logout_successfully));
                            Intent intent = new Intent(context, SignInActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                        } else {
                            if (jsonObject.getString("msg").equals(util.Invalid_Authorization)) {
                                savePref.setAuthorization_key("");
                                Intent intent = new Intent(context, SignInActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                            } else {
                                util.showToast(context, jsonObject.getString("msg"));
                            }
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    private void DELETE_PROFILE_Alert() {
        new IOSDialog.Builder(context)
                .setTitle(context.getResources().getString(R.string.app_name))
                .setMessage(getString(R.string.sure_to_delete_profile))
                .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        DELETE_PROFILE_API();
                    }
                })
                .setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).show();
    }

    private void DELETE_PROFILE_API() {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        FormBody.Builder formBuilder = new FormBody.Builder();
        formBuilder.add(Parameters.AUTH_KEY, "");
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.DELETE_PROFILE, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonObject = new JSONObject(result);
                        if (jsonObject.getString("code").equalsIgnoreCase("200")) {
                            savePref.setAuthorization_key("");
                            util.showToast(context, getString(R.string.account_removed_successfully));
                            Intent intent = new Intent(context, SignInActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                        } else {
                            if (jsonObject.getString("msg").equals(util.Invalid_Authorization)) {
                                savePref.setAuthorization_key("");
                                Intent intent = new Intent(context, SignInActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                            } else {
                                util.showToast(context, jsonObject.getString("msg"));
                            }
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    private void LanguageDialog() {
        ArrayList<String> list = new ArrayList<>();
        list.add("English");
        list.add("Spanish");
        final CharSequence[] Animals = list.toArray(new String[list.size()]);
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        dialogBuilder.setTitle(context.getResources().getString(R.string.select_language));
        dialogBuilder.setItems(Animals, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                if (item == 0) {
                    savePref.setLang("en");
                    language.setText(context.getResources().getString(R.string.language) + " (English)");
                    /*language.setCompoundDrawablesWithIntrinsicBounds(getContext().getResources().getDrawable(R.drawable.flag),
                            null, getContext().getResources().getDrawable(R.drawable.arrow_d), null);*/
                    dialog.dismiss();
                    LANGUAGE_CHANGE_API("1");
                } else {
                    savePref.setLang("es");
                    language.setText(context.getResources().getString(R.string.language) + " (Spanish)");
                   /* language.setCompoundDrawablesWithIntrinsicBounds(getContext().getResources().getDrawable(R.drawable.romania_flag),
                            null, getContext().getResources().getDrawable(R.drawable.arrow_d), null);*/
                    dialog.dismiss();
                    LANGUAGE_CHANGE_API("2");

                }
            }
        });
        //Create alert dialog object via builder
        AlertDialog alertDialogObject = dialogBuilder.create();
        //Show the dialog
        alertDialogObject.show();
    }

    private void LANGUAGE_CHANGE_API(String type) {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        FormBody.Builder formBuilder = new FormBody.Builder();
        formBuilder.add(Parameters.TYPE, type);//1=eng , 2=spanish
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.LANGUAGE_CHANGE, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonObject = new JSONObject(result);
                        if (jsonObject.getString("code").equalsIgnoreCase("200")) {
                           context1= LocaleHelper.setLocale(context, savePref.getLang());
                           resources=context1.getResources();
                            Intent intent = new Intent(context, BarbarMainActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                        } else {
                            if (jsonObject.getString("msg").equals(util.Invalid_Authorization)) {
                                savePref.setAuthorization_key("");
                                Intent intent = new Intent(context, SignInActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                            } else {
                                util.showToast(context, jsonObject.getString("msg"));
                            }
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }
}